import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
class checkout extends Thread
{
	SuperUser akki;
	Customer cust;
	StoreHouse house;
	double bill;
	public checkout(Customer c, StoreHouse h)
	{
		this.cust = c;
		this.house = h;
	}
	public void run()
	{
		//********************************************************************
		SuperUser temp = null;	
		try
		{
			File data = new File("SuperUserData.txt");
			if(data.exists())
			{
				FileInputStream file = new FileInputStream(data);
				ObjectInputStream in = new ObjectInputStream(file);
				temp = (SuperUser) in.readObject();
				in.close();
				file.close();
			}
			System.out.println("Reading Successful");
		} 
		catch (IOException | ClassNotFoundException e) 
		{
			e.printStackTrace();
		}
		finally
		{
			if(temp == null)
			{
				akki = new SuperUser();
			}
			else
			{
				akki = temp;
			}
		}
		//********************************************************************
		double ans = 0;
		Customer one = null;
		StoreHouse two = null;
		for(Customer x : akki.getCustomers())
		{
			if(x.getIdentity().equals(this.cust.getIdentity()))
			{
				one = x;
				for(StoreHouse y : akki.getStores())
				{
					if(house.getStorehouseid() == house.getStorehouseid())
					{
						two = y;
						int k=0;
				    	boolean flag = true;
				    	while(k<x.shopping_list.size() && cust.getShopping_list().size()!=0)
				    	{
				    		Category temp1 = y.getAdmin().getProductid().get(x.shopping_list.get(k).getId());
				    		System.out.println(y.getAdmin().getProductid());
				    		System.out.println(x.shopping_list.get(k));
				    		
				    		if(temp1.getQty()<x.shopping_list.get(k).getQty())
				    		{
				    			System.out.println("Insufficient Stock Exception");
				    			flag = false;
				    			return;
				    		}
				    		else
				    		{
				    			temp1.setQty(temp1.getQty()-x.shopping_list.get(k).getQty());
				    			//**********************
				    			if(temp1.getQty() == 0)
				    			{
				    				ArrayList<Category> list = new ArrayList<Category>();
				    				list.add(new Category(temp1.getType(), 10));
				    				order thread = new order(list, y);
				                	thread.run();
				                	try 
				                	{
										thread.join();
									} 
				                	catch (InterruptedException e) 
				                	{
										System.out.println("Can Not Join The Thread");
									}
				    			}
				    			System.out.println("ORDRERED");
				    			//*************************
				    			flag = true;
				    		}
				    		if(flag==true)
				    		{
				    			x.getShopping_list().remove(k);
				    			
				    		}
				    		else
				    		{
				    			k++;
				    		}
//				    		cust.getShopping_list().remove(0);
				    	}
					}
				}
			}
		}
		this.bill = ans;
		
		//********************************************************************
		try
		{
			File data = new File("SuperUserData.txt");
			if(data.exists())
			{
				FileOutputStream file = new FileOutputStream(data);
				ObjectOutputStream out = new ObjectOutputStream(file);
				
				data.delete();
				
				data = new File("SuperUserData.txt");
				file = new FileOutputStream(data);
				out = new ObjectOutputStream(file);
				
				out.writeObject(akki);
				out.close();
				file.close();
			}
			else
			{
				FileOutputStream file = new FileOutputStream(data);
				ObjectOutputStream out = new ObjectOutputStream(file);
				out.writeObject(akki);
				out.close();
				file.close();
			}
			System.out.println("Writing Successful");
		}
		catch(IOException e)
		{
			e.printStackTrace();
//			System.out.println("Error While Writing the Data");
		}
		//********************************************************************
	}
}
class update extends Thread
{
	SuperUser akki ;
	int qty ;
	long id;
	Cart c;
	Customer cust;
	public update(int q, long i, Customer c, Cart e)
	{
		this.qty = q;
		this.id = i;
		this.cust = c;
		this.c = e;
	}
	public void run()
	{
		//********************************************************************
		SuperUser temp = null;	
		try
		{
			File data = new File("SuperUserData.txt");
			if(data.exists())
			{
				FileInputStream file = new FileInputStream(data);
				ObjectInputStream in = new ObjectInputStream(file);
				temp = (SuperUser) in.readObject();
				in.close();
				file.close();
			}
			System.out.println("Reading Successful");
		} 
		catch (IOException | ClassNotFoundException e) 
		{
			e.printStackTrace();
		}
		finally
		{
			if(temp == null)
			{
				akki = new SuperUser();
			}
			else
			{
				akki = temp;
			}
		}
		//********************************************************************
		System.out.println(qty+"Changed");
		for(Customer x : akki.getCustomers())
		{
			if(x.getIdentity().equals(this.cust.getIdentity()))
			{
				for(Cart y : x.getShopping_list())
				{
					if(y.getId() == c.getId())
					{
						y.setQty(qty);
					}
				}
			}
		}
		//********************************************************************
		try
		{
			File data = new File("SuperUserData.txt");
			if(data.exists())
			{
				FileOutputStream file = new FileOutputStream(data);
				ObjectOutputStream out = new ObjectOutputStream(file);
				
				data.delete();
				
				data = new File("SuperUserData.txt");
				file = new FileOutputStream(data);
				out = new ObjectOutputStream(file);
				
				out.writeObject(akki);
				out.close();
				file.close();
			}
			else
			{
				FileOutputStream file = new FileOutputStream(data);
				ObjectOutputStream out = new ObjectOutputStream(file);
				out.writeObject(akki);
				out.close();
				file.close();
			}
			System.out.println("Writing Successful");
		}
		catch(IOException e)
		{
			e.printStackTrace();
//			System.out.println("Error While Writing the Data");
		}
		//********************************************************************
	}
}
class addproduct extends Thread
{
	SuperUser akki;
	int qty;
	long id;
	Customer cust ; 
	ArrayList<Category> total;
	public addproduct(int q, long i, Customer c, ArrayList<Category> obj)
	{
		this.qty = q;
		this.id = i;
		this.cust = c;
		this.total = obj;
		System.out.println(id+" "+qty+" "+cust);
	}
	public void run()
	{
		//********************************************************************
		SuperUser temp = null;	
		try
		{
			File data = new File("SuperUserData.txt");
			if(data.exists())
			{
				FileInputStream file = new FileInputStream(data);
				ObjectInputStream in = new ObjectInputStream(file);
				temp = (SuperUser) in.readObject();
				in.close();
				file.close();
			}
			System.out.println("Reading Successful");
		} 
		catch (IOException | ClassNotFoundException e) 
		{
			e.printStackTrace();
		}
		finally
		{
			if(temp == null)
			{
				akki = new SuperUser();
			}
			else
			{
				akki = temp;
			}
		}
		//********************************************************************
		for(Customer x : akki.getCustomers())
		{
			if(x.getIdentity().equals(this.cust.getIdentity()))
			{
				double bill = 0.0;
				for(Category xx : total)
				{
					if(xx.getId() == id) 
					{
						bill = xx.getPrize();
					}
				}
				
				Cart t = new Cart(qty, id);
				t.setBill(bill);
				System.out.println(t+"FIanl");
				x.shopping_list.add(t);
				System.out.println(x.shopping_list);
			}
		}
		//********************************************************************
		try
		{
			File data = new File("SuperUserData.txt");
			if(data.exists())
			{
				FileOutputStream file = new FileOutputStream(data);
				ObjectOutputStream out = new ObjectOutputStream(file);
				
				data.delete();
				
				data = new File("SuperUserData.txt");
				file = new FileOutputStream(data);
				out = new ObjectOutputStream(file);
				
				out.writeObject(akki);
				out.close();
				file.close();
			}
			else
			{
				FileOutputStream file = new FileOutputStream(data);
				ObjectOutputStream out = new ObjectOutputStream(file);
				out.writeObject(akki);
				out.close();
				file.close();
			}
			System.out.println("Writing Successful");
		}
		catch(IOException e)
		{
			e.printStackTrace();
//			System.out.println("Error While Writing the Data");
		}
		//********************************************************************
	}
}
class addcustomer extends Thread
{
	SuperUser akki;
	Customer house ;
	String id;
	String pass ;
	public addcustomer(Customer obj, String i, String p)
	{
		this.house = obj;
		this.id = i;
		this.pass = p;
	}
	public void run()
	{
		//********************************************************************
		SuperUser temp = null;
		try
		{
			File data = new File("SuperUserData.txt");
			if(data.exists())
			{
				FileInputStream file = new FileInputStream(data);
				ObjectInputStream in = new ObjectInputStream(file);
				temp = (SuperUser) in.readObject();
				in.close();
				file.close();
			}
			System.out.println("Reading Successful");
		} 
		catch (IOException | ClassNotFoundException e) 
		{
			e.printStackTrace();
		}
		finally
		{
			if(temp == null)
			{
				akki = new SuperUser();
			}
			else
			{
				akki = temp;
			}
		}
		//********************************************************************
		akki.getCustomers().add(new Customer(0.0, id, pass));
		//********************************************************************
		try
		{
			File data = new File("SuperUserData.txt");
			if(data.exists())
			{
				FileOutputStream file = new FileOutputStream(data);
				ObjectOutputStream out = new ObjectOutputStream(file);
				
				data.delete();
				
				data = new File("SuperUserData.txt");
				file = new FileOutputStream(data);
				out = new ObjectOutputStream(file);
				
				out.writeObject(akki);
				out.close();
				file.close();
			}
			else
			{
				FileOutputStream file = new FileOutputStream(data);
				ObjectOutputStream out = new ObjectOutputStream(file);
				out.writeObject(akki);
				out.close();
				file.close();
			}
			System.out.println("Writing Successful");
		}
		catch(IOException e)
		{
			e.printStackTrace();
//			System.out.println("Error While Writing the Data");
		}
		//********************************************************************
	}
}
public class CustomerThread 
{
	
}
