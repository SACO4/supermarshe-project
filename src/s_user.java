import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;

import javafx.application.Application;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.Separator;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCharacterCombination;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.Mnemonic;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.stage.Stage;
import javafx.stage.StageStyle;


public class s_user extends Application {
    private double xOffset = 0;
    private double yOffset = 0;

    public static class product 
    {
    	String name1;
    	long qty1;
    	double cost1;
    	long id;
    	public product(String n,long q, double c, long i)
    	{
    		name1 = n;
    		qty1 = q;
    		cost1 = c;
    		id = i;
    	}
    	public String getName1() {
    		return name1;
    	}
    	public long getId() {
			return id;
		}
		public void setId(long id) {
			this.id = id;
		}
		public void setName1(String name1) {
    		this.name1 = name1;
    	}
    	public long getQty1() {
    		return qty1;
    	}
    	public void setQty1(long qty1) {
    		this.qty1 = qty1;
    	}
    	public double getCost1() {
    		return cost1;
    	}
    	public void setCost1(double cost1) {
    		this.cost1 = cost1;
    	}
    	public String toString()
    	{
    		return(this.name1+" "+this.qty1+" "+this.cost1);
    	}
    }
    
    public static class admin 
    {
    	String name1;
    	long qty1;
    	String state;
    	String city;
    	long qty2;
    	public admin(String n,long q, String st, String ct, long q2)
    	{
    		name1 = n;
    		qty1 = q;
    		state = st;
    		city = ct;
    		qty2 = q2;
    	}
    	public String getName1() {
			return name1;
		}
		public void setName1(String name1) {
			this.name1 = name1;
		}
		public long getQty1() {
			return qty1;
		}
		public void setQty1(long qty1) {
			this.qty1 = qty1;
		}
		public String getState() {
			return state;
		}
		public void setState(String state) {
			this.state = state;
		}
		public String getCity() {
			return city;
		}
		public void setCity(String city) {
			this.city = city;
		}
		public long getQty2() {
			return qty2;
		}
		public void setQty2(long qty2) {
			this.qty2 = qty2;
		}
		public String toString()
    	{
    		return(this.name1+" "+this.qty1+" "+this.qty2+" "+this.state+" "+this.city);
    	}
    }
    public static VBox linkPanel()
    {
    	VBox ans = new VBox();
    	ans.getStylesheets().add("button.css");
    	GridPane grid = new GridPane();
    	grid.setHgap(20);
    	grid.setVgap(10);
    	grid.setPadding(new Insets(50,10,30,20));
    	grid.getStylesheets().add("button.css");
        grid.setStyle("-fx-background-color: #3a3a3a;");
        
        Label text = new Label();
        text.setText("Enter Number of Queries");
        text.setStyle("-fx-text-fill: #ffffff;");
        text.setAlignment(Pos.CENTER);
        text.setPadding(new Insets(5,0,5,0));
        
        final TextField search = new TextField();
        search.setStyle("-fx-background-color: #ffaaaa;-fx-prompt-text-fill: #ffffff;-fx-text-fill: #3a3a3a;");
        search.setPromptText("Length of Path");
        search.setAlignment(Pos.BASELINE_LEFT);
        search.setText(null);
        search.setMinWidth(400);
        
        Button submit = new Button();
        submit.setText("Submit");
        submit.setId("GeneralButton");
        submit.setMinWidth(40);
        
        grid.add(text, 1, 0);
        grid.add(search, 2, 0);
        grid.add(submit, 3, 0);
        
        ScrollPane scroll = new ScrollPane();
        
        ans.getChildren().add(grid);
        
        submit.setOnAction(e ->
        {
        	while(ans.getChildren().size()!=1)
        	{
        		ans.getChildren().remove(1);
        	}
        	
        	Label left = new Label();
        	left.setText("WareHouse/StoreHouse Admin Id");
        	left.setStyle("-fx-text-fill: #3a3a3a;");
        	left.setAlignment(Pos.CENTER_LEFT);
        	left.setPadding(new Insets(5,0,5,0));
        	left.setPrefSize(300, 30);
        	
        	Label right = new Label();
        	right.setText("WareHouse/StoreHouse Id");
        	right.setStyle("-fx-text-fill: #3a3a3a;");
        	right.setAlignment(Pos.CENTER_LEFT);
        	right.setPadding(new Insets(5,0,5,10));
        	right.setPrefSize(300, 30);
        	
        	VBox outerpart = new VBox();
        	outerpart.setPadding(new Insets(15,15,15,15));
        	outerpart.setSpacing(30);
        	HBox first = new HBox();
        	first.setSpacing(300);
        	first.getChildren().addAll(left, right);
        	outerpart.getChildren().add(first);
        	long n = Integer.parseInt(search.getText());
        	final TextField search1[] = new TextField[(int) n];
        	final TextField search11[] = new TextField[(int) n];
        	for(int i=0;i<n;i++)
        	{
        		HBox part = new HBox();
        		part.setSpacing(50);
        		search1[i] = new TextField();
    	        search1[i].setStyle("-fx-background-color: #ffaaaa;-fx-prompt-text-fill: #ffffff;-fx-text-fill: #ffffff;");
    	        search1[i].setPromptText("Enter StoreHouse/WareHouse Id");
    	        search1[i].setAlignment(Pos.BASELINE_LEFT);
//    	        search1.setAlignment(Pos.CENTER);
    	        search1[i].setText(null);
    	        search1[i].setMinWidth(300);
    	        
    	        search11[i] = new TextField();
    	        search11[i].setStyle("-fx-background-color: #ffaaaa;-fx-prompt-text-fill: #ffffff;-fx-text-fill: #ffffff;");
    	        search11[i].setPromptText("Enter StoreHouse/WareHouse Id");
    	        search11[i].setAlignment(Pos.BASELINE_LEFT);
//    	        search1.setAlignment(Pos.CENTER);
    	        search11[i].setText(null);
    	        search11[i].setMinWidth(300);
    	        
    	        Label center = new Label();
    	        center.setText("------------------->");
    	        center.setStyle("-fx-text-fill: #3a3a3a;");
    	        center.setAlignment(Pos.CENTER);
    	        center.setPadding(new Insets(5,0,5,0));
    	        
    	        part.getChildren().addAll(search1[i], center, search11[i]);
    	        outerpart.getChildren().add(part);
        	}
        	scroll.setContent(outerpart);
        	
        	Button next = new Button();
        	next.setText("Submit");
        	next.setId("GeneralButtonGreen");
        	next.setPadding(new Insets(10, 10, 10 ,10));
	        next.setAlignment(Pos.BASELINE_LEFT);
        	
        	ans.getChildren().add(scroll);
        	ans.setSpacing(20);
        	ans.setMargin(next, new Insets(10,10,10,10));
        	ans.getChildren().add(next);
        	
        	next.setOnAction(f ->
        	{
        		
                ArrayList<Integer> part1 = new ArrayList<Integer>();
                ArrayList<Integer> part2 = new ArrayList<Integer>();
                for(int i=0;i<n;i++)
                {
                	part1.add(Integer.parseInt(search1[i].getText()));
                	part2.add(Integer.parseInt(search11[i].getText()));
                }
                LinkThread thread = new LinkThread(part1,part2);
            	thread.start();
            	try 
            	{
					thread.join();
				} 
            	catch (InterruptedException e1) 
            	{
            		
				}
            	System.out.println(thread.condition);
            	if(thread.condition == true)
            	{
            		Alert alert = new Alert(AlertType.INFORMATION);
                    alert.setTitle("Information Dialog");
                    alert.setHeaderText(null);
                    alert.setContentText("Linked Successfully");
                    alert.initStyle(StageStyle.UNDECORATED);
                    alert.showAndWait();
            	}
            	else
            	{
            		Alert alert = new Alert(AlertType.ERROR);
                    alert.setTitle("Information Dialog");
                    alert.setHeaderText(null);
                    alert.setContentText("Unable To Link");
                    alert.initStyle(StageStyle.UNDECORATED);
                    alert.showAndWait();
            	}
                
                while(ans.getChildren().size()!=1)
                {
                	ans.getChildren().remove(1);
                	search.setText(null);
                }
        	});
        	
        	});
		return ans;
    	
    }
    public static void main(String[] bale) {
        launch(bale);
    }
    public static void DisplayProduct(VBox grid,ObservableList<admin> temp)
    {
    	 TableView<admin> table = new TableView<admin>();
    	 table.setStyle("-fx-background-color: #ff9393;");
//    	 table.setEditable(true);
    	 TableColumn idCol = new TableColumn("Admin Id");
    	 idCol.setMinWidth(20);
    	 idCol.setCellValueFactory(
                 new PropertyValueFactory<admin, String>("qty1"));
    	 
         TableColumn firstNameCol = new TableColumn("Admin Name");
         firstNameCol.setMinWidth(250);
         firstNameCol.setCellValueFactory(
                 new PropertyValueFactory<admin, String>("name1"));
  
         TableColumn lastNameCol = new TableColumn("WareHouse Id");
         lastNameCol.setMinWidth(10);
         lastNameCol.setCellValueFactory(
                 new PropertyValueFactory<admin, String>("qty2"));
  
         TableColumn emailCol = new TableColumn("State");
         emailCol.setMinWidth(70);
         emailCol.setCellValueFactory(
                 new PropertyValueFactory<admin, String>("state"));
         
         TableColumn city = new TableColumn("City");
         city.setMinWidth(70);
         city.setCellValueFactory(
                 new PropertyValueFactory<admin, String>("city"));
         
         table.getColumns().addAll(idCol, firstNameCol, lastNameCol, emailCol, city);
         
         System.out.println(temp.size());
//         grid.setPadding(new Insets(10,10,10,10));
         table.setItems(temp);
//         ((Group) scene.getRoot()).getChildren().addAll(grid);
         if(grid.getChildren().size()>1)
         {
        	 grid.getChildren().remove(grid.getChildren().size()-1);
         }
         grid.getChildren().add(table);
    }
    public static VBox SearchPanel()
    {
    	ArrayList<admin> list = new ArrayList<admin>();
    	list.add(new admin("Sachin",1,"Delhi", "New Delhi", 11));
    	list.add(new admin("Utsav",2, "Delhi", "New Delhi", 71));
    	list.add(new admin("Manish",3, "Delhi", "New Delhi", 81));
    	list.add(new admin("Durvish",4, "Delhi", "New Delhi", 191));
    	list.add(new admin("Nakul",5, "Delhi", "New Delhi", 101));
    	list.add(new admin("Preyansh",6, "Delhi", "New Delhi", 111));
    	list.add(new admin("Shivam",7, "Delhi", "New Delhi", 112));
    	list.add(new admin("Shiva",8, "Delhi", "New Delhi", 113));
    	list.add(new admin("Aniket",9, "Delhi", "New Delhi", 114));
    	list.add(new admin("Anton",10, "Delhi", "New Delhi", 121));
    	list.add(new admin("Aman",11, "Delhi", "New Delhi", 311));
    	list.add(new admin("Prakhar",12, "Delhi", "New Delhi", 411));
    	list.add(new admin("Jay",13, "Delhi", "New Delhi", 511));
    	list.add(new admin("Dhruv",14, "Delhi", "New Delhi", 611));
    	
    	VBox ans = new VBox();
    	
    	GridPane grid = new GridPane();
    	grid.setHgap(20);
    	grid.setVgap(10);
    	grid.setPadding(new Insets(50,10,50,20));
    	grid.getStylesheets().add("button.css");
        grid.setStyle("-fx-background-color: #3a3a3a;");
        Label s = new Label();
        s.setStyle("-fx-text-fill: #ffffff;");
        s.setText("Search");
        
        final TextField search = new TextField();
        search.setStyle("-fx-background-color: #ffaaaa;-fx-prompt-text-fill: #ffffff;-fx-text-fill: #3a3a3a;");
        search.setPromptText("Search Any Product");
        search.setAlignment(Pos.BASELINE_LEFT);
        search.setText(null);
        search.setMinWidth(400);
        
        Button submit = new Button();
        submit.setText("Search");
		submit.setId("GeneralButton");
		submit.setMinWidth(150);
		
		
		Button clear = new Button();
        clear.setText("Cancel");
		clear.setId("GeneralButton");
		clear.setMinWidth(150);
		
		submit.setOnAction(e -> 
        {
        	ArrayList<admin> data = new ArrayList<admin>();
        	ObservableList<admin> temp = FXCollections.observableArrayList();
        	String str = search.getText();
        	if(str!=null)
        	{
        		for(admin p : list)
            	{
        			String arr = p.name1+Long.toString(p.qty1)+p.state+p.city+Long.toString(p.qty2);
            		if(arr.toLowerCase().contains(str.toLowerCase())||str.toLowerCase().contains(arr.toLowerCase()))
            		{
            			data.add(p);
            			temp.add(p);
            		}
            	}
        		DisplayProduct(ans, temp);
        	}
        	else
        	{
        		for(admin p : list)
        		{
        			data.add(p);
//        			temp.add(new admin(p.name1, p.qty1, p.cost1, p.id));
        			temp.add(p);
        		}
        		DisplayProduct(ans, temp);
        	}
  		});
  		clear.setOnAction(e ->
  		{
	  		search.setText(null);
		});
        grid.setMargin(s, new Insets(0,0,0,00));
        grid.setMargin(search, new Insets(0,0,0,0));
        grid.setMargin(clear, new Insets(0,0,0,0));
        grid.setHalignment(s, HPos.CENTER);
        grid.setHalignment(search, HPos.CENTER);
        grid.add(s, 0, 0);
        grid.add(search, 1, 0);
        grid.add(submit, 2, 0);
        grid.add(clear, 3, 0);
        ans.getChildren().add(grid);
        return ans;
    }
    @Override
    public void start(final Stage primaryStage) throws FileNotFoundException {
        primaryStage.initStyle(StageStyle.UNDECORATED);
        
        BorderPane root = new BorderPane();
        final Tooltip tooltip = new Tooltip();
        tooltip.setText("\nClose\n");
        root.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });
        root.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                primaryStage.setX(event.getScreenX() - xOffset);
                primaryStage.setY(event.getScreenY() - yOffset);
            }
        });
        GridPane grid = new GridPane();
        grid.getStylesheets().add("button.css");
        grid.setStyle("-fx-background-color: #dc143c;");
        Button exit = new Button();
        Button mini = new Button();
        Label text = new Label();
        text.setText("Super Marché");
        text.setId("MarketName");
        mini.setId("OpenMinButton");
        exit.setId("OpenMinButton");
        exit.setTooltip(new Tooltip("Close"));
        mini.setTooltip(new Tooltip("Minimize"));
        exit.setText("x");
        mini.setText("-");
        exit.setOnAction(e -> primaryStage.close());
        mini.setOnAction(e -> {((Stage)((Button)e.getSource()).getScene().getWindow()).setIconified(true);});
        grid.setPadding(new Insets(5, 0, 10, 5));
        grid.setVgap(5); 
        grid.setHgap(5);
        ColumnConstraints col1 = new ColumnConstraints();
        col1.setPercentWidth(94);
        ColumnConstraints col2 = new ColumnConstraints();
        col2.setPercentWidth(3);
        ColumnConstraints col3 = new ColumnConstraints();
        col3.setPercentWidth(3);
        grid.getColumnConstraints().addAll(col1);
        text.setAlignment(Pos.TOP_LEFT);
        
        grid.add(exit, 2, 0);
        grid.add(mini, 1, 0);
        grid.add(text, 0, 0);
        root.setTop(grid);

        GridPane navigation = new GridPane();
        navigation.setMinHeight(500);
      	navigation.setMinWidth(300);
      	navigation.setMaxWidth(300);
        navigation.setAlignment(Pos.TOP_CENTER);
        navigation.getStylesheets().add("button.css");
        Label name = new Label();
        name.setStyle("-fx-text-fill: #ffffff;-fx-font-weight: bold;");
        name.setText("USERNAME");
        Label user = new Label();
        user.setStyle("-fx-text-fill: #ffffff;");
        user.setText("Super User");
        user.setStyle("-fx-font-weight: bold;-fx-text-fill: #ffffff;-fx-font-size: 20;");
        Button nav0 = new Button();
        nav0.setId("NavigationButton");
        nav0.setText("Home");
        Button nav1 = new Button();
        nav1.setId("NavigationButton");
        nav1.setText("Search ");
        Button nav2 = new Button();
        nav2.setId("NavigationButton");
        nav2.setText("Link");
        Button nav3 = new Button();
        nav3.setId("NavigationButton");
        nav3.setText("Add New Admin");
        Button nav4 = new Button();
        nav4.setId("NavigationButton");
        nav4.setText("Add New Store");
        Button nav5 = new Button();
        nav5.setId("NavigationButton");
        nav5.setText("Add New StoreHouse");
//        Button nav6 = new Button();
//        nav6.setId("NavigationButton");
//        nav6.setText("Add New StoreHouse Admin");
        Button nav7 = new Button();
        nav7.setId("GeneralButtonRed");
        nav7.setPrefSize(1000, 20);
        nav7.setText("Log Out");
        
        Image img = new Image(new FileInputStream("superuser.png"));
        ImageView i = new ImageView(img);
        i.setFitHeight(230);
      	i.setFitWidth(230);
      	i.setPreserveRatio(true);
      	i.setStyle("-fx-background-color: #494949;");
      	
      	navigation.setHgap(0);
      	navigation.setPadding(new Insets(0,0,0,0));
      	navigation.setMargin(user, new Insets(20,10,10,10));
      	navigation.setMargin(i, new Insets(30,30,30,30));
      	navigation.setMargin(nav0, new Insets(60,10,0,10));
      	navigation.setMargin(nav1, new Insets(0,10,0,10));
      	navigation.setMargin(nav2, new Insets(0,10,0,10));
      	navigation.setMargin(nav3, new Insets(0,10,0,10));
      	navigation.setMargin(nav4, new Insets(0,10,0,10));
//      	navigation.setMargin(nav5, new Insets(0,10,0,10));
//      	navigation.setMargin(nav6, new Insets(0,10,0,10));
      	navigation.setMargin(nav7, new Insets(60,10,0,10));
      	navigation.setVgap(0);
      	
      	navigation.add(i, 0, 1);
      	navigation.add(user, 0, 2);
      	navigation.add(nav0, 0, 3);
//      	navigation.add(nav1, 0, 4);
      	navigation.add(nav2, 0, 5);
      	navigation.add(nav3, 0, 6);
      	navigation.add(nav4, 0, 7);
//      	navigation.add(nav6, 0, 8);
      	navigation.add(nav7, 0, 8);
//      	navigation.add(nav7, 0, 10);
      	
      	navigation.setHalignment(i, HPos.CENTER);
      	navigation.setHalignment(user, HPos.CENTER);
      	navigation.setHalignment(nav0, HPos.CENTER);
//      	navigation.setHalignment(nav1, HPos.CENTER);
      	navigation.setHalignment(nav2, HPos.CENTER);
      	navigation.setHalignment(nav3, HPos.CENTER);
      	navigation.setHalignment(nav4, HPos.CENTER);
//      	navigation.setHalignment(nav5, HPos.CENTER);
//      	navigation.setHalignment(nav6, HPos.CENTER);
      	navigation.setHalignment(nav7, HPos.CENTER);
      	navigation.setStyle("-fx-background-color: #494949;");
      	
      	ArrayList<product> order = new ArrayList<product>();
      	order.add(new product("IPhone",10, 90000.0, 2));
      	order.add(new product("Vivo",12, 15000.0, 3));
      	order.add(new product("Redmi Note 4g",101, 5000.0, 4));
      	order.add(new product("Motorola",93, 40000.0, 5));
      	order.add(new product("Redmi 1s",93, 40000.0, 6));
      	order.add(new product("OnePlus 5t",15, 45000.0, 1));
      	order.add(new product("OnePlus 6t",15, 20000.0, 7));
      	order.add(new product("OnePlus 5",15, 45000.0, 8));
      	order.add(new product("OnePlus 6",15, 45000.0, 9));
      	order.add(new product("OnePlus 1",15, 45000.0, 10));
      	
      	
        root.setLeft(navigation);
        root.setCenter(HomePanel(order));
        nav0.setOnAction(e ->
        {
        	VBox temp = HomePanel(order);
        	root.setCenter(temp);
        });
        nav1.setOnAction(e ->
        {
        	VBox temp = SearchPanel();
        	root.setCenter(temp);
        });
        nav2.setOnAction(e ->
        {
        	VBox temp = linkPanel();
        	root.setCenter(temp);
        });
        nav3.setOnAction(e ->
        {
        	VBox temp = AddAdmin();
        	root.setCenter(temp);
        });
        nav4.setOnAction(e ->
        {
        	VBox temp = AddStore();
        	root.setCenter(temp);
        });
//        nav5.setOnAction(e ->
//        {
//        	VBox temp = OrderPanel();
//        	root.setCenter(temp);
//        });
//        nav6.setOnAction(e ->
//        {
//        	VBox temp = HandleOrder(order);
//        	root.setCenter(temp);
//        });
        nav7.setOnAction(e ->
        {
        	Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Information Dialog");
            alert.setHeaderText(null);
            alert.setContentText("Logged Out successfully !");
            alert.initStyle(StageStyle.UNDECORATED);
            alert.showAndWait();
            
            loginpage obj = new loginpage();
            
    		try 
    		{
    			primaryStage.close();
    			Stage n = new Stage();
				obj.start(n);
			} 
    		catch (FileNotFoundException e1) 
    		{
				e1.printStackTrace();
			}
        });
//        root.setLeft(i);
        GridPane foot = new GridPane();
        foot.setStyle("-fx-background-color: #dc143c;");
        foot.setPadding(new Insets(10, 10, 10, 20));
        name = new Label("©VENOM");
        name.setStyle("-fx-text-fill: #ffffff;");
        Label pass = new Label(" v 1.0 bETA");
        pass.setStyle("-fx-text-fill: #ffffff;");
        name.setAlignment(Pos.CENTER_LEFT);
        pass.setAlignment(Pos.CENTER_RIGHT);
        foot.add(name, 0, 0);
        foot.add(pass, 1, 0);
        root.setBottom(foot);
        
        Scene scene = new Scene(root, 1200, 900);
        primaryStage.setScene(scene);
        primaryStage.setMinHeight(900);
        primaryStage.setMinWidth(1200);
        primaryStage.show();
    }
    public static VBox HandleOrder(ArrayList<product> order) 
    {
    	VBox ans = new VBox();
    	ans.setSpacing(20);
    	ans.getStylesheets().add("button.css");
    	ans.setPrefSize(500, 500);
    	ans.setStyle("-fx-background-color: #ffffff;");
    	Separator line = new Separator();
        line.setOrientation(Orientation.HORIZONTAL);
    	
    	Label s = new Label();
        s.setText("Pending Orders ");
        s.setPadding(new Insets(10,0,0,30));
        s.setPrefSize(300, 70);
        s.setStyle("-fx-text-fill: #3a3a3a; -fx-font-size: 30; -fx-background-color: #ffffff;");
        
        ans.getChildren().addAll(s, line);
    	
//        Label s0 = new Label();
//        s0.setText("Number of Pending Order : " + order.size());
//        s0.setPadding(new Insets(10,0,0,10));
//        s0.setPrefSize(200, 50);
//        s0.setStyle("-fx-text-fill: #3a3a3a;  -fx-background-color: #ffffff;");
//        
//        ans.getChildren().addAll(s0, line);
        
    	ScrollPane scroll = new ScrollPane();
    	VBox outerpart = new VBox();
    	
    	for(int i=0;i<order.size();i++)
		{
			HBox part = new HBox();
            part.setPrefSize(900, 50);
            part.setSpacing(50);
            
            if(i%2==0)
            	part.setStyle("-fx-background-color: #ffffff;-fx-background-color: #ffaaaa;");
            else
            	part.setStyle("-fx-background-color: #ffffff;");
            
            Label s1 = new Label();
            s1.setText("Name ");
            s1.setPadding(new Insets(10,0,0,10));
            s1.setPrefSize(100, 10);
            s1.setStyle("-fx-text-fill: #3a3a3a;");
            
            Label s2 = new Label();
            s2.setText(order.get(i).name1);
            s2.setPadding(new Insets(10,0,0,10));
            s2.setPrefSize(100, 10);
            s2.setStyle("-fx-text-fill: #3a3a3a;");
            
            Label s3 = new Label();
            s3.setText("Quantites");
            s3.setPadding(new Insets(10,0,0,10));
            s3.setPrefSize(100, 10);
            s3.setStyle("-fx-text-fill: #3a3a3a;");

            Label s4 = new Label();
            s4.setText(Long.toString(order.get(i).qty1));
            s4.setPadding(new Insets(10,0,0,10));
            s4.setPrefSize(100, 10);
            s4.setStyle("-fx-text-fill: #3a3a3a;");
            
            part.getChildren().addAll(s1, s2, s3, s4);
            part.setPadding(new Insets(10,10,10,20));
            outerpart.getChildren().add(part);
            
		}
//    	DisplayProduct(ans, (ObservableList<product>) order);
    	scroll.setContent(outerpart);
    	ans.getChildren().add(scroll);
    	
    	if(order.size()!=0)
    	{
    		Button accept1 = new Button();
    		accept1.setText("Handle Order");
    		accept1.setId("GeneralButtonGreen");
    		accept1.setPadding(new Insets(10, 10, 10 ,10));
    		accept1.setAlignment(Pos.BASELINE_LEFT);
    		
    		ans.getChildren().add(accept1);
    		
    		accept1.setOnAction(g -> 
    		{
    			Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Information Dialog");
                alert.setHeaderText(null);
                alert.setContentText("Are you sure ?");
                alert.initStyle(StageStyle.UNDECORATED);
                alert.showAndWait();
                while(order.size()!=0)
                {
                	order.remove(0);
                }
                ans.getChildren().remove(ans.getChildren().size()-1);
                ans.getChildren().remove(ans.getChildren().size()-1);
    		});
    	}
		
    	return ans;
    }
    public static VBox AddAdmin()
    {
    	VBox ans = new VBox();
    	ans.getStylesheets().add("button.css");
    	GridPane grid = new GridPane();
    	grid.setHgap(20);
    	grid.setVgap(10);
    	grid.setPadding(new Insets(50,10,10,20));
    	grid.getStylesheets().add("button.css");
        grid.setStyle("-fx-background-color: #3a3a3a;");
        
        Button bypath = new Button();
        bypath.setText("WareHouse Admin");
        bypath.setId("GeneralButton");
        bypath.setMinWidth(410);
		
		Button byid = new Button();
		byid.setText("StoreHouse Admin");
		byid.setId("GeneralButton");
		byid.setMinWidth(410);
    	
		grid.add(bypath, 1, 0);
		grid.add(byid, 2, 0);
		grid.setHalignment(byid, HPos.CENTER);
		grid.setHalignment(bypath, HPos.CENTER);
		grid.setPadding(new Insets(50, 10, 50, 10));
		ans.getChildren().add(grid);
		
		bypath.setOnAction(e ->
		{
			VBox outerpart = new VBox();
			while(ans.getChildren().size()!=1)
			{
				ans.getChildren().remove(1);
			}
			Label pass = new Label(" Add WareHouse Admin");
	        pass.setStyle("-fx-text-fill: #000000;-fx-font-size: 20;"); 
	        pass.setPadding(new Insets(20, 20, 20, 30));
	        pass.setAlignment(Pos.CENTER_RIGHT);
	        Separator line = new Separator();
	        line.setOrientation(Orientation.HORIZONTAL);
	        ans.getChildren().add(pass);
	        ans.getChildren().add(line);
	        
	        HBox part1 = new HBox();
	        part1.setStyle("-fx-background-color: #ffffff;");
	        part1.setSpacing(63);
	        part1.setPadding(new Insets(10, 10, 10 ,10));
	        
	        Label s = new Label();
	        s.setText("Enter First Name ");
	        s.setStyle("-fx-text-fill: #3a3a3a;");
	        s.setPadding(new Insets(5,25,0,0));
	        
	        final TextField search = new TextField();
	        search.setStyle("-fx-background-color: #ffaaaa;-fx-prompt-text-fill: #ffffff;-fx-text-fill: #3a3a3a;");
	        search.setPromptText("Enter First Name");
	        search.setAlignment(Pos.BASELINE_LEFT);
	        search.setText(null);
	        search.setMinWidth(400);
	        
	        part1.getChildren().addAll(s, search);
	        
	        Button accept = new Button();
	        accept.setText("Add New Admin");
	        accept.setId("GeneralButtonGreen");
	        accept.setPadding(new Insets(10, 10, 10 ,10));
	        accept.setAlignment(Pos.BASELINE_LEFT);
	        
	        HBox part2 = new HBox();
	        part2.setStyle("-fx-background-color: #3a3a3a;");
	        part2.setSpacing(90);
	        part2.setPadding(new Insets(10, 10, 10 ,10));
	        
	        Label s1 = new Label();
	        s1.setText("Enter Last Name");
	        s1.setStyle("-fx-text-fill: #ffffff;");
	        s1.setPadding(new Insets(5,0,0,0));
	        
	        final TextField search1 = new TextField();
	        search1.setStyle("-fx-background-color: #ffaaaa;-fx-prompt-text-fill: #ffffff;-fx-text-fill: #3a3a3a;");
	        search1.setPromptText("Enter Last Name");
	        search1.setAlignment(Pos.BASELINE_LEFT);
	        search1.setText(null);
	        search1.setMinWidth(400);
	        
	        part2.getChildren().addAll(s1, search1);
	        
	        HBox part4 = new HBox();
	        part4.setStyle("-fx-background-color: #ffffff;");
	        part4.setSpacing(40);
	        part4.setPadding(new Insets(10, 10, 10 ,10));
	        
	        Label s4 = new Label();
	        DatePicker date = new DatePicker();
	        s4.setText("Enter DOB(dd/mm/yyyy)");
	        s4.setStyle("-fx-text-fill: #3a3a3a;");
	        s4.setPadding(new Insets(5,0,0,0));
	        
	        final TextField search4 = new TextField();
	        search4.setStyle("-fx-background-color: #ffaaaa;-fx-prompt-text-fill: #ffffff;-fx-text-fill: #3a3a3a;");
	        search4.setPromptText("dd / mm / yyyy");
	        search4.setAlignment(Pos.BASELINE_LEFT);
	        search4.setText(null);
	        search4.setMinWidth(400);
	        
	        part4.getChildren().addAll(s4, date);
	        
	        HBox part5 = new HBox();
	        part5.setStyle("-fx-background-color: #3a3a3a;");
	        part5.setSpacing(60);
	        part5.setPadding(new Insets(10, 10, 10 ,10));
	        
	        Label s5 = new Label();
	        s5.setStyle("-fx-text-fill: #ffffff;");
	        s5.setText("Enter E-mail Address");
	        s5.setPadding(new Insets(5,0,0,0));
	        
	        final TextField search5 = new TextField();
	        search5.setStyle("-fx-background-color: #ffaaaa;-fx-prompt-text-fill: #ffffff;-fx-text-fill: #3a3a3a;");
	        search5.setPromptText("Enter E-mail Address");
	        search5.setAlignment(Pos.BASELINE_LEFT);
	        search5.setText(null);
	        search5.setMinWidth(400);
	        
	        part5.getChildren().addAll(s5, search5);
	        
	        HBox part3 = new HBox();
	        part3.setSpacing(30);
	        part3.setPadding(new Insets(10, 10, 10 ,10));
	        
	        part3.getChildren().add(accept);	        
	        ans.getChildren().addAll(part1, part2, part4, part5, part3);
	        
	        accept.setOnAction(f ->
	        {
	        	Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Information Dialog");
                alert.setHeaderText(null);
                
                AddWareHouseAdminThread thread = new AddWareHouseAdminThread();
                thread.start();
                
                alert.setContentText("Ware House Admin Created Successfully");
                alert.initStyle(StageStyle.UNDECORATED);
                alert.showAndWait();
                
                while(ans.getChildren().size()!=0)
                {
                	ans.getChildren().remove(0);
                }
                ans.getChildren().add(grid);
	        });        	
		});
		byid.setOnAction(e ->
		{
			VBox outerpart = new VBox();
			while(ans.getChildren().size()!=1)
			{
				ans.getChildren().remove(1);
			}
			Label pass = new Label(" Add StoreHouse Admin");
	        pass.setStyle("-fx-text-fill: #000000;-fx-font-size: 20;"); 
	        pass.setPadding(new Insets(20, 20, 20, 30));
	        pass.setAlignment(Pos.CENTER_RIGHT);
	        Separator line = new Separator();
	        line.setOrientation(Orientation.HORIZONTAL);
	        ans.getChildren().add(pass);
	        ans.getChildren().add(line);
	        
	        HBox part1 = new HBox();
	        part1.setStyle("-fx-background-color: #3a3a3a;");
	        part1.setSpacing(63);
	        part1.setPadding(new Insets(10, 10, 10 ,10));
	        
	        Label s = new Label();
	        s.setText("Enter First Name ");
	        s.setStyle("-fx-text-fill: #ffffff;");
	        s.setPadding(new Insets(5,25,0,0));
	        
	        final TextField search = new TextField();
	        search.setStyle("-fx-background-color: #ffaaaa;-fx-prompt-text-fill: #ffffff;-fx-text-fill: #3a3a3a;");
	        search.setPromptText("Enter First Name");
	        search.setAlignment(Pos.BASELINE_LEFT);
	        search.setText(null);
	        search.setMinWidth(400);
	        
	        part1.getChildren().addAll(s, search);
	        
	        Button accept = new Button();
	        accept.setText("Add New Admin");
	        accept.setId("GeneralButtonGreen");
	        accept.setPadding(new Insets(10, 10, 10 ,10));
	        accept.setAlignment(Pos.BASELINE_LEFT);
	        
	        HBox part2 = new HBox();
	        part2.setSpacing(90);
	        part2.setPadding(new Insets(10, 10, 10 ,10));
	        
	        Label s1 = new Label();
	        s1.setText("Enter Last Name");
	        s1.setPadding(new Insets(5,0,0,0));
	        
	        final TextField search1 = new TextField();
	        search1.setStyle("-fx-background-color: #ffaaaa;-fx-prompt-text-fill: #ffffff;-fx-text-fill: #3a3a3a;");
	        search1.setPromptText("Enter Last Name");
	        search1.setAlignment(Pos.BASELINE_LEFT);
	        search1.setText(null);
	        search1.setMinWidth(400);
	        
	        part2.getChildren().addAll(s1, search1);
	        
	        HBox part4 = new HBox();
	        part4.setStyle("-fx-background-color: #3a3a3a;");
	        part4.setSpacing(40);
	        part4.setPadding(new Insets(10, 10, 10 ,10));
	        
	        Label s4 = new Label();
	        DatePicker date = new DatePicker();
	        s4.setText("Enter DOB(dd/mm/yyyy)");
	        s4.setStyle("-fx-text-fill: #ffffff;");
	        s4.setPadding(new Insets(5,0,0,0));
	        
	        final TextField search4 = new TextField();
	        search4.setStyle("-fx-background-color: #ffaaaa;-fx-prompt-text-fill: #ffffff;-fx-text-fill: #3a3a3a;");
	        search4.setPromptText("dd / mm / yyyy");
	        search4.setAlignment(Pos.BASELINE_LEFT);
	        search4.setText(null);
	        search4.setMinWidth(400);
	        
	        part4.getChildren().addAll(s4, date);
	        
	        HBox part5 = new HBox();
	        part5.setSpacing(60);
	        part5.setPadding(new Insets(10, 10, 10 ,10));
	        
	        Label s5 = new Label();
	        s5.setText("Enter E-mail Address");
	        s5.setPadding(new Insets(5,0,0,0));
	        
	        final TextField search5 = new TextField();
	        search5.setStyle("-fx-background-color: #ffaaaa;-fx-prompt-text-fill: #ffffff;-fx-text-fill: #3a3a3a;");
	        search5.setPromptText("Enter E-mail Address");
	        search5.setAlignment(Pos.BASELINE_LEFT);
	        search5.setText(null);
	        search5.setMinWidth(400);
	        
	        part5.getChildren().addAll(s5, search5);
	        
	        HBox part3 = new HBox();
	        part3.setSpacing(30);
	        part3.setPadding(new Insets(10, 10, 10 ,10));
	        
	        part3.getChildren().add(accept);	        
	        ans.getChildren().addAll(part1, part2, part4, part5, part3);
	        
	        accept.setOnAction(f ->
	        {
	        	Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Information Dialog");
                alert.setHeaderText(null);
                
                AddStoreHouseAdminThread thread = new AddStoreHouseAdminThread();
                thread.start();
                
                alert.setContentText("Store House Created Successfully");
                alert.initStyle(StageStyle.UNDECORATED);
                alert.showAndWait();
                
                while(ans.getChildren().size()!=0)
                {
                	ans.getChildren().remove(0);
                }
                ans.getChildren().add(grid);
	        }); 
	        
		});
    	return ans;
    }
    public static VBox AddStore()
    {
    	VBox ans = new VBox();
    	ans.getStylesheets().add("button.css");
    	GridPane grid = new GridPane();
    	grid.setHgap(20);
    	grid.setVgap(10);
    	grid.setPadding(new Insets(50,10,10,20));
    	grid.getStylesheets().add("button.css");
        grid.setStyle("-fx-background-color: #3a3a3a;");
        
        Button bypath = new Button();
        bypath.setText("Ware House");
        bypath.setId("GeneralButton");
        bypath.setMinWidth(410);
		
		Button byid = new Button();
		byid.setText("Store House");
		byid.setId("GeneralButton");
		byid.setMinWidth(410);
    	
		grid.add(bypath, 1, 0);
		grid.add(byid, 2, 0);
		grid.setHalignment(byid, HPos.CENTER);
		grid.setHalignment(bypath, HPos.CENTER);
		grid.setPadding(new Insets(50, 10, 50, 10));
		ans.getChildren().add(grid);
		
		bypath.setOnAction(e ->
		{
			VBox outerpart = new VBox();
			while(ans.getChildren().size()!=1)
			{
				ans.getChildren().remove(1);
			}
			Label pass = new Label(" Add Ware House");
	        pass.setStyle("-fx-text-fill: #000000;-fx-font-size: 20;"); 
	        pass.setPadding(new Insets(20, 20, 20, 30));
	        pass.setAlignment(Pos.CENTER_RIGHT);
	        Separator line = new Separator();
	        line.setOrientation(Orientation.HORIZONTAL);
	        ans.getChildren().add(pass);
	        ans.getChildren().add(line);
	        
	        HBox part1 = new HBox();
	        part1.setStyle("-fx-background-color: #ffffff;");
	        part1.setSpacing(63);
	        part1.setPadding(new Insets(10, 10, 10 ,10));
	        
	        Label s = new Label();
//	        s.setText("Enter First Name ");
	        s.setText("Enter State      ");
	        s.setStyle("-fx-text-fill: #3a3a3a;");
	        s.setPadding(new Insets(5,25,0,0));
	        
	        final TextField search = new TextField();
	        search.setStyle("-fx-background-color: #ffaaaa;-fx-prompt-text-fill: #ffffff;-fx-text-fill: #3a3a3a;");
	        search.setPromptText("Enter State");
	        search.setAlignment(Pos.BASELINE_LEFT);
	        search.setText(null);
	        search.setMinWidth(400);
	        
	        part1.getChildren().addAll(s, search);
	        
	        Button accept = new Button();
	        accept.setText("Add New WareHouse");
	        accept.setId("GeneralButtonGreen");
	        accept.setPadding(new Insets(10, 10, 10 ,10));
	        accept.setAlignment(Pos.BASELINE_LEFT);
	        
	        HBox part2 = new HBox();
	        part2.setStyle("-fx-background-color: #3a3a3a;");
	        part2.setSpacing(90);
	        part2.setPadding(new Insets(10, 10, 10 ,10));
	        
	        Label s1 = new Label();
//	        s1.setText("Enter Last Name");
	        s1.setText("Enter City     ");
	        s1.setStyle("-fx-text-fill: #ffffff;");
	        s1.setPadding(new Insets(5,0,0,0));
	        
	        final TextField search1 = new TextField();
	        search1.setStyle("-fx-background-color: #ffaaaa;-fx-prompt-text-fill: #ffffff;-fx-text-fill: #3a3a3a;");
	        search1.setPromptText("Enter City");
	        search1.setAlignment(Pos.BASELINE_LEFT);
	        search1.setText(null);
	        search1.setMinWidth(400);
	        
	        part2.getChildren().addAll(s1, search1);
	        
	        HBox part3 = new HBox();
	        part3.setSpacing(30);
	        part3.setPadding(new Insets(10, 10, 10 ,10));
	        
	        part3.getChildren().add(accept);	        
	        ans.getChildren().addAll(part1, part2, part3);
	        
	        accept.setOnAction(f ->
	        {
	        	Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Information Dialog");
                alert.setHeaderText(null);
                AddWareHouseThread thread = new AddWareHouseThread(search.getText(), search1.getText());
                thread.start();
                alert.setContentText("Created Successfully");
                alert.initStyle(StageStyle.UNDECORATED);
                alert.showAndWait();
                
                while(ans.getChildren().size()!=0)
                {
                	ans.getChildren().remove(0);
                }
                ans.getChildren().add(grid);
	        });        	
		});
		byid.setOnAction(e ->
		{
			VBox outerpart = new VBox();
			while(ans.getChildren().size()!=1)
			{
				ans.getChildren().remove(1);
			}
			Label pass = new Label(" Add Store House");
	        pass.setStyle("-fx-text-fill: #000000;-fx-font-size: 20;"); 
	        pass.setPadding(new Insets(20, 20, 20, 30));
	        pass.setAlignment(Pos.CENTER_RIGHT);
	        Separator line = new Separator();
	        line.setOrientation(Orientation.HORIZONTAL);
	        ans.getChildren().add(pass);
	        ans.getChildren().add(line);
	        
	        HBox part1 = new HBox();
	        part1.setStyle("-fx-background-color: #3a3a3a;");
	        part1.setSpacing(63);
	        part1.setPadding(new Insets(10, 10, 10 ,10));
	        
	        Label s = new Label();
//	        s.setText("Enter First Name ");
	        s.setText("Enter State      ");
	        s.setStyle("-fx-text-fill: #ffffff;");
	        s.setPadding(new Insets(5,25,0,0));
	        
	        final TextField search = new TextField();
	        search.setStyle("-fx-background-color: #ffaaaa;-fx-prompt-text-fill: #ffffff;-fx-text-fill: #3a3a3a;");
	        search.setPromptText("Enter State");
	        search.setAlignment(Pos.BASELINE_LEFT);
	        search.setText(null);
	        search.setMinWidth(400);
	        
	        part1.getChildren().addAll(s, search);
	        
	        Button accept = new Button();
	        accept.setText("Add New Store");
//	        accept.setText("Add City     ");
	        accept.setId("GeneralButtonGreen");
	        accept.setPadding(new Insets(10, 10, 10 ,10));
	        accept.setAlignment(Pos.BASELINE_LEFT);
	        
	        HBox part2 = new HBox();
	        part2.setSpacing(90);
	        part2.setPadding(new Insets(10, 10, 10 ,10));
	        
	        Label s1 = new Label();
//	        s1.setText("Enter Last Name");
	        s1.setText("Enter City     ");
	        s1.setPadding(new Insets(5,0,0,0));
	        
	        final TextField search1 = new TextField();
	        search1.setStyle("-fx-background-color: #ffaaaa;-fx-prompt-text-fill: #ffffff;-fx-text-fill: #3a3a3a;");
	        search1.setPromptText("Enter City");
	        search1.setAlignment(Pos.BASELINE_LEFT);
	        search1.setText(null);
	        search1.setMinWidth(400);
	        
	        part2.getChildren().addAll(s1, search1);
	        
	        HBox part3 = new HBox();
	        part3.setSpacing(30);
	        part3.setPadding(new Insets(10, 10, 10 ,10));
	        
	        part3.getChildren().add(accept);	        
	        ans.getChildren().addAll(part1, part2, part3);
	        
	        accept.setOnAction(f ->
	        {
	        	Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Information Dialog");
                alert.setHeaderText(null);
                AddStoreHouseThread thread = new AddStoreHouseThread(search.getText(), search1.getText());
                thread.start();
                alert.setContentText("Added Successfully");
                alert.initStyle(StageStyle.UNDECORATED);
                alert.showAndWait();
                
                while(ans.getChildren().size()!=0)
                {
                	ans.getChildren().remove(0);
                }
                ans.getChildren().add(grid);
	        }); 
	        
		});
    	return ans;
    }
    public static VBox HomePanel(ArrayList<product> order) 
    {
    	VBox ans = new VBox();
    	ans.setSpacing(20);
    	ans.getStylesheets().add("button.css");
    	ans.setPrefSize(1500, 1500);
    	ans.setStyle("-fx-background-color: #ffffff;");
    	
    	VBox box = new VBox();
    	
    	Separator line = new Separator();
        line.setOrientation(Orientation.HORIZONTAL);
        Separator line1 = new Separator();
        line.setOrientation(Orientation.HORIZONTAL);
        Separator line2 = new Separator();
        line.setOrientation(Orientation.HORIZONTAL);
    	
    	Label s = new Label();
        s.setText("WELCOME");
        s.setPadding(new Insets(10,0,0,10));
        s.setPrefSize(200, 50);
        s.setStyle("-fx-text-fill: #3a3a3a; -fx-font-size: 30; -fx-background-color: #ffffff;");
        
        ans.getChildren().addAll(s, line);
        
        Label s1 = new Label();
        String str = "Admin : " + "V3N0M \t Post : " + "Chief Executive Officer\n";
        s1.setText(str);
        s1.setPadding(new Insets(10,0,0,10));
        s1.setPrefSize(600, 100);
        s1.setStyle("-fx-text-fill: #3a3a3a; -fx-font-size: 20; -fx-background-color: #ffffff;");
    	
        ans.getChildren().addAll(s1, line1);
        
        Label s2 = new Label();
        String st = "WareHouseId : 121\tRating : 3.9"
        		+ "\n\nState : "+"Delhi\tCity : New Delhi"
        				+ "\n\nStatus : OK";
        s2.setText(st);
        s2.setPadding(new Insets(10,0,0,10));
        s1.setPrefSize(900, 100);
        s2.setStyle("-fx-text-fill: #3a3a3a; -fx-font-size: 20; -fx-background-color: #ffffff;");
        
//        ans.getChildren().addAll(s2, line2);
    	return ans;
    }
}
//Agar warehouse delete kar diya toh 