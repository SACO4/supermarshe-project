import java.util.*;
interface Admin 
{
	public boolean CheckForCredentials(String id, String pass);
	public void InsertByPath(String path, String product) throws DuplicacyException;
	public void InsertById(String p, long c) throws DuplicacyException;
	public void ModifyById(double cost, int q, long c);
	public boolean DeleteById(long c);
	public Category SearchByName(String n) throws ProductNotFoundException;
	public Category SearchById(long c) throws ProductNotFoundException;
	
}
public class admin
{

}
