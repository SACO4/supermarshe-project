import java.io.Serializable;
import java.util.*;

class Customer extends SuperUser implements Serializable
{
	double funds;
	String identity;
	ArrayList<Cart> shopping_list;
	String pass;
	public Customer(double p, String id, String word)
	{
		this.setPass(word);
		this.setFunds(p);
		this.setIdentity(id);
		shopping_list = new ArrayList<Cart>();
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public String getIdentity() {
		return identity;
	}
	public void setIdentity(String identity) {
		this.identity = identity;
	}
	public Customer(String id)
	{
		this.setFunds(0);
		this.setIdentity(id);
		shopping_list = new ArrayList<Cart>();
	}
	public void Insert(long n, int qty) 
	{
		Category c = SearchById(n);
		Cart product = new Cart(c.getType(), qty, c.getId());
		shopping_list.add(product);
	}
	public void Insert(String n, int qty)
	{
		Category c = SearchByName(n);
		Cart product = new Cart(c.getType(), qty, c.getId());
		shopping_list.add(product);
	}
	public void Delete(long n)
	{
		for(Cart c : shopping_list)
		{
			if(c.getId()==n)
			{
				c.setQty(c.getQty()-1);
			}
		}
	}
	public void Delete(String n)
	{
		for(Cart c : shopping_list)
		{
			if(c.getType().equals(n))
			{
				c.setQty(c.getQty()-1);
			}
		}
	}
	public void AddFund(Double d)
	{
		this.setFunds(this.getFunds()+d);
	}	
	public void CheckOut(admin akki)
	{

	}
	public ArrayList<Cart> getShopping_list() {
		return shopping_list;
	}
	public void setShopping_list(ArrayList<Cart> list) {
		this.shopping_list = list;
	}
	public double getFunds() {
		return funds;
	}

	public void setFunds(double funds) {
		this.funds = funds;
	}
}
public class customer extends SuperUser 
{

}
