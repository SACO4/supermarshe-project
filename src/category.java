import java.io.*;
import java.util.*;
class Category extends DataBase implements Serializable
{
	private String type;
	private long qty;
	private double prize;
	private long id;
	private static long code = 0;
	
	public static HashMap<String, Long> Read() throws IOException, ClassNotFoundException
	{
		HashMap<String, Long> ans = null;
		File f = new File("DataBase.txt");
		if(f.exists())
		{
			FileInputStream obj = new FileInputStream(f);
			ObjectInputStream out = new ObjectInputStream(obj);
			ans = (HashMap<String, Long>) out.readObject();
		}
		else 
		{
			ans = new HashMap<String, Long>();
		}
		return ans;
	}
	public static void Write(HashMap<String, Long> map) throws IOException
	{
		File f = new File("DataBase.txt");
		if(f.exists())
		{
			FileOutputStream obj = new FileOutputStream(f);
			ObjectOutputStream out = new ObjectOutputStream(obj);
			out.writeObject(map);
		}
		else
		{
			FileOutputStream obj = new FileOutputStream(f);
			ObjectOutputStream out = new ObjectOutputStream(obj);
			out.writeObject(map);
		}
	}
	public Category(String n, int num, double p)
	{
		super();
		
		HashMap<String, Long> allproduct = null;
		try 
		{
			allproduct = Category.Read();
		} 
		catch (ClassNotFoundException | IOException e) 
		{
			System.out.println("Unable to read");
		}
		if(allproduct.containsKey(n)==true)
		{
			this.setId(allproduct.get(n));
		}
		else
		{
			code++;
			this.setId(code);
			allproduct.put(n, code);
		}
		this.setType(n);
		this.setQty(num);
		this.setPrize(p);
		System.out.println(allproduct+"Ye h");
		try 
		{
			Category.Write(allproduct);
		} 
		catch (IOException e) 
		{
			System.out.println("Can not Write");
		}
	}
	public Category()
	{
		super();
		this.setId(code);
		this.setPrize(-1.0);
		this.setQty(-1);
		this.setType("");
	}
	public Category(String p, long q , long i)
	{
		this.setType(p);
		this.setId(i);
		this.setQty(q);
	}
	public Category(String p, long q)
	{
		HashMap<String, Long> allproduct = null;
		try 
		{
			allproduct = Category.Read();
		} 
		catch (ClassNotFoundException | IOException e) 
		{
			System.out.println("Unable to read");
		}
		System.out.println(p+" "+q+" "+allproduct);
		this.setType(p);
		this.setId(allproduct.get(p));
		this.setQty(q);
	}
	public Category(String n, long q, long i, double p)
	{
		this.setType(n);
		this.setQty(q);
		this.setId(i);
		this.setPrize(p);

	}
	public Category(String n)
	{
		super();
		HashMap<String, Long> allproduct = null;
		try
		{
			allproduct = Category.Read();
		} 
		catch (ClassNotFoundException | IOException e) 
		{
			System.out.println("Unable to read");
		}
		System.out.println(allproduct+"Hello World !");
		if(allproduct.containsKey(n)==true)
		{
			this.setId(allproduct.get(n));
		}
		else
		{
			code++;
			this.setId(allproduct.size()+1);
			allproduct.put(n, code);
		}
		this.setPrize(-1.0);
		this.setQty(-1);
		this.setType(n);
		System.out.println(allproduct);
		try {
			Category.Write(allproduct);
		} catch (IOException e) {
		}
	}
	public String toString()
	{
		return(this.getType()+" " + this.getQty() + " " + this.getPrize()+" "+this.getId());
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public long getQty() {
		return qty;
	}
	public void setQty(long qty) {
		this.qty = qty;
	}
	public double getPrize() {
		return prize;
	}
	public void setPrize(double prize) {
		this.prize = prize;
	}
	public long getId() 
	{
		return id;
	}
	public void setId(long id) 
	{
		this.id = id;
	}
	public static long getCode() 
	{
		return code;
	}
	public static void setCode(long code) 
	{
		Category.code = code;
	}
}
public class category 
{

}


//e
//d
//d
//12
//500
//d
//e
//15
//600
//d
//f
//16
//700
//e