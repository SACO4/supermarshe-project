import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
class remove extends Thread
{
	SuperUser akki;
	StoreHouse house ;
	long id;
	boolean condition = true;
	public remove(StoreHouse obj, long i)
	{
		this.house = obj;
		this.id = i;
	}
	public void run()
	{
		//********************************************************************
		SuperUser temp = null;
		try
		{
			File data = new File("SuperUserData.txt");
			if(data.exists())
			{
				FileInputStream file = new FileInputStream(data);
				ObjectInputStream in = new ObjectInputStream(file);
				temp = (SuperUser) in.readObject();
				in.close();
				file.close();
			}
			System.out.println("Reading Successful");
		} 
		catch (IOException | ClassNotFoundException e) 
		{
			e.printStackTrace();
		}
		finally
		{
			if(temp == null)
			{
				akki = new SuperUser();
			}
			else
			{
				akki = temp;
			}
		}
		//********************************************************************
		boolean ch = false;
		for(StoreHouse a : akki.getStores())
		{
			if(a.getStorehouseid() == house.getStorehouseid())
			{
				house = a ; 
				int s = house.getAdmin().getProductid().size();
				System.out.println(house.getAdmin().getProductid()+" "+house);
				house.getAdmin().getProductid().remove(id);
				if(s != house.getAdmin().getProductid().size())
				{
					ch = true;
				}
				System.out.println(house.getAdmin().getProductid()+" "+house);
			}
		}
		if(ch == false)
		{
			condition = false;
		}
		//********************************************************************
		try
		{
			File data = new File("SuperUserData.txt");
			if(data.exists())
			{
				FileOutputStream file = new FileOutputStream(data);
				ObjectOutputStream out = new ObjectOutputStream(file);
				
				data.delete();
				
				data = new File("SuperUserData.txt");
				file = new FileOutputStream(data);
				out = new ObjectOutputStream(file);
				
				out.writeObject(akki);
				out.close();
				file.close();
			}
			else
			{
				FileOutputStream file = new FileOutputStream(data);
				ObjectOutputStream out = new ObjectOutputStream(file);
				out.writeObject(akki);
				out.close();
				file.close();
			}
			System.out.println("Writing Successful");
		}
		catch(IOException e)
		{
			e.printStackTrace();
//			System.out.println("Error While Writing the Data");
		}
		//********************************************************************
	}
}
class modifyinstore extends Thread
{
	SuperUser akki;
	StoreHouse house ;
	double cost;
	String n;
	long id;
	boolean condition = true;
	public modifyinstore(StoreHouse obj, double c , String m, long i)
	{
		this.house = obj;
		this.cost = c;
		this.n = m;
		this.id = i;
	}
	public void run()
	{
		//********************************************************************
		SuperUser temp = null;
		try
		{
			File data = new File("SuperUserData.txt");
			if(data.exists())
			{
				FileInputStream file = new FileInputStream(data);
				ObjectInputStream in = new ObjectInputStream(file);
				temp = (SuperUser) in.readObject();
				in.close();
				file.close();
			}
			System.out.println("Reading Successful");
		} 
		catch (IOException | ClassNotFoundException e) 
		{
			e.printStackTrace();
		}
		finally
		{
			if(temp == null)
			{
				akki = new SuperUser();
			}
			else
			{
				akki = temp;
			}
		}
		//********************************************************************
		boolean ch = false;
		for(StoreHouse a : akki.getStores())
		{
			if(a.getStorehouseid() == house.getStorehouseid())
			{
				ch = true;
				house = a ; 
				System.out.println(house.getAdmin().getProductid()+" "+house);
				house.getAdmin().getProductid().get(id).setPrize(cost);
				house.getAdmin().getProductid().get(id).setType(n);
			}
		}
		if(ch == false)
		{
			condition = true;
		}
		//********************************************************************
		try
		{
			File data = new File("SuperUserData.txt");
			if(data.exists())
			{
				FileOutputStream file = new FileOutputStream(data);
				ObjectOutputStream out = new ObjectOutputStream(file);
				
				data.delete();
				
				data = new File("SuperUserData.txt");
				file = new FileOutputStream(data);
				out = new ObjectOutputStream(file);
				
				out.writeObject(akki);
				out.close();
				file.close();
			}
			else
			{
				FileOutputStream file = new FileOutputStream(data);
				ObjectOutputStream out = new ObjectOutputStream(file);
				out.writeObject(akki);
				out.close();
				file.close();
			}
			System.out.println("Writing Successful");
		}
		catch(IOException e)
		{
			e.printStackTrace();
//			System.out.println("Error While Writing the Data");
		}
		//********************************************************************
	}
}
class searchinstore extends Thread
{
	SuperUser akki;
	String prd ;
	StoreHouse house;
	ArrayList<Category> ans = new ArrayList<Category>();
	public searchinstore(String part, StoreHouse p)
	{
		this.prd = part;
		this.house = p;
	}
	public void run()
	{
		//********************************************************************
		SuperUser temp = null;
		try
		{
			File data = new File("SuperUserData.txt");
			if(data.exists())
			{
				FileInputStream file = new FileInputStream(data);
				ObjectInputStream in = new ObjectInputStream(file);
				temp = (SuperUser) in.readObject();
				in.close();
				file.close();
			}
			System.out.println("Reading Successful");
		} 
		catch (IOException | ClassNotFoundException e) 
		{
			e.printStackTrace();
		}
		finally
		{
			if(temp == null)
			{
				akki = new SuperUser();
			}
			else
			{
				akki = temp;
			}
		}
		//********************************************************************
		ArrayList<Category> shop = new ArrayList<Category>();
		for(StoreHouse a : akki.getStores())
		{
			if(a.getStorehouseid() == house.getStorehouseid())
			{
				house = a ;
				try 
				{
					System.out.println("//"+a.getStorehouseid());
					this.ans = a.PartialSearch(prd, a.getStorehouseid());
				} 
				catch (ProductNotFoundException e) 
				{
					System.out.println("Product Not Found");
				}
			}
		}
		//********************************************************************
		try
		{
			File data = new File("SuperUserData.txt");
			if(data.exists())
			{
				FileOutputStream file = new FileOutputStream(data);
				ObjectOutputStream out = new ObjectOutputStream(file);
				
				data.delete();
				
				data = new File("SuperUserData.txt");
				file = new FileOutputStream(data);
				out = new ObjectOutputStream(file);
				
				out.writeObject(akki);
				out.close();
				file.close();
			}
			else
			{
				FileOutputStream file = new FileOutputStream(data);
				ObjectOutputStream out = new ObjectOutputStream(file);
				out.writeObject(akki);
				out.close();
				file.close();
			}
			System.out.println("Writing Successful");
		}
		catch(IOException e)
		{
			e.printStackTrace();
//			System.out.println("Error While Writing the Data");
		}
		//********************************************************************
	}
}
class order extends Thread
{
	ArrayList<Category> list;
	SuperUser akki;
	StoreHouse house;
	
	public order(ArrayList<Category> obj, StoreHouse h)
	{
		this.list = obj;
		this.house = h;
	}
	public void run()
	{
		//********************************************************************
		SuperUser temp = null;
		try
		{
			File data = new File("SuperUserData.txt");
			if(data.exists())
			{
				FileInputStream file = new FileInputStream(data);
				ObjectInputStream in = new ObjectInputStream(file);
				temp = (SuperUser) in.readObject();
				in.close();
				file.close();
			}
			System.out.println("Reading Successful");
		} 
		catch (IOException | ClassNotFoundException e) 
		{
			e.printStackTrace();
		}
		finally
		{
			if(temp == null)
			{
				akki = new SuperUser();
			}
			else
			{
				akki = temp;
			}
		}
		//********************************************************************
		ArrayList<Category> shop = new ArrayList<Category>();
		WareHouse like = null;
		for(StoreHouse a : akki.getStores())
		{
			if(a.getId() == house.getId())
			{
				System.out.println(house);
				house = a ;
				like = house.GenerateOrder(list, akki, house);
				if(like!=null)
				{
					for(WareHouse a1 : akki.getWarehouses())
					{
						if(a1.getWarehouseid() == like.getWarehouseid())
						{
							a1.getAdmin().setOrder(list);
							a1.getAdmin().setRequest(house.getAdmin());
						}
					}
				}
				
			}
		}
		if(like!=null)
		{
			for(WareHouse a : akki.getWarehouses())
			{
				if(a.getWarehouseid() == like.getWarehouseid())
				{
					like = a ;
					for(Category y : list)
					{
						long t = a.getProductid().get(y.getId()).getQty();
						a.getProductid().get(y.getId()).setQty(t - y.getQty());
						
					}
				}
			}
		}
		//********************************************************************
		try
		{
			File data = new File("SuperUserData.txt");
			if(data.exists())
			{
				FileOutputStream file = new FileOutputStream(data);
				ObjectOutputStream out = new ObjectOutputStream(file);
				
				data.delete();
				
				data = new File("SuperUserData.txt");
				file = new FileOutputStream(data);
				out = new ObjectOutputStream(file);
				
				out.writeObject(akki);
				out.close();
				file.close();
			}
			else
			{
				FileOutputStream file = new FileOutputStream(data);
				ObjectOutputStream out = new ObjectOutputStream(file);
				out.writeObject(akki);
				out.close();
				file.close();
			}
			System.out.println("Writing Successful");
		}
		catch(IOException e)
		{
			e.printStackTrace();
//			System.out.println("Error While Writing the Data");
		}
		//********************************************************************
	}
}
public class StoreHouseThreads 
{
	
}
