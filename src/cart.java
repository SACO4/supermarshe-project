import java.io.Serializable;

class Cart implements Serializable
{
	private String type;
	private int qty;
	private double bill;
	private long id;
	public Cart(String s, int n, long i)
	{
		this.setQty(n);
		this.setType(s);
		this.setId(i);
		this.setBill(0.0);
	}
	public Cart(int n, long i)
	{
		this.setQty(n);
		this.setId(i);
		System.out.println("Hello"+this.getQty()+" "+this.getId());
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public double getBill() {
		return bill;
	}
	public void setBill(double bill) {
		this.bill = bill;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getQty() {
		return qty;
	}
	public void setQty(int qty) {
		this.qty = qty;
	}
	public String toString()
	{
		return(this.getId()+" "+ this.getQty());
	}
}
public class cart {

}
