import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
//DataBase storeroot = new Category();
//StoreHouseAdmin storeadmin = new StoreHouseAdmin(storeroot);
//storeadmin.InsertByPath("Electronic Smartphone", "OnePlus");
//storeadmin.InsertByPath("Electronic Smartphone", "Vivo");
//storeadmin.InsertByPath("Electronic Toys", "Car");
////storeadmin.InsertByPath("Electronic Toys", "Vibrator");
//storeadmin.InsertById("Rubiks Cube", 5);
//storeadmin.InsertById("Chess", 5);
//storeadmin.ModifyById(45000, 6, 3);
//storeadmin.ModifyById(15000, 8, 4);
//storeadmin.ModifyById(5600, 3, 6);
//storeadmin.ModifyById(6000, 5, 7);
//storeadmin.ModifyById(12000, 15, 8);
//storeadmin.DeleteById(8);
//storeadmin.InsertById("Ludo", 5);
//Category c = storeadmin.SearchByName("Car");
//System.out.println(c+"**");
//c = storeadmin.SearchById(9);
//System.out.println(c+"*");
//disp(storeadmin.getRoot());
//
//System.out.println();
//System.out.println();
//
//DataBase warehouseroot = new Category();
//WareHouseAdmin warehouseadmin = new WareHouseAdmin(warehouseroot);
//warehouseadmin.InsertByPath("Garments Shirt", "Denim");
//warehouseadmin.InsertByPath("Garments Shirt", "Lee Cooper");
//warehouseadmin.InsertByPath("Garments Jeans", "Levis");
//warehouseadmin.InsertById("Killer Jeans", 14);
//warehouseadmin.InsertById("Flying Machine", 14);
//warehouseadmin.ModifyById(45000, 6, 13);
//warehouseadmin.ModifyById(15000, 8, 12);
//warehouseadmin.ModifyById(5600, 3, 15);
//warehouseadmin.DeleteById(15);
//warehouseadmin.InsertById("Lee Cooper", 14);
//Category d = warehouseadmin.SearchByName("Denim");
//System.out.println(d+"**");
//d = warehouseadmin.SearchById(15);
//System.out.println(d+"*");
//disp(warehouseadmin.getRoot());
public class MainCode 
{
	public static void disp(DataBase top)
	{
		ArrayList<Category> list = top.getArr();
		System.out.println(list+" "+top);
		for(Category a : list)
		{
			if(a.getArr().size()!=0)
			{
				disp(a);
			}
		}
	}
	public static String MD5(String input) 
    { 
        try 
        { 
            MessageDigest md = MessageDigest.getInstance("MD5"); 
            byte[] messageDigest = md.digest(input.getBytes()); 
            BigInteger no = new BigInteger(1, messageDigest); 
            String hashtext = no.toString(16); 
            while (hashtext.length() < 32) 
            { 
                hashtext = "0" + hashtext; 
            } 
            return hashtext; 
        }  
        catch (NoSuchAlgorithmException e) 
        { 
            throw new RuntimeException(e); 
        } 
    }
	public static void main(String args[])
	{
		
		SuperUser akki = new SuperUser();
		System.out.println(akki.getPassword()+" "+akki.getUsername());
		while(akki.CheckForCredentials(MD5("super"), MD5("1234")))
		{
			akki.CreateWareHouse("New Delhi", "Delhi");
			akki.CreateWareHouse("Kolkata", "West Bengal");
//			System.out.println(akki.getWarehouses());
			
			WareHouse admin = akki.getWarehouses().get(0);
			akki.CreateWareHouseAdmin();
			WareHouseAdmin ginni = akki.getWarehouse_admin().get(0);
//			System.out.println(akki.getStores());
			try 
			{
				akki.Link(2, 2);
			} 
			catch (InvalidIdException e1) 
			{
				System.out.println("Invalid Id Exception");
//				e1.getMessage();
//				e1.printStackTrace();
			}
			System.out.println(admin.getAdmin());
//			System.out.println(admin.getWarehouseid());
//			System.out.println(admin.getLocation());
//			System.out.println(admin.getUsername());
//			System.out.println(admin.getPassword());
			if(admin.getAdmin()!=null)
			{
				admin.InsertByPath("Electronic Smartphone", "OnePlus");
				admin.InsertByPath("Electronic Toys", "RC Car");
				admin.ModifyById(45000.0, 15, 3);
				disp(admin.getRoot());
			}
			
			admin = akki.getWarehouses().get(1);
//			System.out.println(admin.getWarehouseid());
//			System.out.println(admin.getLocation());
//			System.out.println(admin.getUsername());
//			System.out.println(admin.getPassword());
			if(admin.getAdmin()!=null)
			{
				admin.InsertByPath("Electronic Smartphone", "OnePlus");
				admin.InsertByPath("Electronic Toys", "RC Car");
				admin.ModifyById(45000.0, 15, 3);
				disp(admin.getRoot());
			}
			
			akki.CreateStoreHouse("Mumbai", "Maharashtra");
			
			
			StoreHouse admin1 = akki.getStores().get(0);
//			System.out.println(admin1.getStorehouseid());
//			System.out.println(admin1.getLocation());
//			System.out.println(admin1.getUsername());
//			System.out.println(admin1.getPassword());
			if(admin1.getAdmin() != null)
			{
				admin1.InsertByPath("Electronic Smartphone", "OnePlus");
				admin1.InsertByPath("Electronic Smartphone", "Vivo");
				admin1.ModifyById(45000.0, 15, 3);
				disp(admin1.getRoot());
			}
			try 
			{
				admin1.SearchByName("Samsung");
			} 
			catch (ProductNotFoundException e) 
			{
				System.out.println("Product Not Found Exception");
			}
			try 
			{
				System.out.println(admin1.SearchById(12344));
			} 
			catch (ProductNotFoundException e) 
			{
				System.out.println("Product Not Found Exception");
			}
//			akki.CreateWareHouse("Mumbai", "Maharashtra");
//			System.out.println(akki.getWarehouses());
//			admin = akki.getWarehouses().get(1);
//			System.out.println(admin.getLocation());
//			System.out.println(admin.getUsername());
//			System.out.println(admin.getPassword());
//			System.out.println(admin.getWarehouseid());
//			
//			akki.CreateStoreHouse("Mumbai", "Maharashtra");
//			System.out.println(akki.getStores());
//			StoreHouse admin1 = akki.getStores().get(0);
//			System.out.println(admin1.getLocation());
//			System.out.println(admin1.getUsername());
//			System.out.println(admin1.getPassword());
//			System.out.println(admin1.getWarehouseid());
//			
//			akki.CreateStoreHouse("Mumbai", "Maharashtra");
//			System.out.println(akki.getStores());
//			admin1 = akki.getStores().get(1);
//			System.out.println(admin1.getLocation());
//			System.out.println(admin1.getUsername());
//			System.out.println(admin1.getPassword());
//			System.out.println(admin1.getWarehouseid());
//			
//			akki.CreateStoreHouse("Mumbai", "Maharashtra");
//			System.out.println(akki.getStores());
//			admin1 = akki.getStores().get(2);
//			System.out.println(admin1.getLocation());
//			System.out.println(admin1.getUsername());
//			System.out.println(admin1.getPassword());
//			System.out.println(admin1.getWarehouseid());
	
			break;
		}
	}
}
