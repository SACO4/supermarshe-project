import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;

import javafx.application.Application;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.Separator;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCharacterCombination;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.Mnemonic;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.stage.Stage;
import javafx.stage.StageStyle;


public class w_admin extends Application 
{
    private double xOffset = 0;
    private double yOffset = 0;
    static WareHouse warehouse ;
    static String name ; 
    public static class product 
    {
    	String name1;
    	long qty1;
    	double cost1;
    	long id;
    	public product(String n,long q, double c, long i)
    	{
    		name1 = n;
    		qty1 = q;
    		cost1 = c;
    		id = i;
    	}
    	public String getName1() {
    		return name1;
    	}
    	public long getId() {
			return id;
		}
		public void setId(long id) {
			this.id = id;
		}
		public void setName1(String name1) {
    		this.name1 = name1;
    	}
    	public long getQty1() {
    		return qty1;
    	}
    	public void setQty1(long qty1) {
    		this.qty1 = qty1;
    	}
    	public double getCost1() {
    		return cost1;
    	}
    	public void setCost1(double cost1) {
    		this.cost1 = cost1;
    	}
    	public String toString()
    	{
    		return(this.name1+" "+this.qty1+" "+this.cost1);
    	}
    }
//    public static 
//    public w_admin()
//    {
//    	String[] bale = new String[5];    	
//    	main(bale);
////    	launch(args);0
//    }
    public static void main(String[] bale) {
        launch(bale);
    }
    public static void DisplayProduct(VBox grid,ObservableList<product> temp)
    {
    	 TableView<product> table = new TableView<product>();
    	 table.setStyle("-fx-background-color: #ff9393;");
//    	 table.setEditable(true);
    	 TableColumn idCol = new TableColumn("Product Id");
    	 idCol.setMinWidth(20);
    	 idCol.setCellValueFactory(
                 new PropertyValueFactory<product, String>("id"));
    	 
         TableColumn firstNameCol = new TableColumn("Product Name");
         firstNameCol.setMinWidth(250);
         firstNameCol.setCellValueFactory(
                 new PropertyValueFactory<product, String>("name1"));
  
         TableColumn lastNameCol = new TableColumn("Available Items");
         lastNameCol.setMinWidth(10);
         lastNameCol.setCellValueFactory(
                 new PropertyValueFactory<product, String>("qty1"));
  
         TableColumn emailCol = new TableColumn("Price(in ₹)");
         emailCol.setMinWidth(70);
         emailCol.setCellValueFactory(
                 new PropertyValueFactory<product, String>("cost1"));
         
         table.getColumns().addAll(idCol, firstNameCol, lastNameCol, emailCol);
         
//         grid.setPadding(new Insets(10,10,10,10));
         table.setItems(temp);
//         ((Group) scene.getRoot()).getChildren().addAll(grid);
         if(grid.getChildren().size()>1)
         {
        	 grid.getChildren().remove(grid.getChildren().size()-1);
         }
         grid.getChildren().add(table);
    }
    public static VBox SearchPanel()
    {
    	
    	VBox ans = new VBox();
    	
    	GridPane grid = new GridPane();
    	grid.setHgap(20);
    	grid.setVgap(10);
    	grid.setPadding(new Insets(50,10,50,20));
    	grid.getStylesheets().add("button.css");
        grid.setStyle("-fx-background-color: #3a3a3a;");
        Label s = new Label();
        s.setStyle("-fx-text-fill: #ffffff;");
        s.setText("Search");
        
        final TextField search = new TextField();
        search.setStyle("-fx-background-color: #ffaaaa;-fx-prompt-text-fill: #ffffff;-fx-text-fill: #3a3a3a;");
        search.setPromptText("Search Any Product");
        search.setAlignment(Pos.BASELINE_LEFT);
        search.setText("");
        search.setMinWidth(400);
        
        Button submit = new Button();
        submit.setText("Search");
		submit.setId("GeneralButton");
		submit.setMinWidth(150);
		
		
		Button clear = new Button();
        clear.setText("Cancel");
		clear.setId("GeneralButton");
		clear.setMinWidth(150);
		
		submit.setOnAction(e -> 
        {
        	MainCode.disp(warehouse.getRoot());;
        	searchinware thread = new searchinware(warehouse , search.getText());
        	thread.start();
        	try {
				thread.join();
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
        	ArrayList<Category> avail = thread.ans;
        	ArrayList<product> list = new ArrayList<product>();
        	for(Category a : avail)
        	{
        		list.add(new product(a.getType(), a.getQty(), a.getPrize(), a.getId()));
        	}
        	
        	ArrayList<product> data = new ArrayList<product>();
        	ObservableList<product> temp = FXCollections.observableArrayList();
        	String str = search.getText();
        	if(str!=null)
        	{
        		for(product p : list)
            	{
            		if(p.name1.toLowerCase().contains(str.toLowerCase())||str.toLowerCase().contains(p.name1.toLowerCase()))
            		{
            			data.add(p);
            			temp.add(new product(p.name1, p.qty1, p.cost1, p.id));
            		}
            	}
        		DisplayProduct(ans, temp);
        	}
        	else
        	{
        		for(product p : list)
        		{
        			data.add(p);
        			temp.add(new product(p.name1, p.qty1, p.cost1, p.id));
        		}
        		DisplayProduct(ans, temp);
        	}
  		});
  		clear.setOnAction(e ->
  		{
	  		search.setText("");
		});
        grid.setMargin(s, new Insets(0,0,0,00));
        grid.setMargin(search, new Insets(0,0,0,0));
        grid.setMargin(clear, new Insets(0,0,0,0));
        grid.setHalignment(s, HPos.CENTER);
        grid.setHalignment(search, HPos.CENTER);
        grid.add(s, 0, 0);
        grid.add(search, 1, 0);
        grid.add(submit, 2, 0);
        grid.add(clear, 3, 0);
        ans.getChildren().add(grid);
        return ans;
    }
    @Override
    public void start(final Stage primaryStage) throws FileNotFoundException {
        primaryStage.initStyle(StageStyle.UNDECORATED);
        
        BorderPane root = new BorderPane();
        final Tooltip tooltip = new Tooltip();
        tooltip.setText("\nClose\n");
        root.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });
        root.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                primaryStage.setX(event.getScreenX() - xOffset);
                primaryStage.setY(event.getScreenY() - yOffset);
            }
        });
        GridPane grid = new GridPane();
        grid.getStylesheets().add("button.css");
        grid.setStyle("-fx-background-color: #dc143c;");
        Button exit = new Button();
        Button mini = new Button();
        Label text = new Label();
        text.setText("Super Marché");
        text.setId("MarketName");
        mini.setId("OpenMinButton");
        exit.setId("OpenMinButton");
        exit.setTooltip(new Tooltip("Close"));
        mini.setTooltip(new Tooltip("Minimize"));
        exit.setText("x");
        mini.setText("-");
        exit.setOnAction(e -> primaryStage.close());
        mini.setOnAction(e -> {((Stage)((Button)e.getSource()).getScene().getWindow()).setIconified(true);});
        grid.setPadding(new Insets(5, 0, 10, 5));
        grid.setVgap(5); 
        grid.setHgap(5);
        ColumnConstraints col1 = new ColumnConstraints();
        col1.setPercentWidth(94);
        ColumnConstraints col2 = new ColumnConstraints();
        col2.setPercentWidth(3);
        ColumnConstraints col3 = new ColumnConstraints();
        col3.setPercentWidth(3);
        grid.getColumnConstraints().addAll(col1);
        text.setAlignment(Pos.TOP_LEFT);
        
        grid.add(exit, 2, 0);
        grid.add(mini, 1, 0);
        grid.add(text, 0, 0);
        root.setTop(grid);

        GridPane navigation = new GridPane();
        navigation.setMinHeight(500);
      	navigation.setMinWidth(300);
      	navigation.setMaxWidth(300);
        navigation.setAlignment(Pos.TOP_CENTER);
        navigation.getStylesheets().add("button.css");
        Label name = new Label();
        name.setStyle("-fx-text-fill: #ffffff;-fx-font-weight: bold;");
        name.setText("USERNAME");
        Label user = new Label();
        user.setStyle("-fx-text-fill: #ffffff;");
        user.setText(w_admin.name);
        user.setStyle("-fx-font-weight: bold;-fx-text-fill: #ffffff;-fx-font-size: 20;");
        Button nav1 = new Button();
        nav1.setId("NavigationButton");
        nav1.setText("Search");
        Button nav2 = new Button();
        nav2.setId("NavigationButton");
        nav2.setText("Add Product");
        Button nav3 = new Button();
        nav3.setId("NavigationButton");
        nav3.setText("Remove Product");
        Button nav4 = new Button();
        nav4.setId("NavigationButton");
        nav4.setText("Modify Product");
//        Button nav5 = new Button();
//        nav5.setId("NavigationButton");
//        nav5.setText("Generate Order");
        Button nav0 = new Button();
        nav0.setId("NavigationButton");
        nav0.setText("Home");
        Button nav6 = new Button();
        nav6.setId("NavigationButton");
        nav6.setText("Handle Order");
        Button nav7 = new Button();
        nav7.setId("GeneralButtonRed");
        nav7.setPrefSize(1000, 20);
        nav7.setText("Log Out");
        
        Image img = new Image(new FileInputStream("admin.png"));
        ImageView i = new ImageView(img);
        i.setFitHeight(230);
      	i.setFitWidth(230);
      	i.setPreserveRatio(true);
      	i.setStyle("-fx-background-color: #494949;");
      	
      	navigation.setHgap(0);
      	navigation.setPadding(new Insets(0,0,0,0));
      	navigation.setMargin(user, new Insets(20,10,10,10));
      	navigation.setMargin(i, new Insets(30,30,30,30));
      	navigation.setMargin(nav0, new Insets(60,10,0,10));
      	navigation.setMargin(nav1, new Insets(0,10,0,10));
      	navigation.setMargin(nav2, new Insets(0,10,0,10));
      	navigation.setMargin(nav3, new Insets(0,10,0,10));
      	navigation.setMargin(nav4, new Insets(0,10,0,10));
//      	navigation.setMargin(nav5, new Insets(0,10,0,10));
      	navigation.setMargin(nav6, new Insets(0,10,0,10));
      	navigation.setMargin(nav7, new Insets(60,10,0,10));
      	navigation.setVgap(0);
      	
      	navigation.add(i, 0, 1);
      	navigation.add(user, 0, 2);
      	navigation.add(nav0, 0, 3);
      	navigation.add(nav1, 0, 4);
      	navigation.add(nav2, 0, 5);
      	navigation.add(nav3, 0, 6);
      	navigation.add(nav4, 0, 7);
      	navigation.add(nav6, 0, 8);
      	navigation.add(nav7, 0, 9);
//      	navigation.add(nav7, 0, 10);
      	
      	navigation.setHalignment(i, HPos.CENTER);
      	navigation.setHalignment(user, HPos.CENTER);
      	navigation.setHalignment(nav0, HPos.CENTER);
      	navigation.setHalignment(nav1, HPos.CENTER);
      	navigation.setHalignment(nav2, HPos.CENTER);
      	navigation.setHalignment(nav3, HPos.CENTER);
      	navigation.setHalignment(nav4, HPos.CENTER);
//      	navigation.setHalignment(nav5, HPos.CENTER);
      	navigation.setHalignment(nav6, HPos.CENTER);
      	navigation.setHalignment(nav7, HPos.CENTER);
      	navigation.setStyle("-fx-background-color: #494949;");
      	
      	ArrayList<product> order = new ArrayList<product>();
      	order.add(new product("IPhone",10, 90000.0, 2));
      	order.add(new product("Vivo",12, 15000.0, 3));
      	order.add(new product("Redmi Note 4g",101, 5000.0, 4));
      	order.add(new product("Motorola",93, 40000.0, 5));
      	order.add(new product("Redmi 1s",93, 40000.0, 6));
      	order.add(new product("OnePlus 5t",15, 45000.0, 1));
      	order.add(new product("OnePlus 6t",15, 20000.0, 7));
      	order.add(new product("OnePlus 5",15, 45000.0, 8));
      	order.add(new product("OnePlus 6",15, 45000.0, 9));
      	order.add(new product("OnePlus 1",15, 45000.0, 10));
      	
      	
        root.setLeft(navigation);
        root.setCenter(HomePanel(order));
        nav0.setOnAction(e ->
        {
        	VBox temp = HomePanel(order);
        	root.setCenter(temp);
        });
        nav1.setOnAction(e ->
        {
        	VBox temp = SearchPanel();
        	root.setCenter(temp);
        });
        nav2.setOnAction(e ->
        {
        	VBox temp = AddProduct();
        	root.setCenter(temp);
        });
        nav3.setOnAction(e ->
        {
        	VBox temp = RemovePanel();
        	root.setCenter(temp);
        });
        nav4.setOnAction(e ->
        {
        	VBox temp = ModifyPanel();
        	root.setCenter(temp);
        });
//        nav5.setOnAction(e ->
//        {
//        	VBox temp = OrderPanel();
//        	root.setCenter(temp);
//        });
        nav6.setOnAction(e ->
        {
        	VBox temp = HandleOrder();
        	root.setCenter(temp);
        });
        nav7.setOnAction(e ->
        {
        	Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Information Dialog");
            alert.setHeaderText(null);
            alert.setContentText("Logged Out successfully !");
            alert.initStyle(StageStyle.UNDECORATED);
            alert.showAndWait();
            
            loginpage obj = new loginpage();
            
    		try 
    		{
    			primaryStage.close();
    			Stage n = new Stage();
				obj.start(n);
			} 
    		catch (FileNotFoundException e1) 
    		{
				e1.printStackTrace();
			}
        });
//        root.setLeft(i);
        GridPane foot = new GridPane();
        foot.setStyle("-fx-background-color: #dc143c;");
        foot.setPadding(new Insets(10, 10, 10, 20));
        name = new Label("©VENOM");
        name.setStyle("-fx-text-fill: #ffffff;");
        Label pass = new Label(" v 1.0 bETA");
        pass.setStyle("-fx-text-fill: #ffffff;");
        name.setAlignment(Pos.CENTER_LEFT);
        pass.setAlignment(Pos.CENTER_RIGHT);
        foot.add(name, 0, 0);
        foot.add(pass, 1, 0);
        root.setBottom(foot);
        
        Scene scene = new Scene(root, 1200, 900);
        primaryStage.setScene(scene);
        primaryStage.setMinHeight(900);
        primaryStage.setMinWidth(1200);
        primaryStage.show();
        if(warehouse.getAdmin().getOrder()!=null && warehouse.getAdmin().getOrder().size()!=0)
      	{
      		Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Information Dialog");
            alert.setHeaderText(null);
            if(warehouse.getOrder()!=null)
            {
            	alert.setContentText("You have "+warehouse.getOrder().size()+" pending orders.");
            }
            alert.initStyle(StageStyle.UNDECORATED);
            alert.showAndWait();
      	}
    }
    public static VBox HandleOrder() 
    {
    	ArrayList<Category> list = warehouse.getAdmin().getOrder();
    	ArrayList<product> order = new ArrayList<product>();
    	if(list!=null)
    	{
    		for(Category a : list)
    		{
    			order.add(new product(a.getType(), a.getQty(), a.getPrize(), a.getId()));
    		}
    	}
    	 
    	VBox ans = new VBox();
    	ans.setSpacing(20);
    	ans.getStylesheets().add("button.css");
    	ans.setPrefSize(500, 500);
    	ans.setStyle("-fx-background-color: #ffffff;");
    	Separator line = new Separator();
        line.setOrientation(Orientation.HORIZONTAL);
    	
    	Label s = new Label();
        s.setText("Pending Orders ");
        s.setPadding(new Insets(10,0,0,30));
        s.setPrefSize(300, 70);
        s.setStyle("-fx-text-fill: #3a3a3a; -fx-font-size: 30; -fx-background-color: #ffffff;");
        
        ans.getChildren().addAll(s, line);
    	
//        Label s0 = new Label();
//        s0.setText("Number of Pending Order : " + order.size());
//        s0.setPadding(new Insets(10,0,0,10));
//        s0.setPrefSize(200, 50);
//        s0.setStyle("-fx-text-fill: #3a3a3a;  -fx-background-color: #ffffff;");
//        
//        ans.getChildren().addAll(s0, line);
        
    	ScrollPane scroll = new ScrollPane();
    	VBox outerpart = new VBox();
    	
    	for(int i=0;i<order.size();i++)
		{
			HBox part = new HBox();
            part.setPrefSize(900, 50);
            part.setSpacing(50);
            
            if(i%2==0)
            	part.setStyle("-fx-background-color: #ffffff;-fx-background-color: #ffaaaa;");
            else
            	part.setStyle("-fx-background-color: #ffffff;");
            
            Label s1 = new Label();
            s1.setText("Id ");
            s1.setPadding(new Insets(10,0,0,10));
            s1.setPrefSize(100, 10);
            s1.setStyle("-fx-text-fill: #3a3a3a;");
            
            Label s2 = new Label();
            s2.setText(Long.toString(order.get(i).id));
            s2.setPadding(new Insets(10,0,0,10));
            s2.setPrefSize(100, 10);
            s2.setStyle("-fx-text-fill: #3a3a3a;");
            
            Label s3 = new Label();
            s3.setText("Quantites");
            s3.setPadding(new Insets(10,0,0,10));
            s3.setPrefSize(100, 10);
            s3.setStyle("-fx-text-fill: #3a3a3a;");

            Label s4 = new Label();
            s4.setText(Long.toString(order.get(i).qty1));
            s4.setPadding(new Insets(10,0,0,10));
            s4.setPrefSize(100, 10);
            s4.setStyle("-fx-text-fill: #3a3a3a;");
            
            part.getChildren().addAll(s1, s2, s3, s4);
            part.setPadding(new Insets(10,10,10,20));
            outerpart.getChildren().add(part);
            
		}
//    	DisplayProduct(ans, (ObservableList<product>) order);
    	scroll.setContent(outerpart);
    	ans.getChildren().add(scroll);
    	
    	if(order.size()!=0)
    	{
    		Button accept1 = new Button();
    		accept1.setText("Handle Order");
    		accept1.setId("GeneralButtonGreen");
    		accept1.setPadding(new Insets(10, 10, 10 ,10));
    		accept1.setAlignment(Pos.BASELINE_LEFT);
    		
    		ans.getChildren().add(accept1);
    		
    		accept1.setOnAction(g -> 
    		{
    			handle thread = new handle(warehouse);
    			thread.start();
    			
    			try 
    			{
					thread.join();
				} 
    			catch (InterruptedException e) 
    			{
    				System.out.println("Interrupted");
				}
    			
    			for(StoreHouse a : read.read().getStores())
    			{
//    				System.out.println(a.getAdmin().getProductid()+"Abe Jaa naa"+a);
    			}
    			Alert alert = new Alert(AlertType.INFORMATION);
    			alert.setTitle("Information Dialog");
                alert.setHeaderText(null);
                alert.setContentText("Are you sure ?");
                alert.initStyle(StageStyle.UNDECORATED);
                alert.showAndWait();
                
                while(order.size()!=0)
                {
                	order.remove(0);
                }
                ans.getChildren().remove(ans.getChildren().size()-1);
                ans.getChildren().remove(ans.getChildren().size()-1);
    		});
    	}
		
    	return ans;
    }
    public static VBox AddProduct()
    {
    	VBox ans = new VBox();
    	ans.getStylesheets().add("button.css");
    	GridPane grid = new GridPane();
    	grid.setHgap(20);
    	grid.setVgap(10);
    	grid.setPadding(new Insets(50,10,10,20));
    	grid.getStylesheets().add("button.css");
        grid.setStyle("-fx-background-color: #3a3a3a;");
        
        Button bypath = new Button();
        bypath.setText("Add By Path");
        bypath.setId("GeneralButton");
        bypath.setMinWidth(410);
		
		Button byid = new Button();
		byid.setText("Add By Id");
		byid.setId("GeneralButton");
		byid.setMinWidth(410);
    	
		grid.add(bypath, 1, 0);
		grid.add(byid, 2, 0);
		grid.setHalignment(byid, HPos.CENTER);
		grid.setHalignment(bypath, HPos.CENTER);
//		grid.setPadding(new Insets(TOP, RIGHT, BOTTOM , LEFT));
		grid.setPadding(new Insets(50, 10, 50, 10));
		ans.getChildren().add(grid);
		
		bypath.setOnAction(e ->
		{
			while(ans.getChildren().size()!=1)
			{
				ans.getChildren().remove(1);
			}
//			System.out.println(ans.getChildren().size());
//			ArrayList<ArrayList<String>> data = new ArrayList<ArrayList<String>>();
//			ArrayList<String> t = new ArrayList<String>();
//			t.add("Electronic");t.add("Non-Electronic");data.add(t);
//			t = new ArrayList<String>();
//			t.add("OnePlus");t.add("Vivo");t.add("IPhone");data.add(t);
//			t = new ArrayList<String>();
//			t.add("OnePlus New");t.add("OnePlus Old");data.add(t);
//			t = new ArrayList<String>();
//			t.add("OnePlus 5 Series");t.add("OnePlus 6 Series");t.add("OnePlus 3 Series");data.add(t);
//			t = new ArrayList<String>();
//			t.add("OnePlus 5t");t.add("OnePlus 5");data.add(t);
//			
//			System.out.println(data.get(0));
//			System.out.println(data.get(1));
//			System.out.println(data.get(2));
//			System.out.println(data.get(3));
//			System.out.println(data.get(4));
//			HashMap<String,String> data = new HashMap<String,String>();
//			data.put("Electronic", "SmartPhone");
//			data.put("SmartPhone", "OnePlus");data.put("SmartPhone", "Vivo");data.put("SmartPhone", "IPhone");
//			data.put("OnePlus", "OnePlus 5 series");data.put("OnePlus", "OnePlus 3 series");data.put("OnePlus", "OnePlus 6 series");
//			data.put("OnePlus 5 series", "OnePlus 5t");data.put("OnePlus 5 series", "OnePlus 5");
//			data.put("OnePlus 3 series", "OnePlus 3t");data.put("OnePlus 3 series", "OnePlus 3");
//			data.put("OnePlus 6 series", "OnePlus 6t");data.put("OnePlus 6 series", "OnePlus 6");

			Label pass = new Label("Add Product By Path");
	        pass.setStyle("-fx-text-fill: #000000;-fx-font-size: 20;"); 
	        pass.setPadding(new Insets(20, 20, 20, 30));
	        pass.setAlignment(Pos.CENTER);
	        Separator line = new Separator();
	        line.setOrientation(Orientation.HORIZONTAL);
	        if(ans.getChildren().size()==1)
	        {
	        	ans.getChildren().add(pass);
		        ans.getChildren().add(line);
	        }
	        	 
	        ScrollPane page = new ScrollPane();
	        page.setPrefSize(400, 500);
	        
	        ArrayList<String> path = new ArrayList<String>();
	        HBox part = new HBox();
	        part.setAlignment(Pos.CENTER);
	        VBox outerpart = new VBox();
	        
	        Label s = new Label();
	        s.setText("Length of Path");
	        s.setPadding(new Insets(5,0,0,0));
	        
	        final TextField search = new TextField();
	        search.setStyle("-fx-background-color: #ffaaaa;-fx-prompt-text-fill: #ffffff;-fx-text-fill: #3a3a3a;");
	        search.setPromptText("Length of Path");
	        search.setAlignment(Pos.BASELINE_LEFT);
	        search.setText("");
	        search.setMinWidth(400);
	        
	        
	        Button next = new Button();
        	next.setText("Submit");
        	next.setId("GeneralButtonGreen");

        	part.setSpacing(50);
        	part.setPadding(new Insets(5,20,5,10));
        	part.getChildren().addAll(s, search, next);
        	part.setAlignment(Pos.CENTER);
        	outerpart.getChildren().add(part);
        	
        	Button accept = new Button();
	        accept.setText("Add This Product");
	        accept.setId("GeneralButtonGreen");
	        
	        Button reject = new Button();
	        reject.setText("Reset");
	        reject.setId("GeneralButtonRed");
	        
	        final TextField[] search111 ;
	        
        	next.setOnAction(f ->
        	{
        		long n = Integer.parseInt(search.getText());
//        		System.out.println(n+"Number");
        		while(outerpart.getChildren().size()!=1)
        		{
        			outerpart.getChildren().remove(1);
        		}
        		final TextField[] search1 = new TextField[(int) n];
        		for(int i=0;i<n;i++)
        		{
        			VBox temp = new VBox();
//        			temp.setId("GeneralButton");
        			search1[i] = new TextField();
        	        search1[i].setStyle("-fx-background-color: #ffaaaa;-fx-prompt-text-fill: #ffffff;-fx-text-fill: #ffffff;");
        	        search1[i].setPromptText("Enter Category");
        	        search1[i].setAlignment(Pos.BASELINE_LEFT);
//        	        search1.setAlignment(Pos.CENTER);
        	        search1[i].setText("");
        	        search1[i].setMinWidth(900);
        	        
        	        Label arrow = new Label();
        	        arrow.setText("v");
        	        arrow.setAlignment(Pos.CENTER);
        	        arrow.setPadding(new Insets(5,0,5,0));
        	        if(i!=n-1)
        	        {
        	        	temp.getChildren().addAll(search1[i], arrow);
        	        }
        	        else
        	        {
        	        	temp.getChildren().addAll(search1[i]);
        	        }
        	        temp.setAlignment(Pos.CENTER);
        			outerpart.getChildren().add(temp);
        			if(i==n-1)
        			{
        				HBox last = new HBox();
        				last.setSpacing(50);
        				last.setPadding(new Insets(10, 0, 10, 20));
            	        last.getChildren().add(accept);
            	        outerpart.getChildren().add(last);
        			}
        		}
        		
        		
        		Label s1 = new Label();
    	        s1.setText("Length of Path");
    	        s1.setPadding(new Insets(5,0,0,0));
    	        
    	        final TextField search11 = new TextField();
    	        search11.setStyle("-fx-background-color: #ffaaaa;-fx-prompt-text-fill: #ffffff;-fx-text-fill: #3a3a3a;");
//    	        search1.setPromptText("Length of Path");
//    	        search1.setAlignment(Pos.BASELINE_LEFT);
    	        search11.setAlignment(Pos.CENTER);
    	        search11.setText("");
    	        search11.setMinWidth(400);
    	        
    	        Button next1 = new Button();
            	next1.setText("Submit");
            	next1.setId("GeneralButtonGreen");
            	
            	accept.setOnAction(g ->
            	{
            		String pp = "";
//            		System.out.println(Integer.parseInt(search.getText())+"FinalNumber");
            		String ppp = search1[Integer.parseInt(search.getText())-1].getText();
            		for(int i=0;i<search1.length-1;i++)
            		{
            			pp = pp + search1[i].getText() + " ";
            		}
            		pp = pp.substring(0, pp.length()-1);
            		
            		addproductbypath thread = new addproductbypath(warehouse, pp, ppp);
            		thread.start();
            		
            		try 
        			{
    					thread.join();
    				} 
        			catch (InterruptedException e1) 
        			{
        				System.out.println("Interrupted");
    				}
            		
            		Alert alert = new Alert(AlertType.INFORMATION);
                    alert.setTitle("Information Dialog");
                    alert.setHeaderText(null);
                    alert.setContentText("Added Successfully");
                    alert.initStyle(StageStyle.UNDECORATED);
                    alert.showAndWait();
//                    while(outerpart.getChildren().size()!=0)
//                    {
//                    	outerpart.getChildren().remove(0);
//                    }
//                    page.setContent(null);
                    while(ans.getChildren().size()!=0)
                    {
                    	ans.getChildren().remove(0);
                    }
                    ans.getChildren().add(grid);
            	});
        		
        	});
        	page.setContent(outerpart);
	        ans.getChildren().add(page);
        	
        	
		});
		byid.setOnAction(e ->
		{
			VBox outerpart = new VBox();
			while(ans.getChildren().size()!=1)
			{
				ans.getChildren().remove(1);
			}
			Label pass = new Label(" Add Product By Id");
	        pass.setStyle("-fx-text-fill: #000000;-fx-font-size: 20;"); 
	        pass.setPadding(new Insets(20, 20, 20, 30));
	        pass.setAlignment(Pos.CENTER_RIGHT);
	        Separator line = new Separator();
	        line.setOrientation(Orientation.HORIZONTAL);
	        ans.getChildren().add(pass);
	        ans.getChildren().add(line);
	        
	        HBox part1 = new HBox();
	        part1.setSpacing(30);
	        part1.setPadding(new Insets(10, 10, 10 ,10));
	        
	        Label s = new Label();
	        s.setText("Enter ID");
	        s.setPadding(new Insets(5,25,0,0));
	        
	        final TextField search = new TextField();
	        search.setStyle("-fx-background-color: #ffaaaa;-fx-prompt-text-fill: #ffffff;-fx-text-fill: #3a3a3a;");
	        search.setPromptText("Enter Product Id");
	        search.setAlignment(Pos.BASELINE_LEFT);
	        search.setText("");
	        search.setMinWidth(400);
	        
	        part1.getChildren().addAll(s, search);
	        
	        Button accept = new Button();
	        accept.setText("Add This Product");
	        accept.setId("GeneralButtonGreen");
	        accept.setPadding(new Insets(10, 10, 10 ,10));
	        accept.setAlignment(Pos.BASELINE_LEFT);
	        
	        HBox part2 = new HBox();
	        part2.setSpacing(30);
	        part2.setPadding(new Insets(10, 10, 10 ,10));
	        
	        Label s1 = new Label();
	        s1.setText("Enter Name");
	        s1.setPadding(new Insets(5,0,0,0));
	        
	        final TextField search1 = new TextField();
	        search1.setStyle("-fx-background-color: #ffaaaa;-fx-prompt-text-fill: #ffffff;-fx-text-fill: #3a3a3a;");
	        search1.setPromptText("Enter Product Name To be Entered");
	        search1.setAlignment(Pos.BASELINE_LEFT);
	        search1.setText("");
	        search1.setMinWidth(400);
	        
	        part2.getChildren().addAll(s1, search1);
	        
	        HBox part3 = new HBox();
	        part3.setSpacing(30);
	        part3.setPadding(new Insets(10, 10, 10 ,10));
	        
	        part3.getChildren().add(accept);	        
	        ans.getChildren().addAll(part1, part2, part3);
	        
	        accept.setOnAction(f ->
	        {

	        	addproductbyid thread = new addproductbyid(warehouse, Integer.parseInt(s1.getText()), search1.getText());
	        	thread.start();
	        	
	        	try 
    			{
					thread.join();
				} 
    			catch (InterruptedException e1) 
    			{
    				System.out.println("Interrupted");
				}
	        	
	        	Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Information Dialog");
                alert.setHeaderText(null);
                alert.setContentText("Added Successfully");
                alert.initStyle(StageStyle.UNDECORATED);
                alert.showAndWait();
                
                while(ans.getChildren().size()!=0)
                {
                	ans.getChildren().remove(0);
                }
                ans.getChildren().add(grid);
	        });
	        
		});
    	return ans;
    }
    public static VBox RemovePanel()
	{
		VBox ans = new VBox();
		ans.getStylesheets().add("button.css");
		HBox part1 = new HBox();
		part1.setStyle("-fx-background-color: #3a3a3a;");
		part1.setSpacing(30);
        part1.setPadding(new Insets(50, 10, 50 ,10));
		
		Label s = new Label();
		s.setStyle("-fx-text-fill: #ffffff;");
        s.setText("Enter ID or Name ");
        s.setPadding(new Insets(5,25,0,0));
        
        final TextField search = new TextField();
        search.setStyle("-fx-background-color: #ffaaaa;-fx-prompt-text-fill: #ffffff;-fx-text-fill: #3a3a3a;");
        search.setPromptText("Enter Product Id or Name of the Product or Category you want to Delete");
        search.setAlignment(Pos.BASELINE_LEFT);
        search.setText("");
        search.setMinWidth(600);
        
        part1.getChildren().addAll(s, search);
        
        HBox part2 = new HBox();
		part2.setSpacing(30);
        part2.setPadding(new Insets(10, 10, 10 ,10));
        Button accept = new Button();
        accept.setText("Remove This Product");
        accept.setId("GeneralButtonRed");
        accept.setPadding(new Insets(10, 10, 10 ,10));
        accept.setAlignment(Pos.BASELINE_LEFT);
        
        part2.getChildren().add(accept);
        
        ans.getChildren().addAll(part1, part2);
        
        accept.setOnAction(f ->
        {
        	Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.setTitle("Information Dialog");
            alert.setHeaderText(null);
            alert.setContentText("Are you sure ?");
            alert.initStyle(StageStyle.UNDECORATED);
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK)
            {
            	try
            	{
            		Long.parseLong(search.getText());
            	}
            	catch(NumberFormatException t)
            	{
            		Alert alert1 = new Alert(AlertType.ERROR);
                    alert1.setTitle("Information Dialog");
                    alert1.setHeaderText(null);
                    alert1.setContentText("Deletion Unsuccessful");
                    alert1.initStyle(StageStyle.UNDECORATED);
                    alert1.showAndWait();	
            		return ;
            	}
            	
            	delete thread = new delete(warehouse, Long.parseLong(search.getText()));
            	thread.start();
            	try 
    			{
					thread.join();
				} 
    			catch (InterruptedException e) 
    			{
    				System.out.println("Interrupted");
				}
            	if(thread.condition == true)
            	{
            		Alert alert1 = new Alert(AlertType.INFORMATION);
                    alert1.setTitle("Information Dialog");
                    alert1.setHeaderText(null);
                    alert1.setContentText("Deleted Successfully");
                    alert1.initStyle(StageStyle.UNDECORATED);
                    alert1.showAndWait();
            	}
            	else
            	{
            		Alert alert1 = new Alert(AlertType.ERROR);
                    alert1.setTitle("Information Dialog");
                    alert1.setHeaderText(null);
                    alert1.setContentText("Deletion Unsuccessful");
                    alert1.initStyle(StageStyle.UNDECORATED);
                    alert1.showAndWait();	
            	}
            	
                search.setText("");
            } 
            search.setText("");
        });
		return ans;
	}
    public static VBox ModifyPanel()
    {
    	
    	searchinware thread1 = new searchinware(warehouse, "");
    	thread1.start();
    	try {
			thread1.join();
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	ArrayList<Category> avail = thread1.ans;
    	ArrayList<product> list = new ArrayList<product>();
    	for(Category a : avail)
    	{
    		list.add(new product(a.getType(), a.getQty(), a.getPrize(), a.getId()));
    	}
    	
    	Separator line = new Separator();
        line.setOrientation(Orientation.HORIZONTAL);
        
    	VBox ans = new VBox();
		ans.getStylesheets().add("button.css");
		HBox part1 = new HBox();
		part1.setStyle("-fx-background-color: #3a3a3a;");
		part1.setSpacing(30);
        part1.setPadding(new Insets(50, 10, 50 ,10));
		
		Label s = new Label();
		s.setStyle("-fx-text-fill: #ffffff;");
        s.setText("Enter ID or Name ");
        s.setPadding(new Insets(5,25,0,0));
        
        final TextField search = new TextField();
        search.setStyle("-fx-background-color: #ffaaaa;-fx-prompt-text-fill: #ffffff;-fx-text-fill: #3a3a3a;");
        search.setPromptText("Enter Product Id or Name of the Product you want to Edit");
        search.setAlignment(Pos.BASELINE_LEFT);
        search.setText("");
        search.setMinWidth(600);
        
        part1.getChildren().addAll(s, search);
        
        HBox part2 = new HBox();
		part2.setSpacing(30);
        part2.setPadding(new Insets(10, 10, 10 ,10));
        Button accept = new Button();
        accept.setText("Submit");
        accept.setId("GeneralButtonGreen");
        accept.setPadding(new Insets(10, 10, 10 ,10));
        accept.setAlignment(Pos.BASELINE_LEFT);
        
        part2.getChildren().add(accept);
        
        ans.getChildren().addAll(part1, part2, line);
        
        accept.setOnAction(f ->
        {
        	product temp = null ;
        	for(product c : list)
        	{
        		if(c.id == Integer.parseInt(search.getText()))
        		{
        			temp = c;
        			break;
        		}
        	}
        	if(temp == null)
        	{
        		Alert alert1 = new Alert(AlertType.ERROR);
                alert1.setTitle("Information Dialog");
                alert1.setHeaderText(null);
                alert1.setContentText("Modification Unsuccessful");
                alert1.initStyle(StageStyle.UNDECORATED);
                alert1.showAndWait();	
                return;
        	}
        	HBox part3 = new HBox();
        	Label s1 = new Label();
    		s1.setStyle("-fx-text-fill: #3a3a3a;");
            s1.setText("Name ");
            s1.setPadding(new Insets(5,25,0,0));
            
            final TextField search1 = new TextField();
            search1.setStyle("-fx-background-color: #ffaaaa;-fx-prompt-text-fill: #ffffff;-fx-text-fill: #3a3a3a;");
//            search1.setPromptText(temp.name1);
            search1.setAlignment(Pos.BASELINE_LEFT);
            search1.setText(temp.name1);
            search1.setMinWidth(600);
            
            part3.getChildren().addAll(s1, search1);
            
        	HBox part4 = new HBox();
        	Label s2 = new Label();
        	Label s3 = new Label();
    		s2.setStyle("-fx-text-fill: #3a3a3a;");
    		s3.setStyle("-fx-text-fill: #3a3a3a;");
            s2.setText("Price ");
            s3.setText("Quantity ");
            s2.setPadding(new Insets(5,25,0,0));
            s3.setPadding(new Insets(5,25,0,0));
            
            final TextField search2 = new TextField();
            final TextField search3 = new TextField();
            search2.setStyle("-fx-background-color: #ffaaaa;-fx-prompt-text-fill: #ffffff;-fx-text-fill: #3a3a3a;");
            search3.setStyle("-fx-background-color: #ffaaaa;-fx-prompt-text-fill: #ffffff;-fx-text-fill: #3a3a3a;");
            search2.setText(Double.toString(temp.cost1));
            search3.setText(Long.toString(temp.qty1));
            search2.setAlignment(Pos.BASELINE_LEFT);
            search3.setAlignment(Pos.BASELINE_LEFT);
//            search2.setText("");
//            search3.setText("");
            search2.setMinWidth(200);
            search3.setMinWidth(200);
            
            part4.getChildren().addAll(s2, search2, s3, search3);
        	
            HBox part5 = new HBox();
            
            Button accept1 = new Button();
            accept1.setText("Edit This Product");
            accept1.setId("GeneralButtonRed");
            accept1.setPadding(new Insets(10, 10, 10 ,10));
            accept1.setAlignment(Pos.BASELINE_LEFT);
            
        	part5.getChildren().add(accept1);
        	
        	
        	part3.setSpacing(50);part4.setSpacing(50);part5.setSpacing(50);
        	part3.setPadding(new Insets(30, 10, 30, 10));
        	part4.setPadding(new Insets(30, 10, 30, 10));
        	part5.setPadding(new Insets(30, 10, 30, 10));
        	ans.getChildren().addAll(part3, part4, part5);
        	
        	accept1.setOnAction(e ->
            {
            	Alert alert = new Alert(AlertType.CONFIRMATION);
                alert.setTitle("Information Dialog");
                alert.setHeaderText(null);
                alert.setContentText("Are you sure ?");
                alert.initStyle(StageStyle.UNDECORATED);
                Optional<ButtonType> result = alert.showAndWait();
                if (result.get() == ButtonType.OK)
                {
                	modify thread = new modify(warehouse, Double.parseDouble(search2.getText()), Long.parseLong(search3.getText()), Integer.parseInt(search.getText()), search1.getText());
                	thread.start();
                	try 
        			{
    					thread.join();
    				} 
        			catch (InterruptedException e1) 
        			{
        				System.out.println("Interrupted");
    				}
                	if(thread.condition == true)
                	{
                		Alert alert1 = new Alert(AlertType.INFORMATION);
                		alert1.setTitle("Information Dialog");
                		alert1.setHeaderText(null);
                		alert1.setContentText("Edited Successfully");
                		alert1.initStyle(StageStyle.UNDECORATED);
                		alert1.showAndWait();
                	}
                	else
                	{
                		Alert alert1 = new Alert(AlertType.ERROR);
                		alert1.setTitle("Information Dialog");
                		alert1.setHeaderText(null);
                		alert1.setContentText("Edit Unsuccessful");
                		alert1.initStyle(StageStyle.UNDECORATED);
                		alert1.showAndWait();
                	}
                    while(ans.getChildren().size()!=2)
                    {
                    	ans.getChildren().remove(2);
                    }
                } 
                else
                {
                	while(ans.getChildren().size()!=2)
                    {
                    	ans.getChildren().remove(2);
                    }
                }
            });
        });
		return ans;
    }
    public static VBox OrderPanel()
    {
    	VBox ans = new VBox();
    	ans.getStylesheets().add("button.css");
    	ans.setStyle("-fx-background-color: #ffffff;");
    	HBox part = new HBox();
        part.setStyle("-fx-background-color: #3a3a3a;");
        part.setPrefSize(400, 100);
        part.setSpacing(50);
    	part.setPadding(new Insets(5,20,5,10));
    	part.setAlignment(Pos.CENTER);
        
        Label s = new Label();
        s.setText("Number of Item you want to Order ?");
        s.setPadding(new Insets(5,0,0,0));
        s.setStyle("-fx-text-fill: #ffffff;");
        
        final TextField search = new TextField();
        search.setStyle("-fx-background-color: #ffaaaa;-fx-prompt-text-fill: #ffffff;-fx-text-fill: #3a3a3a;");
        search.setPromptText("Number Of Items");
        search.setText("");
        search.setMinWidth(100);
        
        
        Button next = new Button();
    	next.setText("Submit");
    	next.setId("GeneralButtonGreen");

    	part.getChildren().addAll(s, search, next);
    	ans.getChildren().add(part);
    	
    	next.setOnAction(f ->
    	{
    		while(ans.getChildren().size()!=1)
    		{
    			ans.getChildren().remove(1);
    		}
    		ScrollPane scroll = new ScrollPane();
    		scroll.setPrefSize(600, 500);
    		scroll.setStyle("-fx-background-color: #ffffff;");
    		
    		VBox outerpart11 = new VBox();
    		outerpart11.setAlignment(Pos.CENTER);
    		outerpart11.setMinHeight(400);
    		outerpart11.setMinWidth(880);
//    		outerpart11.setPrefSize(400, 600);
//    		outerpart11.setSpacing(10);
//    		outerpart11.setAlignment(Pos.CENTER);
    		for(int i=0;i<Integer.parseInt(search.getText());i++)
    		{
    			HBox part11 = new HBox();
//                part11.setAlignment(Pos.CENTER);
                part11.setStyle("-fx-background-color: #ffffff;");
//                part11.setMinHeight(50);
//                part11.setMinWidth(300);
                part11.setPrefSize(600, 100);
                part11.setSpacing(50);
                
                Label s11 = new Label();
                s11.setText("Name or Id");
                s11.setPadding(new Insets(10,0,0,10));
//                s11.setMinWidth(300);
                s11.setPrefSize(100, 20);
                s11.setStyle("-fx-text-fill: #3a3a3a;");
                
                final TextField search11 = new TextField();
                search11.setStyle("-fx-background-color: #ffaaaa;-fx-prompt-text-fill: #ffffff;-fx-text-fill: #3a3a3a;");
                search11.setPromptText("Name or Id of Product");
                search11.setText("");
                search11.setMinWidth(100);
                
                Label s21 = new Label();
                s21.setText("Quantites");
                s21.setPadding(new Insets(10,0,0,10));
//                s11.setMinWidth(300);
                s21.setPrefSize(100, 20);
                s21.setStyle("-fx-text-fill: #3a3a3a;");
                
                final TextField search21 = new TextField();
                search21.setStyle("-fx-background-color: #ffaaaa;-fx-prompt-text-fill: #ffffff;-fx-text-fill: #3a3a3a;");
                search21.setPromptText("Number Of Items");
                search21.setText("");
                search21.setMinWidth(100);
                
                part11.getChildren().addAll(s11, search11, s21, search21);
                part11.setPadding(new Insets(10,10,10,20));
                outerpart11.getChildren().add(part11);
    		}
			Button accept1 = new Button();
			accept1.setText("Generate Order");
			accept1.setId("GeneralButtonGreen");
			accept1.setPadding(new Insets(10, 10, 10 ,10));
			accept1.setAlignment(Pos.BASELINE_LEFT);
            
			outerpart11.getChildren().add(accept1);
			scroll.setContent(outerpart11);
    		ans.getChildren().add(scroll);    		
    		
    		accept1.setOnAction(g -> 
    		{
    			Alert alert = new Alert(AlertType.CONFIRMATION);
                alert.setTitle("Information Dialog");
                alert.setHeaderText(null);
                alert.setContentText("Are you sure ?");
                alert.initStyle(StageStyle.UNDECORATED);
                Optional<ButtonType> result = alert.showAndWait();
                if (result.get() == ButtonType.OK)
                {
                	Alert alert1 = new Alert(AlertType.INFORMATION);
                    alert1.setTitle("Information Dialog");
                    alert1.setHeaderText(null);
                    alert1.setContentText("Odered Successfully");
                    alert1.initStyle(StageStyle.UNDECORATED);
                    alert1.showAndWait();
                    while(ans.getChildren().size()!=2)
                    {
                    	ans.getChildren().remove(2);
                    }
                } 
                else
                {
                	while(ans.getChildren().size()!=2)
                    {
                    	ans.getChildren().remove(2);
                    }
                }
    		});
    	});
    	return ans;
    }
    public static VBox HomePanel(ArrayList<product> order) 
    {
    	VBox ans = new VBox();
    	ans.setSpacing(20);
    	ans.getStylesheets().add("button.css");
    	ans.setPrefSize(1500, 1500);
    	ans.setStyle("-fx-background-color: #ffffff;");
    	
    	VBox box = new VBox();
    	
    	Separator line = new Separator();
        line.setOrientation(Orientation.HORIZONTAL);
        Separator line1 = new Separator();
        line.setOrientation(Orientation.HORIZONTAL);
        Separator line2 = new Separator();
        line.setOrientation(Orientation.HORIZONTAL);
    	
    	Label s = new Label();
        s.setText("WELCOME");
        s.setPadding(new Insets(10,0,0,10));
        s.setPrefSize(200, 50);
        s.setStyle("-fx-text-fill: #3a3a3a; -fx-font-size: 30; -fx-background-color: #ffffff;");
        
        ans.getChildren().addAll(s, line);
        
        Label s1 = new Label();
        String str = "Admin : " + w_admin.name +" \t Rating : " + "4.1\n";
        s1.setText(str);
        s1.setPadding(new Insets(10,0,0,10));
        s1.setPrefSize(600, 100);
        s1.setStyle("-fx-text-fill: #3a3a3a; -fx-font-size: 20; -fx-background-color: #ffffff;");
    	
        ans.getChildren().addAll(s1, line1);
        
        Label s2 = new Label();
        String st = "WareHouseId : 121\tRating : 3.9"
        		+ "\n\nState : "+"Delhi\tCity : New Delhi"
        				+ "\n\nStatus : OK";
        s2.setText(st);
        s2.setPadding(new Insets(10,0,0,10));
        s1.setPrefSize(900, 100);
        s2.setStyle("-fx-text-fill: #3a3a3a; -fx-font-size: 20; -fx-background-color: #ffffff;");
        
        ans.getChildren().addAll(s2, line2);
    	
//        Label s0 = new Label();
//        s0.setText("Number of Pending Order : " + order.size());
//        s0.setPadding(new Insets(10,0,0,10));
//        s0.setPrefSize(200, 50);
//        s0.setStyle("-fx-text-fill: #3a3a3a;  -fx-background-color: #ffffff;");
//        
//        ans.getChildren().addAll(s0, line);
        
//    	ScrollPane scroll = new ScrollPane();
//    	VBox outerpart = new VBox();
//    	
//    	for(int i=0;i<order.size();i++)
//		{
//			HBox part = new HBox();
//            part.setPrefSize(900, 50);
//            part.setSpacing(50);
//            
//            if(i%2==0)
//            	part.setStyle("-fx-background-color: #ffffff;-fx-background-color: #ffaaaa;");
//            else
//            	part.setStyle("-fx-background-color: #ffffff;");
//            
//            Label s1 = new Label();
//            s1.setText("Name ");
//            s1.setPadding(new Insets(10,0,0,10));
//            s1.setPrefSize(100, 10);
//            s1.setStyle("-fx-text-fill: #3a3a3a;");
//            
//            Label s2 = new Label();
//            s2.setText(order.get(i).name1);
//            s2.setPadding(new Insets(10,0,0,10));
//            s2.setPrefSize(100, 10);
//            s2.setStyle("-fx-text-fill: #3a3a3a;");
//            
//            Label s3 = new Label();
//            s3.setText("Quantites");
//            s3.setPadding(new Insets(10,0,0,10));
//            s3.setPrefSize(100, 10);
//            s3.setStyle("-fx-text-fill: #3a3a3a;");
//
//            Label s4 = new Label();
//            s4.setText(Long.toString(order.get(i).qty1));
//            s4.setPadding(new Insets(10,0,0,10));
//            s4.setPrefSize(100, 10);
//            s4.setStyle("-fx-text-fill: #3a3a3a;");
//            
//            part.getChildren().addAll(s1, s2, s3, s4);
//            part.setPadding(new Insets(10,10,10,20));
//            outerpart.getChildren().add(part);
//            
//		}
////    	DisplayProduct(ans, (ObservableList<product>) order);
//    	scroll.setContent(outerpart);
//    	ans.getChildren().add(scroll);
//    	
//    	if(order.size()!=0)
//    	{
//    		Button accept1 = new Button();
//    		accept1.setText("Handle Order");
//    		accept1.setId("GeneralButtonGreen");
//    		accept1.setPadding(new Insets(10, 10, 10 ,10));
//    		accept1.setAlignment(Pos.BASELINE_LEFT);
//    		
//    		ans.getChildren().add(accept1);
//    		
//    		accept1.setOnAction(g -> 
//    		{
//    			Alert alert = new Alert(AlertType.INFORMATION);
//                alert.setTitle("Information Dialog");
//                alert.setHeaderText(null);
//                alert.setContentText("Are you sure ?");
//                alert.initStyle(StageStyle.UNDECORATED);
//                alert.showAndWait();
//                while(order.size()!=0)
//                {
//                	order.remove(0);
//                }
//                ans.getChildren().remove(ans.getChildren().size()-1);
//                ans.getChildren().remove(ans.getChildren().size()-1);
//    		});
//    	}
		
    	return ans;
    }
}