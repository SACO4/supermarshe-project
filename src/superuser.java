import java.io.*;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
class SuperUser implements Serializable
{
	private ArrayList<StoreHouse> stores ;
	private ArrayList<WareHouse> warehouses ;
	private ArrayList<StoreHouseAdmin> storehouse_admin ;
	private ArrayList<WareHouseAdmin> warehouse_admin ;
	private static long wareadminid;
	public ArrayList<StoreHouseAdmin> getStorehouse_admin() {
		return storehouse_admin;
	}

	public void setStorehouse_admin(ArrayList<StoreHouseAdmin> storehouse_admin) {
		this.storehouse_admin = storehouse_admin;
	}

	public ArrayList<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(ArrayList<Customer> customers) {
		this.customers = customers;
	}

	public ArrayList<WareHouseAdmin> getWarehouse_admin() {
		return warehouse_admin;
	}

	public void setWarehouse_admin(ArrayList<WareHouseAdmin> warehouse_admin) {
		this.warehouse_admin = warehouse_admin;
	}
	private static long storeadminid;
	private static long wareid ;
	private static long storeid ;
	private String username ;
	private String password ;
	private ArrayList<Customer> customers;
	public SuperUser()
	{
		customers = new ArrayList<Customer>(); 
		stores = new ArrayList<StoreHouse>();
		warehouses = new ArrayList<WareHouse>();
		storehouse_admin = new ArrayList<StoreHouseAdmin>();
		warehouse_admin = new ArrayList<WareHouseAdmin>();
		username = MD5("super");
		password = MD5("1234");
		wareid = 0;
		storeid = -1;
		wareadminid = 0;
		storeadminid = -1;
	}
	
	public ArrayList<Category> PartialSearch(String part)
	{
		ArrayList<Category> ans = new ArrayList<Category>();
		for(WareHouse w : warehouses)
		{
			DataBase root = w.getRoot();
			for(Category c : root.getArr())
			{
				String str = c.getType();
				if(str.contains(part))
				{
					ans.add(c);
				}
			}
		}
		for(StoreHouse w : stores)
		{
			DataBase root = w.getRoot();
			for(Category c : root.getArr())
			{
				String str = c.getType();
				if(str.contains(part))
				{
					ans.add(c);
				}
			}
		}
		return ans;
		
	}
	public Category SearchByName(String n)
	{
		Category ans = null ;
		for(WareHouse w : warehouses)
		{
			DataBase root = w.getRoot();
			for(Category c : root.getArr())
			{
				if(c.getType().equals(n))
				{
					ans = c;
					return ans;
				}
			}
		}
		for(StoreHouse w : stores)
		{
			DataBase root = w.getRoot();
			for(Category c : root.getArr())
			{
				if(c.getType().equals(n))
				{
					ans = c;
					return ans;
				}
			}
		}
		return ans;
	}
	public Category SearchById(long id)
	{
		Category ans = null ;
		for(WareHouse w : warehouses)
		{
			DataBase root = w.getRoot();
			for(Category c : root.getArr())
			{
				if(c.getId()==id)
				{
					ans = c;
					return ans;
				}
			}
		}
		for(StoreHouse w : stores)
		{
			DataBase root = w.getRoot();
			for(Category c : root.getArr())
			{
				if(c.getId()==id)
				{
					ans = c;
					return ans;
				}
			}
		}
		return ans;
	}
	
	
	public String GenerateWareHouseAdminUsername()
	{
		String ans ;
		ans = "WareHouse" + GenerateWareHouseAdminId() +"@superstore.com";
		return ans;
	}
	public String GenerateWareHouseAdminPassword()
	{
		String ans ;
		ans = "WareHouse@" + GenerateWareHouseAdminId();
		return ans;
	}
	public String GenerateStoreHouseAdminUsername()
	{
		String ans ;
		ans = "StoreHouse" + GenerateStoreHouseAdminId() +"@superstore.com";
		return ans;
	}
	public String GenerateStoreHouseAdminPassword()
	{
		String ans ;
		ans = "StoreHouse@" + GenerateStoreHouseAdminId();
		return ans;
	}
	public long GenerateWareHouseId()
	{
		long ans ;
		ans = (warehouses.size()*2)+2;
//		SuperUser.setWareid(SuperUser.getWareid() + 2);
//		ans = SuperUser.getWareid() ;
		return ans;
	}
	public long GenerateStoreHouseId()
	{
		long ans ;
		ans = (stores.size()==0)?(stores.size()+1):((stores.size()*2)+1);
//		SuperUser.setStoreid(SuperUser.getStoreid() + 2);
//		ans = SuperUser.getStoreid() ;
		return ans;
	}
	public long GenerateWareHouseAdminId()
	{
		long ans ;
//		SuperUser.setWareadminid(SuperUser.getWareadminid() + 2);
//		ans = SuperUser.getWareadminid() ;
		ans = (warehouse_admin.size()*2)+2;
		return ans;
	}
	public long GenerateStoreHouseAdminId()
	{
		long ans ;
//		SuperUser.setStoreadminid(SuperUser.getStoreadminid() + 2);
//		ans = SuperUser.getStoreadminid() ;
		ans = (storehouse_admin.size()==0)?(storehouse_admin.size()+1):((storehouse_admin.size()*2)+1); 
		return ans;
	}
	public void CreateStoreHouse(String c, String s)
	{
		StoreHouse fresh = new StoreHouse(GenerateStoreHouseId(), c, s);
		stores.add(fresh);
	}
	public void CreateWareHouse(String c, String s)
	{
		WareHouse fresh = new WareHouse(GenerateWareHouseId(), c, s);
//		System.out.println(fresh);
		warehouses.add(fresh);
	}
	public void CreateWareHouseAdmin()
	{
		WareHouseAdmin fresh = new WareHouseAdmin(GenerateWareHouseAdminId(), GenerateWareHouseAdminUsername(), GenerateWareHouseAdminPassword());
		System.out.println(fresh);
		warehouse_admin.add(fresh);
	}
	public void CreateStoreHouseAdmin()
	{
		StoreHouseAdmin fresh = new StoreHouseAdmin(GenerateStoreHouseAdminId() ,GenerateStoreHouseAdminUsername(), GenerateStoreHouseAdminPassword());
		System.out.println(fresh);
		storehouse_admin.add(fresh);
	}
	public boolean CheckForCredentials(String id, String pass) 
	{
		boolean ans = false;
		if(MD5(pass).equals(MD5(this.getPassword())) && id.equals(this.getUsername()))
		{
			ans = true;
		}
		return ans;
	}
	public static String MD5(String input) 
    { 
        try 
        { 
            MessageDigest md = MessageDigest.getInstance("MD5"); 
            byte[] messageDigest = md.digest(input.getBytes()); 
            BigInteger no = new BigInteger(1, messageDigest); 
            String hashtext = no.toString(16); 
            while (hashtext.length() < 32) 
            { 
                hashtext = "0" + hashtext; 
            } 
            return hashtext; 
        }  
        catch (NoSuchAlgorithmException e) 
        { 
            throw new RuntimeException(e); 
        } 
    }  
	public void Link(long adminid, long storeid) throws InvalidIdException
	{
		System.out.println(adminid + " " +storeid);
		if(storeid%2==0)
		{
			//WareHouse
			WareHouseAdmin t = null;
			WareHouse temp = null;
			ArrayList<WareHouse> list = warehouses;
			ArrayList<WareHouseAdmin> arr = warehouse_admin;
			for(WareHouseAdmin a : arr)
			{
				System.out.println(a.getId()+" "+adminid);
				if(a.getId() == adminid)
				{
					t = a;
				}
			}
			if(t == null)
			{
				throw(new InvalidIdException());
			}
			for(WareHouse a : list)
			{
				System.out.println(a.getWarehouseid()+" "+storeid);
				if(a.getWarehouseid() == storeid)
				{
					temp = a;
				}
			}
			if(temp == null)
			{
				throw(new InvalidIdException());
			}
			temp.setAdmin(t);
		}
		else
		{
			//StoreHouse
			StoreHouseAdmin t = null;
			StoreHouse temp = null;
			ArrayList<StoreHouse> list = stores;
			ArrayList<StoreHouseAdmin> arr = storehouse_admin;
			for(StoreHouseAdmin a : arr)
			{
				if(a.getId() == adminid)
				{
					t = a;
				}
			}
			if(t == null)
			{
				throw(new InvalidIdException());
			}
			for(StoreHouse a : list)
			{
				if(a.getId() == storeid)
				{
					temp = a;
				}
			}
			if(temp == null)
			{
				throw(new InvalidIdException());
			}
			temp.setAdmin(t);
		}
	}
	
	public static long getWareid() {
		return wareid;
	}
	public static void setWareid(long wareid) {
		SuperUser.wareid = wareid;
	}
	public static long getStoreid() {
		return storeid;
	}
	public static void setStoreid(long storeid) {
		SuperUser.storeid = storeid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public ArrayList<StoreHouse> getStores() {
		return stores;
	}
	public void setStores(ArrayList<StoreHouse> stores) {
		this.stores = stores;
	}
	public ArrayList<WareHouse> getWarehouses() {
		return warehouses;
	}
	public void setWarehouses(ArrayList<WareHouse> warehouses) {
		this.warehouses = warehouses;
	}
	public static long getWareadminid() {
		return wareadminid;
	}
	public static void setWareadminid(long wareadminid) {
		SuperUser.wareadminid = wareadminid;
	}
	public static long getStoreadminid() {
		return storeadminid;
	}
	public static void setStoreadminid(long storeadminid) {
		SuperUser.storeadminid = storeadminid;
	}
}
public class superuser 
{

}
