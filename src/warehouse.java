import java.io.Serializable;

class WareHouse extends WareHouseAdmin implements Serializable
{
	private long warehouseid;
	private WareHouseAdmin admin;
	private Address location ;
	public WareHouse(long n, String c, String s)
	{
		this.setLocation(new Address(c,s));
		this.setWarehouseid(n);
	}
	public Address getLocation() {
		return location;
	}
	public void setLocation(Address location) {
		this.location = location;
	}
	public WareHouseAdmin getAdmin() {
		return admin;
	}
	public void setAdmin(WareHouseAdmin admin) {
		this.admin = admin;
	}
	public long getWarehouseid() 
	{
		return warehouseid;
	}
	public void setWarehouseid(long id) 
	{
		this.warehouseid = id;
	}
	public String toString()
	{
		return(warehouseid + " " + admin + " " + location);
	}
	
}
public class warehouse 
{

}
