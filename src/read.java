import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.swing.plaf.synth.SynthViewportUI;

public class read 
{
	public synchronized static void write(SuperUser obj)
	{
		SuperUser temp = null;
		SuperUser akki = null ; 
		try
		{
			File data = new File("SuperUserData.txt");
			if(data.exists())
			{
				FileOutputStream file = new FileOutputStream(data);
				ObjectOutputStream in = new ObjectOutputStream(file);
				in.writeObject(obj);
				in.close();
				file.close();
			}
			System.out.println("Writing Successful");
		} 
		catch (IOException e) 
		{
			System.out.println("Unable to Write To File");
		}
	}
	public synchronized static SuperUser read()
	{
		SuperUser temp = null;
		SuperUser akki = null ; 
		try
		{
			File data = new File("SuperUserData.txt");
			if(data.exists())
			{
				FileInputStream file = new FileInputStream(data);
				ObjectInputStream in = new ObjectInputStream(file);
				temp = (SuperUser) in.readObject();
				in.close();
				file.close();
			}
			System.out.println("Reading Successful");
		} 
		catch (IOException | ClassNotFoundException e) 
		{
			System.out.println("Unable to read From File");
		}
		finally
		{
			if(temp == null)
			{
				akki = new SuperUser();
			}
			else
			{
				akki = temp;
			}
		}
		return akki;
	}
}
