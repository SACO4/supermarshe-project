import java.io.Serializable;
import java.util.*;
class Address implements Serializable
{
	private String city;
	private String state;
	public Address(String c, String s)
	{
		this.setCity(c);
		this.setState(s);
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String toString()
	{
		return("State : " + this.getState() + " City : " +  this.getCity());
	}
}
public class address 
{

}
