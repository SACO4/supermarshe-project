import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.Separator;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCharacterCombination;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.Mnemonic;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;


public class customer_login extends Application 
{
    private double xOffset = 0;
    private double yOffset = 0;
    public static Customer cust = null;
    public static long storeid = -1;
    public static StoreHouse house;
    
    public static class product 
    {
    	String name1;
    	long qty1;
    	double cost1;
    	long id;
    	public product(String n,long q, double c, long i)
    	{
    		name1 = n;
    		qty1 = q;
    		cost1 = c;
    		id = i;
    	}
    	public String getName1() {
    		return name1;
    	}
    	public long getId() {
			return id;
		}
		public void setId(long id) {
			this.id = id;
		}
		public void setName1(String name1) {
    		this.name1 = name1;
    	}
    	public long getQty1() {
    		return qty1;
    	}
    	public void setQty1(long qty1) {
    		this.qty1 = qty1;
    	}
    	public double getCost1() {
    		return cost1;
    	}
    	public void setCost1(double cost1) {
    		this.cost1 = cost1;
    	}
    	public String toString()
    	{
    		return(this.name1+" "+this.qty1+" "+this.cost1);
    	}
    }
//    public static 
//    public w_admin()
//    {
//    	String[] bale = new String[5];    	
//    	main(bale);
////    	launch(args);0
//    }
    public static void main(String[] bale) {
        launch(bale);
    }
    public static VBox CheckOutPanel(Stage primaryStage)
    {
    	VBox ans = new VBox();
    	
    	Button accept = new Button();
        accept.setText("Add New Admin");
        accept.setId("GeneralButtonGreen");
        accept.setPadding(new Insets(10, 10, 10 ,10));
        accept.setAlignment(Pos.BASELINE_LEFT);
    	
        HashMap<String, Long> allproduct = null;
        try 
        {
			allproduct = Category.Read();
		} 
        catch (ClassNotFoundException | IOException e1) 
        {
        	
        }
        
    	ArrayList<product> list = new ArrayList<product>();
    	System.out.println(cust.getShopping_list()+"last");
    	for(Cart e : cust.getShopping_list())
    	{
    		//name qty cost id
    		System.out.println(e.getBill());
    		for(String t : allproduct.keySet())
    		{
    			if(allproduct.get(t) == e.getId() && e.getQty()!=0)
    			{
    				list.add(new product(t, e.getQty(), e.getBill(), e.getId()));
    			}
    		}
    	}
    	
    	ArrayList<product> data = new ArrayList<product>();
    	ObservableList<product> temp = FXCollections.observableArrayList();
    	
    	double bill = 0;
    	for(product p : list)
		{
			data.add(p);
			temp.add(new product(p.name1, p.qty1, p.cost1, p.id));
			bill = bill + (p.cost1*p.qty1);
		}
    	final double cost = bill;
		DisplayProduct(ans, temp);
		
		Button next = new Button();
    	next.setText("Submit");
    	next.setId("GeneralButtonGreen");
    	next.setAlignment(Pos.CENTER);
    	
    	ans.setSpacing(20);
    	ans.setMargin(next, new Insets(10,10,10,10));
    	ans.getChildren().add(next);
    	
    	next.setOnAction(f ->
    	{
    		Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.setTitle("Information Dialog");
            alert.setHeaderText(null);
            alert.setContentText("Your Bill Is : " + cost);
            alert.initStyle(StageStyle.UNDECORATED);
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK)
            {
            	
            	checkout thread = new checkout(cust, house);
            	thread.start();
            	try {
					thread.join();
				} catch (InterruptedException e1) {
				}
//            	int k=0;
//		    	boolean flag = true;
//            	while(cust.getShopping_list().size()!=0)
//		    	{
//		    		Category temp1 = house.getProductid().get(cust.shopping_list.get(k).getId());
//		    		if(temp1.getQty()<cust.getShopping_list().get(k).getQty())
//		    		{
//		    			System.out.println("Insufficient Stock Exception");
//		    			flag = false;
//		    		}
//		    		else
//		    		{
//		    			temp1.setQty(temp1.getQty()-cust.shopping_list.get(k).getQty());
//		    			flag = true;
//		    		}
//		    		if(flag==true)
//		    		{
//		    			cust.getShopping_list().remove(k);
//		    		}
//		    		else
//		    		{
//		    			k++;
//		    		}
////		    		cust.getShopping_list().remove(0);
//		    	}
            	Alert alert1 = new Alert(AlertType.INFORMATION);
                alert1.setTitle("Information Dialog");
                alert1.setHeaderText(null);
                alert1.setContentText("Transaction Completed Successfully");
                alert1.initStyle(StageStyle.UNDECORATED);
                alert1.showAndWait();
                
                while(list.size()!=0)
                {
                	list.remove(0);
                }
                SuperUser akki = read.read();
                for(Customer x : akki.getCustomers())
                {
                	if(x.getIdentity().equals(cust.getIdentity()))
                	{
                		x.shopping_list = null;
                	}
                }
                System.out.println(list);
                alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Information Dialog");
                alert.setHeaderText(null);
                alert.setContentText("Login Again Please!");
                alert.initStyle(StageStyle.UNDECORATED);
                alert.showAndWait();
                
                loginpage obj1 = new loginpage();
                
        		try 
        		{
        			primaryStage.close();
        			Stage n = new Stage();
        			obj1.start(n);
        		} 
        		catch (FileNotFoundException e1) 
        		{
        			e1.printStackTrace();
        		}
            }
    	});
    	
    	return ans;
    }
    public static void DisplayProduct(VBox grid,ObservableList<product> temp)
    {
    	 TableView<product> table = new TableView<product>();
    	 table.setStyle("-fx-background-color: #ff9393;");
//    	 table.setEditable(true);
    	 TableColumn idCol = new TableColumn("Product Id");
    	 idCol.setMinWidth(20);
    	 idCol.setCellValueFactory(
                 new PropertyValueFactory<product, String>("id"));
    	 
         TableColumn firstNameCol = new TableColumn("Product Name");
         firstNameCol.setMinWidth(250);
         firstNameCol.setCellValueFactory(
                 new PropertyValueFactory<product, String>("name1"));
  
         TableColumn lastNameCol = new TableColumn("Available Items");
         lastNameCol.setMinWidth(10);
         lastNameCol.setCellValueFactory(
                 new PropertyValueFactory<product, String>("qty1"));
  
         TableColumn emailCol = new TableColumn("Price(in ₹)");
         emailCol.setMinWidth(70);
         emailCol.setCellValueFactory(
                 new PropertyValueFactory<product, String>("cost1"));
         
         table.getColumns().addAll(idCol, firstNameCol, lastNameCol, emailCol);
         
         System.out.println(temp.size());
//         grid.setPadding(new Insets(10,10,10,10));
         table.setItems(temp);
//         ((Group) scene.getRoot()).getChildren().addAll(grid);
         if(grid.getChildren().size()>1)
         {
        	 grid.getChildren().remove(grid.getChildren().size()-1);
         }
         grid.getChildren().add(table);
    }
    public static VBox SearchPanel()
    {
    	
    	VBox ans = new VBox();
    	
    	GridPane grid = new GridPane();
    	grid.setHgap(20);
    	grid.setVgap(10);
    	grid.setPadding(new Insets(50,10,50,20));
    	grid.getStylesheets().add("button.css");
        grid.setStyle("-fx-background-color: #3a3a3a;");
        Label s = new Label();
        s.setStyle("-fx-text-fill: #ffffff;");
        s.setText("Search");
        
        final TextField search = new TextField();
        search.setStyle("-fx-background-color: #ffaaaa;-fx-prompt-text-fill: #ffffff;-fx-text-fill: #3a3a3a;");
        search.setPromptText("Search Any Product");
        search.setAlignment(Pos.BASELINE_LEFT);
        search.setText("");
        search.setMinWidth(400);
        
        Button submit = new Button();
        submit.setText("Search");
		submit.setId("GeneralButton");
		submit.setMinWidth(150);
		
		
		Button clear = new Button();
        clear.setText("Cancel");
		clear.setId("GeneralButton");
		clear.setMinWidth(150);
		
		submit.setOnAction(e -> 
        {
        	searchinstore thread = new searchinstore(search.getText(), house);
        	thread.start();
        	try 
        	{
				thread.join();
			} 
        	catch (InterruptedException e1) 
        	{
				e1.printStackTrace();
			}
        	ArrayList<Category> avail = thread.ans;
        	System.out.println(avail+"SearchedArray");
        	ArrayList<product> list = new ArrayList<product>();
        	for(Category a : avail)
        	{
        		list.add(new product(a.getType(), a.getQty(), a.getPrize(), a.getId()));
        	}
        	
        	ArrayList<product> data = new ArrayList<product>();
        	ObservableList<product> temp = FXCollections.observableArrayList();
        	String str = search.getText();
        	if(str!=null)
        	{
        		for(product p : list)
            	{
            		if(p.name1.toLowerCase().contains(str.toLowerCase())||str.toLowerCase().contains(p.name1.toLowerCase()))
            		{
            			data.add(p);
            			temp.add(new product(p.name1, p.qty1, p.cost1, p.id));
            		}
            	}
        		DisplayProduct(ans, temp);
        	}
        	else
        	{
        		for(product p : list)
        		{
        			data.add(p);
        			temp.add(new product(p.name1, p.qty1, p.cost1, p.id));
        		}
        		DisplayProduct(ans, temp);
        	}
  		});
  		clear.setOnAction(e ->
  		{
	  		search.setText(null);
		});
        grid.setMargin(s, new Insets(0,0,0,00));
        grid.setMargin(search, new Insets(0,0,0,0));
        grid.setMargin(clear, new Insets(0,0,0,0));
        grid.setHalignment(s, HPos.CENTER);
        grid.setHalignment(search, HPos.CENTER);
        grid.add(s, 0, 0);
        grid.add(search, 1, 0);
        grid.add(submit, 2, 0);
        grid.add(clear, 3, 0);
        ans.getChildren().add(grid);
        return ans;
    }
    @Override
    public void start(final Stage primaryStage) throws FileNotFoundException {
//    	System.out.println(cust+","+house+","+Category.allproduct);
        primaryStage.initStyle(StageStyle.UNDECORATED);
        
        BorderPane root = new BorderPane();
        final Tooltip tooltip = new Tooltip();
        tooltip.setText("\nClose\n");
        root.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });
        root.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                primaryStage.setX(event.getScreenX() - xOffset);
                primaryStage.setY(event.getScreenY() - yOffset);
            }
        });
        GridPane grid = new GridPane();
        grid.getStylesheets().add("button.css");
        grid.setStyle("-fx-background-color: #dc143c;");
        Button exit = new Button();
        Button mini = new Button();
        Label text = new Label();
        text.setText("Super Marché");
        text.setId("MarketName");
        mini.setId("OpenMinButton");
        exit.setId("OpenMinButton");
        exit.setTooltip(new Tooltip("Close"));
        mini.setTooltip(new Tooltip("Minimize"));
        exit.setText("x");
        mini.setText("-");
        exit.setOnAction(e -> primaryStage.close());
        mini.setOnAction(e -> {((Stage)((Button)e.getSource()).getScene().getWindow()).setIconified(true);});
        grid.setPadding(new Insets(5, 0, 10, 5));
        grid.setVgap(5); 
        grid.setHgap(5);
        ColumnConstraints col1 = new ColumnConstraints();
        col1.setPercentWidth(94);
        ColumnConstraints col2 = new ColumnConstraints();
        col2.setPercentWidth(3);
        ColumnConstraints col3 = new ColumnConstraints();
        col3.setPercentWidth(3);
        grid.getColumnConstraints().addAll(col1);
        text.setAlignment(Pos.TOP_LEFT);
        
        grid.add(exit, 2, 0);
        grid.add(mini, 1, 0);
        grid.add(text, 0, 0);
        root.setTop(grid);

        GridPane navigation = new GridPane();
        navigation.setMinHeight(500);
      	navigation.setMinWidth(300);
      	navigation.setMaxWidth(300);
        navigation.setAlignment(Pos.TOP_CENTER);
        navigation.getStylesheets().add("button.css");
        Label name = new Label();
        name.setStyle("-fx-text-fill: #ffffff;-fx-font-weight: bold;");
        name.setText("USERNAME");
        Label user = new Label();
        user.setStyle("-fx-text-fill: #ffffff;");
        if(cust!=null)
        {
        	user.setText(cust.identity);
        }
        else
        {
        	user.setText(null);
        }
        user.setStyle("-fx-font-weight: bold;-fx-text-fill: #ffffff;-fx-font-size: 20;");
        Button nav1 = new Button();
        nav1.setId("NavigationButton");
        nav1.setText("Search");
        Button nav2 = new Button();
        nav2.setId("NavigationButton");
        nav2.setText("Add Product to Cart");
        Button nav3 = new Button();
        nav3.setId("NavigationButton");
        nav3.setText("Update Cart");
        Button nav4 = new Button();
        nav4.setId("NavigationButton");
        nav4.setText("Change Personal Info");
//        Button nav5 = new Button();
//        nav5.setId("NavigationButton");
//        nav5.setText("Generate Order");
        Button nav0 = new Button();
        nav0.setId("NavigationButton");
        nav0.setText("Home");
        Button nav6 = new Button();
        nav6.setId("NavigationButton");
        nav6.setText("Check Out");
        Button nav7 = new Button();
        nav7.setId("GeneralButtonRed");
        nav7.setPrefSize(1000, 20);
        nav7.setText("Log Out");
        
        Image img = new Image(new FileInputStream("customer.png"));
        ImageView i = new ImageView(img);
        i.setFitHeight(230);
      	i.setFitWidth(230);
      	i.setPreserveRatio(true);
      	i.setStyle("-fx-background-color: #494949;");
      	
      	navigation.setHgap(0);
      	navigation.setPadding(new Insets(0,0,0,0));
      	navigation.setMargin(user, new Insets(20,10,10,10));
      	navigation.setMargin(i, new Insets(30,30,30,30));
      	navigation.setMargin(nav0, new Insets(60,10,0,10));
      	navigation.setMargin(nav1, new Insets(0,10,0,10));
  		navigation.setMargin(nav2, new Insets(0,10,0,10));
  		navigation.setMargin(nav3, new Insets(0,10,0,10));
  		navigation.setMargin(nav6, new Insets(0,10,0,10));
      	navigation.setMargin(nav4, new Insets(0,10,0,10));
//      	navigation.setMargin(nav5, new Insets(0,10,0,10));
      	navigation.setMargin(nav7, new Insets(60,10,0,10));
      	navigation.setVgap(0);
      	
      	navigation.add(i, 0, 1);
      	navigation.add(user, 0, 2);
      	navigation.add(nav0, 0, 3);
      	navigation.add(nav1, 0, 4);
      	if(cust!=null)
      	{
      		navigation.add(nav2, 0, 5);
      		navigation.add(nav3, 0, 6);
      		navigation.add(nav6, 0, 8);
      	}
      	if(cust == null)
      	{
      		navigation.add(nav4, 0, 7);
      	}
      	navigation.add(nav7, 0, 9);
//      	navigation.add(nav7, 0, 10);
      	
      	navigation.setHalignment(i, HPos.CENTER);
      	navigation.setHalignment(user, HPos.CENTER);
      	navigation.setHalignment(nav0, HPos.CENTER);
      	navigation.setHalignment(nav1, HPos.CENTER);
      	navigation.setHalignment(nav2, HPos.CENTER);
      	navigation.setHalignment(nav3, HPos.CENTER);
      	navigation.setHalignment(nav4, HPos.CENTER);
//      	navigation.setHalignment(nav5, HPos.CENTER);
      	navigation.setHalignment(nav6, HPos.CENTER);
      	navigation.setHalignment(nav7, HPos.CENTER);
      	navigation.setStyle("-fx-background-color: #494949;");
      	
      	
      	
        root.setLeft(navigation);
        root.setCenter(HomePanel());
        nav0.setOnAction(e ->
        {
        	VBox temp = HomePanel();
        	root.setCenter(temp);
        });
        nav1.setOnAction(e ->
        {
        	VBox temp = SearchPanel();
        	root.setCenter(temp);
        	
        });
        nav2.setOnAction(e ->
        {
        	VBox temp = AddProduct(primaryStage);
        	root.setCenter(temp);
        });
        nav3.setOnAction(e ->
        {
        	VBox temp = UpdatePanel(primaryStage);
        	root.setCenter(temp);
        	
        });
        nav4.setOnAction(e ->
        {
        	VBox temp = PersonalInfo(primaryStage);
        	root.setCenter(temp);
        });
//        nav5.setOnAction(e ->
//        {
//        	VBox temp = OrderPanel();
//        	root.setCenter(temp);
//        });
        nav6.setOnAction(e ->
        {
        	VBox temp = CheckOutPanel(primaryStage);
        	root.setCenter(temp);
        	
        });
        nav7.setOnAction(e ->
        {
        	Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Information Dialog");
            alert.setHeaderText(null);
            alert.setContentText("Logged Out successfully !");
            alert.initStyle(StageStyle.UNDECORATED);
            alert.showAndWait();
            
            loginpage obj = new loginpage();
            
    		try 
    		{
    			primaryStage.close();
    			Stage n = new Stage();
				obj.start(n);
			} 
    		catch (FileNotFoundException e1) 
    		{
				e1.printStackTrace();
			}
        });
        GridPane foot = new GridPane();
        foot.setStyle("-fx-background-color: #dc143c;");
        foot.setPadding(new Insets(10, 10, 10, 20));
        name = new Label("©VENOM");
        name.setStyle("-fx-text-fill: #ffffff;");
        Label pass = new Label(" v 1.0 bETA");
        pass.setStyle("-fx-text-fill: #ffffff;");
        name.setAlignment(Pos.CENTER_LEFT);
        pass.setAlignment(Pos.CENTER_RIGHT);
        foot.add(name, 0, 0);
        foot.add(pass, 1, 0);
        root.setBottom(foot);
        
        Scene scene = new Scene(root, 1200, 900);
        primaryStage.setScene(scene);
        primaryStage.setMinHeight(900);
        primaryStage.setMinWidth(1200);
        primaryStage.show();
    }
    public static VBox AddProduct(Stage primaryStage)
    {
    	ArrayList<Category> total = new ArrayList<Category>();
    	for(long a : house.getAdmin().getProductid().keySet())
    	{
    		total.add(house.getAdmin().getProductid().get(a));
    	}
    	System.out.println(total+"in total***********************");
    	VBox ans = new VBox();
    	ans.getStylesheets().add("button.css");
    	GridPane grid = new GridPane();
    	grid.setHgap(20);
    	grid.setVgap(10);
    	grid.setPadding(new Insets(50,10,30,20));
    	grid.getStylesheets().add("button.css");
        grid.setStyle("-fx-background-color: #3a3a3a;");
        
        Label text = new Label();
        text.setText("Enter Number of Queries");
        text.setStyle("-fx-text-fill: #ffffff;");
        text.setAlignment(Pos.CENTER);
        text.setPadding(new Insets(5,0,5,0));
        
        final TextField search = new TextField();
        search.setStyle("-fx-background-color: #ffaaaa;-fx-prompt-text-fill: #ffffff;-fx-text-fill: #3a3a3a;");
        search.setPromptText("Number of Items to be Added");
        search.setAlignment(Pos.BASELINE_LEFT);
        search.setText(null);
        search.setMinWidth(400);
        
        Button submit = new Button();
        submit.setText("Submit");
        submit.setId("GeneralButton");
        submit.setMinWidth(40);
        
        grid.add(text, 1, 0);
        grid.add(search, 2, 0);
        grid.add(submit, 3, 0);
        
        ScrollPane scroll = new ScrollPane();
        
        ans.getChildren().add(grid);
        
        submit.setOnAction(e ->
        {
        	while(ans.getChildren().size()!=1)
        	{
        		ans.getChildren().remove(1);
        	}
        	
        	Label left = new Label();
        	left.setText("Product Id or Name");
        	left.setStyle("-fx-text-fill: #3a3a3a;");
        	left.setAlignment(Pos.CENTER_LEFT);
        	left.setPadding(new Insets(5,0,5,0));
        	left.setPrefSize(300, 30);
        	
        	Label right = new Label();
        	right.setText("Quantity");
        	right.setStyle("-fx-text-fill: #3a3a3a;");
        	right.setAlignment(Pos.CENTER_LEFT);
        	right.setPadding(new Insets(5,0,5,10));
        	right.setPrefSize(300, 30);
        	
        	VBox outerpart = new VBox();
        	outerpart.setPadding(new Insets(15,15,15,15));
        	outerpart.setSpacing(30);
        	HBox first = new HBox();
        	first.setSpacing(300);
        	first.getChildren().addAll(left, right);
        	outerpart.getChildren().add(first);
        	long n = Integer.parseInt(search.getText());
        	final ComboBox search11[] = new ComboBox[(int) n];
        	final TextField search1[] = new TextField[(int) n];
        	for(int i=0;i<n;i++)
        	{
        		HBox part = new HBox();
        		part.setSpacing(50);
        		
        		search11[i] = new ComboBox();
                search11[i].getItems().addAll("0","1","2","3","4","5");
        		
        		search1[i] = new TextField();
    	        search1[i].setStyle("-fx-background-color: #ffaaaa;-fx-prompt-text-fill: #ffffff;-fx-text-fill: #ffffff;");
    	        search1[i].setPromptText("Enter Product Id/Name");
    	        search1[i].setAlignment(Pos.BASELINE_LEFT);
//    	        search1.setAlignment(Pos.CENTER);
    	        search1[i].setMinWidth(300);
    	        
//    	        final TextField search11 = new TextField();
//    	        search11.setStyle("-fx-background-color: #ffaaaa;-fx-prompt-text-fill: #ffffff;-fx-text-fill: #ffffff;");
//    	        search11.setPromptText("Enter StoreHouse/WareHouse Id");
//    	        search11.setAlignment(Pos.BASELINE_LEFT);
//    	        search1.setAlignment(Pos.CENTER);
//    	        search11.setText(null);
//    	        search11.setMinWidth(300);
    	        
    	        Label center = new Label();
    	        center.setText("------------------->");
    	        center.setStyle("-fx-text-fill: #3a3a3a;");
    	        center.setAlignment(Pos.CENTER);
    	        center.setPadding(new Insets(5,0,5,0));
    	        
    	        part.getChildren().addAll(search1[i], center, search11[i]);
    	        outerpart.getChildren().add(part);
        	}
        	scroll.setContent(outerpart);
        	
        	Button next = new Button();
        	next.setText("Submit");
        	next.setId("GeneralButtonGreen");
        	next.setAlignment(Pos.CENTER);
        	
        	ans.getChildren().add(scroll);
        	ans.setSpacing(20);
        	ans.setMargin(next, new Insets(10,10,10,10));
        	ans.getChildren().add(next);
        	
        	next.setOnAction(f ->
        	{
        		
        		for(int i=0;i<n;i++)
        		{
        			addproduct thread = new addproduct(Integer.parseInt((String) search11[i].getValue()), Long.parseLong(search1[i].getText()), cust, total);
        			thread.start();
        			try {
						thread.join();
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}
        		}
        		
        		Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Information Dialog");
                alert.setHeaderText(null);
                alert.setContentText("Added Successfully");
                alert.initStyle(StageStyle.UNDECORATED);
                alert.showAndWait();
                while(ans.getChildren().size()!=1)
                {
                	ans.getChildren().remove(1);
                	search.setText(null);
                }
                alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Information Dialog");
                alert.setHeaderText(null);
                alert.setContentText("Login Again Please!");
                alert.initStyle(StageStyle.UNDECORATED);
                alert.showAndWait();
                
                loginpage obj1 = new loginpage();
                
        		try 
        		{
        			primaryStage.close();
        			Stage w = new Stage();
        			obj1.start(w);
        		} 
        		catch (FileNotFoundException e1) 
        		{
        			e1.printStackTrace();
        		}
                
        	});
        	
        	});
        
		return ans;
    	
    }
    public static VBox UpdatePanel(Stage primaryStage)
    {
    	ArrayList<Cart> list = cust.getShopping_list();
    	
    	Separator line = new Separator();
        line.setOrientation(Orientation.HORIZONTAL);
        
    	VBox ans = new VBox();
		ans.getStylesheets().add("button.css");
		
		VBox outerpart = new VBox();
    	outerpart.setPadding(new Insets(15,15,15,15));
    	outerpart.setSpacing(30);
    	
//    	HBox first = new HBox();
//    	first.setSpacing(300);
//    	outerpart.getChildren().add(first);
    	
    	ScrollPane scroll = new ScrollPane();
    	final ComboBox search11[] = new ComboBox[list.size()];
    	for(int i=0;i<list.size();i++)
    	{
    		HBox part = new HBox();
    		part.setSpacing(50);
    		part.setPadding(new Insets(10,10,10,10));
    		if(i%2==0)
    		{
    			part.setStyle("-fx-background-color: #ffa0a0; ");
    		}
    		
    		search11[i] = new ComboBox();
            search11[i].getItems().addAll("0","1","2","3","4","5");
            search11[i].setValue(list.get(i).getQty());
    		
            Label s = new Label();
            s.setText("Number of Item you want to Order ?");
            s.setPadding(new Insets(5,0,0,0));
            s.setStyle("-fx-text-fill: #3a3a3a;");
//	        search1.setAlignment(Pos.CENTER);
            System.out.println(list);
	        s.setText(Long.toString(list.get(i).getId()));
	        s.setMinWidth(300);
	        
//	        final TextField search11 = new TextField();
//	        search11.setStyle("-fx-background-color: #ffaaaa;-fx-prompt-text-fill: #ffffff;-fx-text-fill: #ffffff;");
//	        search11.setPromptText("Enter StoreHouse/WareHouse Id");
//	        search11.setAlignment(Pos.BASELINE_LEFT);
//	        search1.setAlignment(Pos.CENTER);
//	        search11.setText(null);
//	        search11.setMinWidth(300);
	        
	        Label center = new Label();
	        center.setText("------------------->");
	        center.setStyle("-fx-text-fill: #3a3a3a;");
	        center.setAlignment(Pos.CENTER);
	        center.setPadding(new Insets(5,0,5,0));
	        
	        part.getChildren().addAll(s, center, search11[i]);
	        outerpart.getChildren().add(part);
    	}
    	scroll.setContent(outerpart);
    	ans.getChildren().add(scroll);
    	
    	Button next = new Button();
    	next.setText("Submit");
    	next.setId("GeneralButtonGreen");
    	next.setAlignment(Pos.CENTER);
    	
    	ans.setSpacing(20);
    	ans.setMargin(next, new Insets(10,10,10,10));
    	ans.getChildren().add(next);
    	next.setOnAction(f ->
    	{
    		for(int i=0;i<list.size();i++)
    		{
    			int check = 0;
    			if(search11[i].getValue().getClass().getName().equalsIgnoreCase("java.lang.String"))
    			{
    				check = Integer.parseInt((String) search11[i].getValue());
    			}
    			else if(search11[i].getValue().getClass().getName().equalsIgnoreCase("java.lang.Integer"))
    			{
    				check = (int) search11[i].getValue();
    			}
//    			System.out.println(search11[i].getValue().getClass().getName()+"*");
//    			System.out.println(search11[i].getValue().getClass());
//    			System.out.println(list.get(i).getId());
//    			System.out.println(list.get(i)+"::");
//    			System.out.println(cust);
    			update thread = new update(check,list.get(i).getId() ,cust, list.get(i));
    			thread.start();
    			try {
					thread.join();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    		}
    		for(int i=0;i<list.size();i++)
    		{
    			System.out.println(list.get(i)+"&*(&(&(&(*");
    		}
    		
    		Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Information Dialog");
            alert.setHeaderText(null);
            alert.setContentText("Added Successfully");
            alert.initStyle(StageStyle.UNDECORATED);
            alert.showAndWait();
            
            loginpage obj = new loginpage();
            
    		try 
    		{
    			primaryStage.close();
    			Stage n = new Stage();
				obj.start(n);
			} 
    		catch (FileNotFoundException e1) 
    		{
				e1.printStackTrace();
			}
    		alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Information Dialog");
            alert.setHeaderText(null);
            alert.setContentText("Login Again Please!");
            alert.initStyle(StageStyle.UNDECORATED);
            alert.showAndWait();
            
            loginpage obj1 = new loginpage();
            
    		try 
    		{
    			primaryStage.close();
    			Stage n = new Stage();
    			obj1.start(n);
    		} 
    		catch (FileNotFoundException e1) 
    		{
    			e1.printStackTrace();
    		}
    		
    	});
    	
    	return ans;
    }
    public static VBox PersonalInfo(Stage primaryStage)
    {
    	VBox ans = new VBox();
		ans.getStylesheets().add("button.css");
		
    	VBox outerpart = new VBox();
		Label pass = new Label(" Enter Personal Data");
        pass.setStyle("-fx-text-fill: #000000;-fx-font-size: 20;"); 
        pass.setPadding(new Insets(20, 20, 20, 30));
        pass.setAlignment(Pos.CENTER_RIGHT);
        Separator line = new Separator();
        line.setOrientation(Orientation.HORIZONTAL);
        ans.getChildren().add(pass);
        ans.getChildren().add(line);
        
        HBox part1 = new HBox();
        part1.setStyle("-fx-background-color: #ffffff;");
        part1.setSpacing(63);
        part1.setPadding(new Insets(10, 10, 10 ,10));
        
        Label s = new Label();
        s.setText("Enter First Name ");
        s.setStyle("-fx-text-fill: #3a3a3a;");
        s.setPadding(new Insets(5,25,0,0));
        
        final TextField search = new TextField();
        search.setStyle("-fx-background-color: #ffaaaa;-fx-prompt-text-fill: #ffffff;-fx-text-fill: #3a3a3a;");
        search.setPromptText("Enter First Name");
        search.setAlignment(Pos.BASELINE_LEFT);
        search.setText(null);
        search.setMinWidth(400);
        
        part1.getChildren().addAll(s, search);
        
        Button accept = new Button();
        accept.setText("Add New Admin");
        accept.setId("GeneralButtonGreen");
        accept.setPadding(new Insets(10, 10, 10 ,10));
        accept.setAlignment(Pos.BASELINE_LEFT);
        
        HBox part2 = new HBox();
        part2.setStyle("-fx-background-color: #3a3a3a;");
        part2.setSpacing(90);
        part2.setPadding(new Insets(10, 10, 10 ,10));
        
        Label s1 = new Label();
        s1.setText("Enter E-Mail Id");
        s1.setStyle("-fx-text-fill: #ffffff;");
        s1.setPadding(new Insets(5,0,0,0));
        
        final TextField search1 = new TextField();
        search1.setStyle("-fx-background-color: #ffaaaa;-fx-prompt-text-fill: #ffffff;-fx-text-fill: #3a3a3a;");
        search1.setPromptText("Enter Last Name");
        search1.setAlignment(Pos.BASELINE_LEFT);
        search1.setText(null);
        search1.setMinWidth(400);
        
        part2.getChildren().addAll(s1, search1);
        
        HBox part4 = new HBox();
        part4.setStyle("-fx-background-color: #ffffff;");
        part4.setSpacing(40);
        part4.setPadding(new Insets(10, 10, 10 ,10));
        
        Label s4 = new Label();
        DatePicker date = new DatePicker();
        s4.setText("Enter DOB(dd/mm/yyyy)");
        s4.setStyle("-fx-text-fill: #3a3a3a;");
        s4.setPadding(new Insets(5,0,0,0));
        
        final TextField search4 = new TextField();
        search4.setStyle("-fx-background-color: #ffaaaa;-fx-prompt-text-fill: #ffffff;-fx-text-fill: #3a3a3a;");
        search4.setPromptText("dd / mm / yyyy");
        search4.setAlignment(Pos.BASELINE_LEFT);
        search4.setText(null);
        search4.setMinWidth(400);
        
        part4.getChildren().addAll(s4, date);
        
        HBox part5 = new HBox();
        part5.setStyle("-fx-background-color: #3a3a3a;");
        part5.setSpacing(60);
        part5.setPadding(new Insets(10, 10, 10 ,10));
        
        Label s5 = new Label();
        s5.setStyle("-fx-text-fill: #ffffff;");
        s5.setText("Set Password");
        s5.setPadding(new Insets(5,0,0,0));
        
        final TextField search5 = new TextField();
        search5.setStyle("-fx-background-color: #ffaaaa;-fx-prompt-text-fill: #ffffff;-fx-text-fill: #3a3a3a;");
        search5.setPromptText("Set Password");
        search5.setAlignment(Pos.BASELINE_LEFT);
        search5.setText(null);
        search5.setMinWidth(400);
        
        part5.getChildren().addAll(s5, search5);
        
        HBox part3 = new HBox();
        part3.setSpacing(30);
        part3.setPadding(new Insets(10, 10, 10 ,10));
        
        part3.getChildren().add(accept);	        
        ans.getChildren().addAll(part1, part2, part4, part5, part3);
        
        accept.setOnAction(f ->
        {
        	
        	addcustomer thread = new addcustomer(cust, search1.getText(), search5.getText());
        	thread.start();
        	try 
        	{
				thread.join();
			}
        	catch (InterruptedException e) 
        	{
        		System.out.println("Error");
			}
        	
    		Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Information Dialog");
            alert.setHeaderText(null);
            alert.setContentText("Login Again Please!");
            alert.initStyle(StageStyle.UNDECORATED);
            alert.showAndWait();
            
            loginpage obj1 = new loginpage();
            
    		try 
    		{
    			primaryStage.close();
    			Stage n = new Stage();
				obj1.start(n);
			} 
    		catch (FileNotFoundException e1) 
    		{
				e1.printStackTrace();
			}
        });
		return ans;
    }
    public static VBox OrderPanel()
    {
    	VBox ans = new VBox();
    	ans.getStylesheets().add("button.css");
    	ans.setStyle("-fx-background-color: #ffffff;");
    	HBox part = new HBox();
        part.setStyle("-fx-background-color: #3a3a3a;");
        part.setPrefSize(400, 100);
        part.setSpacing(50);
    	part.setPadding(new Insets(5,20,5,10));
    	part.setAlignment(Pos.CENTER);
        
        Label s = new Label();
        s.setText("Number of Item you want to Order ?");
        s.setPadding(new Insets(5,0,0,0));
        s.setStyle("-fx-text-fill: #ffffff;");
        
        final TextField search = new TextField();
        search.setStyle("-fx-background-color: #ffaaaa;-fx-prompt-text-fill: #ffffff;-fx-text-fill: #3a3a3a;");
        search.setPromptText("Number Of Items");
        search.setText(null);
        search.setMinWidth(100);
        
        
        Button next = new Button();
    	next.setText("Submit");
    	next.setId("GeneralButtonGreen");

    	part.getChildren().addAll(s, search, next);
    	ans.getChildren().add(part);
    	
    	next.setOnAction(f ->
    	{
    		while(ans.getChildren().size()!=1)
    		{
    			ans.getChildren().remove(1);
    		}
    		ScrollPane scroll = new ScrollPane();
    		scroll.setPrefSize(600, 500);
    		scroll.setStyle("-fx-background-color: #ffffff;");
    		
    		VBox outerpart11 = new VBox();
    		outerpart11.setAlignment(Pos.CENTER);
    		outerpart11.setMinHeight(400);
    		outerpart11.setMinWidth(880);
//    		outerpart11.setPrefSize(400, 600);
//    		outerpart11.setSpacing(10);
//    		outerpart11.setAlignment(Pos.CENTER);
    		for(int i=0;i<Integer.parseInt(search.getText());i++)
    		{
    			HBox part11 = new HBox();
//                part11.setAlignment(Pos.CENTER);
                part11.setStyle("-fx-background-color: #ffffff;");
//                part11.setMinHeight(50);
//                part11.setMinWidth(300);
                part11.setPrefSize(600, 100);
                part11.setSpacing(50);
                
                Label s11 = new Label();
                s11.setText("Name or Id");
                s11.setPadding(new Insets(10,0,0,10));
//                s11.setMinWidth(300);
                s11.setPrefSize(100, 20);
                s11.setStyle("-fx-text-fill: #3a3a3a;");
                
                final TextField search11 = new TextField();
                search11.setStyle("-fx-background-color: #ffaaaa;-fx-prompt-text-fill: #ffffff;-fx-text-fill: #3a3a3a;");
                search11.setPromptText("Name or Id of Product");
                search11.setText(null);
                search11.setMinWidth(100);
                
                Label s21 = new Label();
                s21.setText("Quantites");
                s21.setPadding(new Insets(10,0,0,10));
//                s11.setMinWidth(300);
                s21.setPrefSize(100, 20);
                s21.setStyle("-fx-text-fill: #3a3a3a;");
                
                final TextField search21 = new TextField();
                search21.setStyle("-fx-background-color: #ffaaaa;-fx-prompt-text-fill: #ffffff;-fx-text-fill: #3a3a3a;");
                search21.setPromptText("Number Of Items");
                search21.setText(null);
                search21.setMinWidth(100);
                
                part11.getChildren().addAll(s11, search11, s21, search21);
                part11.setPadding(new Insets(10,10,10,20));
                outerpart11.getChildren().add(part11);
    		}
			Button accept1 = new Button();
			accept1.setText("Generate Order");
			accept1.setId("GeneralButtonGreen");
			accept1.setPadding(new Insets(10, 10, 10 ,10));
			accept1.setAlignment(Pos.BASELINE_LEFT);
            
			outerpart11.getChildren().add(accept1);
			scroll.setContent(outerpart11);
    		ans.getChildren().add(scroll);    		
    		
    		accept1.setOnAction(g -> 
    		{
    			Alert alert = new Alert(AlertType.CONFIRMATION);
                alert.setTitle("Information Dialog");
                alert.setHeaderText(null);
                alert.setContentText("Are you sure ?");
                alert.initStyle(StageStyle.UNDECORATED);
                Optional<ButtonType> result = alert.showAndWait();
                if (result.get() == ButtonType.OK)
                {
                	Alert alert1 = new Alert(AlertType.INFORMATION);
                    alert1.setTitle("Information Dialog");
                    alert1.setHeaderText(null);
                    alert1.setContentText("Odered Successfully");
                    alert1.initStyle(StageStyle.UNDECORATED);
                    alert1.showAndWait();
                    while(ans.getChildren().size()!=2)
                    {
                    	ans.getChildren().remove(2);
                    }
                } 
                else
                {
                	while(ans.getChildren().size()!=2)
                    {
                    	ans.getChildren().remove(2);
                    }
                }
    		});
    	});
    	return ans;
    }
    public static VBox HomePanel() 
    {
    	VBox ans = new VBox();
    	ans.setSpacing(20);
    	ans.getStylesheets().add("button.css");
    	ans.setPrefSize(1500, 1500);
    	ans.setStyle("-fx-background-color: #ffffff;");
    	
    	VBox box = new VBox();
    	
    	Separator line = new Separator();
        line.setOrientation(Orientation.HORIZONTAL);
        Separator line1 = new Separator();
        line.setOrientation(Orientation.HORIZONTAL);
        Separator line2 = new Separator();
        line.setOrientation(Orientation.HORIZONTAL);
    	
    	Label s = new Label();
        s.setText("WELCOME");
        s.setPadding(new Insets(10,0,0,10));
        s.setPrefSize(200, 50);
        s.setStyle("-fx-text-fill: #3a3a3a; -fx-font-size: 30; -fx-background-color: #ffffff;");
        
        ans.getChildren().addAll(s, line);
        
        Label s1 = new Label();
        String str ;
        if(cust!=null)
        {
        	str = " \t E-Mail : " + cust.identity;
        }
        else
        {
        	str = " \t E-Mail : " + "Invalid";
        }
        s1.setText(str);
        s1.setPadding(new Insets(10,0,0,10));
        s1.setPrefSize(600, 100);
        s1.setStyle("-fx-text-fill: #3a3a3a; -fx-font-size: 20; -fx-background-color: #ffffff;");
    	
        ans.getChildren().addAll(s1, line1);
        
        Label s2 = new Label();
        String st = "Customer : 121\tDOB : 18-03-1999"
        		+ "\n\nState : "+"Delhi\tCity : New Delhi"
        				+ "\n\nVerified : OK";
        s2.setText(st);
        s2.setPadding(new Insets(10,0,0,10));
        s1.setPrefSize(900, 100);
        s2.setStyle("-fx-text-fill: #3a3a3a; -fx-font-size: 20; -fx-background-color: #ffffff;");
        
        ans.getChildren().addAll(s2, line2);
		
    	return ans;
    }
}

