import java.io.Serializable;

class StoreHouse extends StoreHouseAdmin implements Serializable
{
	private long storehouseid;
	private StoreHouseAdmin admin;
	private Address location ;
	public StoreHouse(long n, String c, String s)
	{
		this.setLocation(new Address(c,s));
		this.setStorehouseid(n);
	}
	public Address getLocation() {
		return location;
	}
	public void setLocation(Address location) {
		this.location = location;
	}
	public StoreHouseAdmin getAdmin() {
		return admin;
	}
	public void setAdmin(StoreHouseAdmin admin) {
		this.admin = admin;
	}
	public long getStorehouseid() 
	{
		return storehouseid;
	}
	public void setStorehouseid(long id) 
	{
		this.storehouseid = id;
	}
	public String toString()
	{
		return(storehouseid + " " + admin + " " + location);
	}
}
public class storehouse {

}
