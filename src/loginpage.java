import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javafx.animation.FadeTransition;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCharacterCombination;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.Mnemonic;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class loginpage extends Application {
	
    private double xOffset = 0;
    private double yOffset = 0;
    private static SuperUser akki = new SuperUser();
    public static String MD5(String input) 
    { 
        try 
        { 
            MessageDigest md = MessageDigest.getInstance("MD5"); 
            byte[] messageDigest = md.digest(input.getBytes()); 
            BigInteger no = new BigInteger(1, messageDigest); 
            String hashtext = no.toString(16); 
            while (hashtext.length() < 32) 
            { 
                hashtext = "0" + hashtext; 
            } 
            return hashtext; 
        }  
        catch (NoSuchAlgorithmException e) 
        { 
            throw new RuntimeException(e); 
        } 
    }
    public static void main(String[] args) 
    {
        launch(args);
    }
    public static GridPane Admin(final Stage primaryStage) throws FileNotFoundException
    {
		GridPane grid1 = new GridPane();
        grid1.getStylesheets().add("button.css");
        DropShadow ee = new DropShadow();
    	ee.setWidth(20);
    	ee.setHeight(20);
    	ee.setOffsetX(10);
    	ee.setOffsetY(10);
    	ee.setRadius(10);
        Image img = new Image(new FileInputStream("customer.png"));
        ImageView i = new ImageView(img);
        i.setEffect(ee);
        i.setX(10);
        i.setY(10);
        i.setFitHeight(300); 
      	i.setFitWidth(300);
      	i.setPreserveRatio(true);
      	i.setStyle("-fx-background-color: #494949;");
        grid1.add(i, 1, 0);
        grid1.setPadding(new Insets(10, 10, 10, 20));
        grid1.setStyle("-fx-background-color: #494949;");
        
        
        grid1.setMinHeight(600);
        grid1.setMinWidth(600);
        grid1.setMaxWidth(500);
        
        Button submit = new Button();
        submit.setText("Login");
		submit.setId("LoginButton");
		Button clear = new Button();
        clear.setText("Reset");
		clear.setId("LoginButton");
		
        final TextField username = new TextField();
        username.setPromptText("Customer Login");
//        username.setText("sachin");
        username.setText("");
        username.setAlignment(Pos.BASELINE_LEFT);
        final PasswordField password = new PasswordField();
        password.setPromptText("Password");
//        password.setText("1234");
        password.setText("");
        password.setAlignment(Pos.BASELINE_LEFT);
        final TextField house = new TextField();
        house.setPromptText("Store Number");
        house.setAlignment(Pos.BASELINE_LEFT);
//        house.setText("");
        house.setText("1");
        
        
	  	Label name = new Label();
        name.setStyle("-fx-text-fill: #ffffff;");
        name.setText("USERNAME");
        Label pass = new Label();	
        pass.setStyle("-fx-text-fill: #ffffff;");
        pass.setText("PASSWORD");
        Label store = new Label();	
        store.setStyle("-fx-text-fill: #ffffff;");
        store.setText("STORE HOUSE");
        
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText(null);
        alert.setContentText("Logged In successfully !");
        alert.initStyle(StageStyle.UNDECORATED);
        
        Alert err = new Alert(AlertType.ERROR);
        err.setTitle("Error Dialog");
        err.setHeaderText(null);
		err.setContentText("Incorrect Login or Password !");
		err.initStyle(StageStyle.UNDECORATED);
        
        submit.setOnAction(e -> 
        {
        	System.out.println(username.getText()+password.getText());
        	SuperUser akki = read.read();
        	if(akki!=null)
        	{
        		boolean flag = false;
        		for(Customer a : akki.getCustomers())
            	{
        			if(username.getText().equals(a.identity) && password.getText().equals(a.pass))
        			{
        				flag = true;
        				alert.showAndWait();
                		customer_login obj = new customer_login();
                		try 
                		{
                			for(StoreHouse x : akki.getStores())
                			{
                				System.out.println(x+"Check kar raha hoon !");
                				if(x.getStorehouseid() == Long.parseLong(house.getText()))
                				{
                					System.out.println(x+"Ye wala h!");
                					primaryStage.close();
                        			Stage n = new Stage();
                        			obj.cust = a;
                        			obj.storeid = Long.parseLong(house.getText());
                        			obj.house = x;
                					obj.start(n);
                				}
                			}
                			
        				} 
                		catch (FileNotFoundException e1) 
                		{
        					e1.printStackTrace();
        				}
        			}
            	}
        		if(flag == false)
        		{
        			err.showAndWait();
        		}
        	}
  		});
        
  		clear.setOnAction(e ->
  		{
	  		username.setText(null);
	  		password.setText(null);
		}); 
		submit.setMinWidth(350);
		clear.setMinWidth(350);
		Button btn1 = new Button();
		btn1.setText("Guest Login");
		btn1.setId("LoginButton");
		Button btn2 = new Button();
		btn2.setText("Create Account");
		btn2.setId("LoginButton");
		btn1.setMinWidth(350);
		btn2.setMinWidth(350);
        grid1.setHgap(15);grid1.setVgap(55);
        grid1.maxWidth(50);
        grid1.add(name, 0, 1);
        grid1.add(username, 1, 1);
        grid1.add(pass, 0, 2);
        grid1.add(password, 1, 2);
        grid1.add(store, 0, 3);
        grid1.add(house, 1, 3);
        grid1.add(submit, 1, 4);
        grid1.add(clear, 1, 5);
        grid1.add(btn1, 1, 6);
//        grid1.add(btn2, 1, 6);
        grid1.setAlignment(Pos.TOP_LEFT);
        
        btn1.setOnAction(e ->
        {
        	SuperUser akki = read.read();
        	for(StoreHouse x : akki.getStores())
        	{
        		if(x.getStorehouseid() == Long.parseLong(house.getText()))
        		{
        			alert.showAndWait();
                	customer_login obj = new customer_login();
            		try 
            		{
            			
            			primaryStage.close();
            			Stage n = new Stage();
            			obj.cust = null;
            			obj.storeid = Long.parseLong(house.getText());
            			obj.house = x;
        				obj.start(n);
        			} 
            		catch (FileNotFoundException e1) 
            		{
        				e1.printStackTrace();
        			}
        		}
        	}
        	
        });
        return grid1;
    }
    public static GridPane Customer(final Stage primaryStage) throws FileNotFoundException
    {
    	GridPane grid2 = new GridPane();
        grid2.getStylesheets().add("button.css");
        DropShadow ee = new DropShadow();
    	ee.setWidth(20);
    	ee.setHeight(20);
    	ee.setOffsetX(10);
    	ee.setOffsetY(10);
    	ee.setRadius(10);
        Image img = new Image(new FileInputStream("admin.png"));
        ImageView i = new ImageView(img);
        i.setEffect(ee);
        i.setX(10);
        i.setY(10);
        i.setFitHeight(300); 
      	i.setFitWidth(300);
      	i.setPreserveRatio(true);
      	i.setStyle("-fx-background-color: #494949;");
        grid2.add(i, 1, 0);
        grid2.setPadding(new Insets(10, 10, 10, 50));
        grid2.setStyle("-fx-background-color: #3a3a3a;");
        grid2.setAlignment(Pos.BASELINE_RIGHT);
        grid2.setMinHeight(600);
        grid2.setMinWidth(600);
        grid2.setMaxWidth(500);
        
        Button submit1 = new Button();
        submit1.setText("Login");
		submit1.setId("LoginButton");
		Button clear1 = new Button();
        clear1.setText("Reset");
		clear1.setId("LoginButton");
        submit1.setMinWidth(350);
		clear1.setMinWidth(350);
		
		
        final TextField username1 = new TextField();
        username1.setPromptText("admin@supermarche.com");
        username1.setAlignment(Pos.BASELINE_LEFT);
        //******************************************************
//        username1.setText("StoreHouse1@superstore.com");
        username1.setText("");
        //******************************************************
        final PasswordField password1 = new PasswordField();
        password1.setPromptText("Password");
        password1.setAlignment(Pos.BASELINE_LEFT);
        //******************************************************
//        password1.setText("StoreHouse@1");
        password1.setText("");
        //******************************************************
        Label name = new Label();
        name.setStyle("-fx-text-fill: #ffffff;");
        name.setText("USERNAME");
        Label pass = new Label();
        pass.setStyle("-fx-text-fill: #ffffff;");
        pass.setText("PASSWORD");
//        username1.setText(null);
//		password1.setText(null);
        grid2.setHgap(15);grid2.setVgap(60);
        grid2.maxWidth(50);
        grid2.add(name, 0, 1);
        grid2.add(username1, 1, 1);
        grid2.add(pass, 0, 2);
        grid2.add(password1, 1, 2);
        grid2.add(submit1, 1, 3);
        grid2.add(clear1, 1, 4);
        
        grid2.setAlignment(Pos.TOP_LEFT);
        
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText(null);
        alert.setContentText("Logged In successfully !");
        alert.initStyle(StageStyle.UNDECORATED);
        
        Alert err = new Alert(AlertType.ERROR);
        err.setTitle("Error Dialog");
        err.setHeaderText(null);
		err.setContentText("Incorrect Login or Password !");
		err.initStyle(StageStyle.UNDECORATED);
		
        submit1.setOnAction(e -> 
        {
        	System.out.println(username1.getText()+password1.getText());
        	boolean flag = false;
        	WareHouse w = null;
        	StoreHouse s = null;
        	
        	akki = read.read();
        	System.out.println(akki.getWarehouse_admin()+" "+akki.getWarehouses());
        	if(akki!=null)
        	{
        		for(WareHouse a : akki.getWarehouses())
            	{
//        			System.out.println(a.getAdmin());
//        			System.out.println(a.getAdmin().getUsername());
//        			System.out.println(a.getAdmin().getUsername().equals(username1.getText()));
//        			System.out.println(MD5(a.getAdmin().getPassword()).equals(MD5(password1.getText())));
            		if(a.getAdmin()!=null && a.getAdmin().getUsername().equals(username1.getText()) && MD5(a.getAdmin().getPassword()).equals(MD5(password1.getText())))
            		{
            			w = a;
            			flag = true;
            		}
            	}
            	for(StoreHouse a : akki.getStores())
            	{
            		if(a.getAdmin()!=null && a.getAdmin().getUsername().equals(username1.getText()) && MD5(a.getAdmin().getPassword()).equals(MD5(password1.getText())))
            		{
            			s = a;
            			flag = true;
            		}
            	}
            	if(w!=null)
            	{
            		alert.showAndWait();
            		w_admin obj = new w_admin();
            		try 
            		{
            			primaryStage.close();
            			Stage n = new Stage();
            			w_admin.warehouse = w;
            			w_admin.name = username1.getText();
//            			System.out.println(w.getUsername()+" "+w);
    					obj.start(n);
    				} 
            		catch (FileNotFoundException e1) 
            		{
    					e1.printStackTrace();
    				}
            	}
            	else if(s != null)
            	{
            		alert.showAndWait();
            		s_admin obj = new s_admin();
            		try 
            		{
            			primaryStage.close();
            			Stage n = new Stage();
            			s_admin.store = s;
            			s_admin.name = username1.getText();
    					obj.start(n);
    				} 
            		catch (FileNotFoundException e1) 
            		{
    					e1.printStackTrace();
    				}
            	}
        	}
        	
        	if(username1.getText().equals("super") && MD5("1234").equals(MD5(password1.getText())))
        	{
        		alert.showAndWait();
        		s_user obj = new s_user();
        		try 
        		{
        			primaryStage.close();
        			Stage n = new Stage();
					obj.start(n);
				} 
        		catch (FileNotFoundException e1) 
        		{
					e1.printStackTrace();
				}
        	}
        	else if(flag == false)
        	{
        		err.showAndWait();
        	}
  		});
  		clear1.setOnAction(e ->
  		{
	  		username1.setText(null);
	  		password1.setText(null);
		});
  		return grid2;
    }
    @Override
    public void start(final Stage primaryStage) throws FileNotFoundException {
        primaryStage.initStyle(StageStyle.UNDECORATED);
        
        BorderPane root = new BorderPane();
        root.setStyle("-fx-background-color: #ffffff;");
        
        final Tooltip tooltip = new Tooltip();
        tooltip.setText("\nClose\n");
        root.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });
        root.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                primaryStage.setX(event.getScreenX() - xOffset);
                primaryStage.setY(event.getScreenY() - yOffset);
            }
        });
        
        GridPane grid = new GridPane();
        grid.getStylesheets().add("button.css");
        grid.setStyle("-fx-background-color: #dc143c;");
        Button exit = new Button();
        Button mini = new Button();
        Label text = new Label();
        text.setText("Super Marche");
        text.setStyle("-fx-text-fill: #ffffff;");
        text.setId("MarketName");
        mini.setId("OpenMinButton");
        exit.setId("OpenMinButton");
        exit.setTooltip(new Tooltip("Close"));
        mini.setTooltip(new Tooltip("Minimize"));
        exit.setText("x");
        mini.setText("-");
        exit.setOnAction(e -> primaryStage.close());
        mini.setOnAction(e -> {((Stage)((Button)e.getSource()).getScene().getWindow()).setIconified(true);});
        grid.setPadding(new Insets(5, 0, 10, 5));
        grid.setVgap(5); 
        grid.setHgap(5);
//        grid.setAlignment(Pos.TOP_CENTER);
//        gridname.setAlignment(Pos.TOP_LEFT);
//        grid.setMaxSize(3, 3);
//		grid.setMinHeight(400);
//		grid.setMinWidth(900);
//        grid.setMinSize(3, 3);
        ColumnConstraints col1 = new ColumnConstraints();
//        col1.setPercentWidth(86.6);
        col1.setPercentWidth(94);
        ColumnConstraints col2 = new ColumnConstraints();
        col2.setPercentWidth(3);
        ColumnConstraints col3 = new ColumnConstraints();
        col3.setPercentWidth(3);
        grid.getColumnConstraints().addAll(col1);
        text.setAlignment(Pos.TOP_LEFT);
        
//        text.setStyle("-fx-background-color: #000000;");
        grid.add(exit, 2, 0);
        grid.add(mini, 1, 0);
        grid.add(text, 0, 0);
        root.setTop(grid);
//        grid.setConstraints(text, 0, 3);
//**************************************************************************************************************
//**************************************************************************************************************
//**************************************************************************************************************
//**************************************************************************************************************
//**************************************************************************************************************
//**************************************************************************************************************
		
        
//        hbox.getChildren().addAll(i, grid1);
        VBox over1 = new VBox();
        over1.setStyle("-fx-background-color: #474747;");
    	over1.setMaxWidth(500);
        over1.setMinWidth(600);
        over1.setMinHeight(600);
    	System.out.println("-fx-background-color: #474747;");
    	
        
//        over1.setOnMouseEntered(e ->
//        {
//        	FadeTransition ft = new FadeTransition(Duration.millis(3000), over1);
//            ft.setFromValue(1.0);
//            ft.setToValue(0.0);
//            ft.play();
//            try 
//            {
//				root.setRight(Customer(primaryStage));
//			} 
//            catch (FileNotFoundException e1) 
//            {
//				e1.printStackTrace();
//			}
//        });
//        over1.setOnMouseExited(e ->
//        {
//        	FadeTransition ft = new FadeTransition(Duration.millis(3000), over1);
//            ft.setFromValue(0.0);
//            ft.setToValue(1.0);
//            ft.play();
////            root.setRight(over1);
//        });
        
        
        
    	root.setRight(Customer(primaryStage));
        
        VBox over2 = new VBox();
        over2.setStyle("-fx-background-color: #3a3a3a;");
    	over2.setMaxWidth(500);
        over2.setMinWidth(600);
        over2.setMinHeight(600);
        
        root.setLeft(Admin(primaryStage));
//        root.setLeft(over2);
		

		GridPane foot = new GridPane();
		foot.setStyle("-fx-background-color: #dc143c;");
		foot.setPadding(new Insets(10, 10, 10, 20));
		Label name = new Label("VENOM");
		name.setStyle("-fx-text-fill: #ffffff;");
		Label pass = new Label(" v 1.0 bETA");
		pass.setStyle("-fx-text-fill: #ffffff;");
		name.setAlignment(Pos.CENTER_LEFT);
		pass.setAlignment(Pos.CENTER_RIGHT);
		
		foot.add(name, 0, 0);
		foot.add(pass, 1, 0);
		root.setBottom(foot);
		
//				Stop[] stops = new Stop[] { new Stop(0, Color.BLACK), new Stop(1, Color.RED)};
//		LinearGradient lg1 = new LinearGradient(1, 0, 0, 0, true, CycleMethod.NO_CYCLE, stops);
//		root.setStyle("-fx-background-color: lg1");

        Scene scene = new Scene(root, 1200, 900);
        primaryStage.setScene(scene);
        primaryStage.setMinHeight(900);
        primaryStage.setMinWidth(1200);
        primaryStage.show();
    }
}