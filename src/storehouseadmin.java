import java.io.Serializable;
import java.math.*;
import java.security.*;
import java.util.*;

class StoreHouseAdmin implements Admin, Serializable
{
	private DataBase root = new Category();
	private String username;
	private String password;
	private boolean status ;
	private HashMap<Category,Category> productpath;
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public static long getId() {
		return id;
	}
	public static void setId(long id) {
		StoreHouseAdmin.id = id;
	}
	private HashMap<Long, Category> productid;
	private static long id;
	
	public StoreHouseAdmin(long n, String user, String pass)
	{
		id = n;
		productid = new HashMap<Long, Category>();
		productpath = new HashMap<Category, Category>();
		this.setPassword(pass);
		this.setUsername(user);
	}
	public StoreHouseAdmin()
	{
		productid = new HashMap<Long, Category>();
		productpath = new HashMap<Category, Category>();
		this.setPassword(null);
		this.setUsername(null);
	}
	public void InsertByPath(String path, String product)
	{
		
		String arr[] = path.split(" ");
		DataBase node = this.getRoot();
		ArrayList<Category> list = node.arr;
		String t;
		HashMap<Category, Category> ppath = this.getProductpath();
		HashMap<Long, Category> pid = this.getProductid();
		
		boolean flag ;
		
		for(int i=0;i<arr.length+1;i++)
		{
			if(i<arr.length)
				t = arr[i];
			else
				t = product;
			if(list.size()==0)
			{
				DataBase fresh = new Category(t);
				list.add((Category) fresh);
				node.setArr(list);
				list = fresh.arr;
				ppath.put((Category) fresh, (Category) node);
				pid.put(((Category)fresh).getId(), (Category) fresh);
				node = fresh;
			}
			else
			{
				flag = false;
				for(int j=0; j<list.size(); j++)
				{
					if(list.get(j).getType().equals(t))
					{
						flag = true;
						node = list.get(j);
						list = node.getArr();
						break;
					}
				}
				if(!flag)
				{
					DataBase fresh = new Category(t);
					list.add((Category) fresh);
					node.setArr(list);
					ppath.put((Category) fresh, (Category) node);
					pid.put(((Category)fresh).getId(), (Category) fresh);
					list = fresh.getArr();
					node = fresh;
				}
			}
		}
		this.setProductid(pid);
		this.setProductpath(ppath);
	}
	
	public void InsertById(String p, long c) throws DuplicacyException
	{
		HashMap<Long, Category> pid = this.getProductid();
		HashMap<Category, Category> ppath = this.getProductpath();
		Category obj = pid.get(c);
		if(obj==null)
		{
			throw(new DuplicacyException());
		}
		DataBase fresh = new Category(p);
		ArrayList<Category> list = obj.getArr();
		list.add((Category) fresh);
		pid.put(((Category)fresh).getId(), (Category) fresh);
		ppath.put((Category) fresh, obj);
	}
	public void Add(long id, Category prd)
	{
		HashMap<Long, Category> pid = this.getProductid();
		pid.put(id, prd);
		this.setProductid(pid);
	}
	public WareHouse GenerateOrder(ArrayList<Category> list, SuperUser akki, StoreHouse obj)
	{
		ArrayList<WareHouse> ware = akki.getWarehouses();
		ArrayList<WareHouse> temp = new ArrayList<WareHouse>();
		boolean ans = true;
		for(WareHouse a : ware)
		{
			ans = true;
			for(Category b : list)
			{
				try 
				{
					Category t = a.SearchById(b.getId());
					System.out.println(t+"Mid Search"); 
					if(t!=null && t.getQty()>=b.getQty())
					{
						System.out.println("Product Found");
					}
					else
					{
						ans = false;
					}
				} 
				catch (ProductNotFoundException e) 
				{
					ans = false;
					System.out.println("Product Not Found");
				}
			}
			if(ans == true)
			{
				temp.add(a);
			}
		}
		if(temp.size()==0)
		{
			System.out.println("Order Not Generated");
			return null;
		}
		WareHouse f = temp.get(0);
		System.out.println(temp+"First Priority");
		for(WareHouse a : temp)
		{
			if(a.getLocation().getState().equalsIgnoreCase(obj.getLocation().getState()) && a.getLocation().getCity().equalsIgnoreCase(obj.getLocation().getCity()))
			{
				f = a;
				return a;
			}
		}
		System.out.println(list+"CHecking");
		f.getAdmin().setOrder(list);
		System.out.println(f+" is connected to "+obj.getAdmin());
		f.getAdmin().setRequest(obj.getAdmin());
		return f;
		
		
		
	}
	public void ModifyById(double cost, int q, long c, String n)
	{
		HashMap<Long, Category> pid = this.getProductid();
		HashMap<Category, Category> ppath = this.getProductpath();
		Category obj = pid.get(c);
		obj.setPrize(cost);
		obj.setQty(q);
		obj.setType(n);
	}
	public boolean DeleteById(long c)
	{
		try 
		{
			SearchById(c);
		} 
		catch (ProductNotFoundException e) 
		{
			System.out.println("Product Not Found Exception");
			return false;
		}
		HashMap<Long, Category> pid = this.getProductid();
		HashMap<Category, Category> ppath = this.getProductpath();
		Category obj = ppath.get(((pid.get(c))));
		ArrayList<Category> list = obj.getArr();
		list.remove(pid.get(c));
		obj.setArr(list);
		return true;
	}
	public Category SearchByName(String n) throws ProductNotFoundException
	{
		Category ans = null;
		ArrayList<Category> queue = (ArrayList<Category>) root.getArr().clone();
		
		while(!queue.isEmpty())
		{
			Category c = queue.get(0);
			queue.remove(0);
			if(c.getType().equals(n))
			{
				ans = c;
				return ans;
			}
			for(Category a : c.getArr())
			{
				if(a.getArr().equals(n))
				{
					ans = a;
					return ans;
				}
				queue.addAll(a.getArr());
			}
		}
		if(ans == null)
		{
			throw(new ProductNotFoundException());
		}
		return ans;
	}
	public Category SearchById(long c) throws ProductNotFoundException
	{
		HashMap<Long, Category> pid = this.getProductid();
		Category obj = null;
		obj = pid.get(c);
		if(obj == null)
		{
			throw(new ProductNotFoundException());
		}
		return obj;
	}
	public ArrayList<Category> PartialSearch(String n, long id) throws ProductNotFoundException
	{
		ArrayList<Category> ans = new ArrayList<Category>();
		SuperUser akki = read.read();
		System.out.println(akki.getStores().get(0).getProductid());
		for(StoreHouse a : akki.getStores())
		{
			System.out.println(a.getAdmin().getProductid().keySet()+"*");
			if(a.getStorehouseid() == id)
			{
				if(n.equals(""))
				{
					for(long c : a.getAdmin().getProductid().keySet())
					{
						ans.add(a.getAdmin().getProductid().get(c));
					}
					return ans;
				}
				else
				{
					for(long c : a.getAdmin().getProductid().keySet())
					{
						if(a.getAdmin().getProductid().get(c).getType().contains(n) || n.contains(a.getAdmin().getProductid().get(c).getType()))
						{
							ans.add(a.getAdmin().getProductid().get(c));
						}
					}
					return ans;
				}
				
			}
		}
		return ans;
	}
	public static String MD5(String input) 
    { 
        try 
        { 
            MessageDigest md = MessageDigest.getInstance("MD5"); 
            byte[] messageDigest = md.digest(input.getBytes()); 
            BigInteger no = new BigInteger(1, messageDigest); 
            String hashtext = no.toString(16); 
            while (hashtext.length() < 32) 
            { 
                hashtext = "0" + hashtext; 
            } 
            return hashtext; 
        }  
        catch (NoSuchAlgorithmException e) 
        { 
            throw new RuntimeException(e); 
        } 
    } 
	public boolean CheckForCredentials(String id, String pass) 
	{
		boolean ans = false;
		if(MD5(pass).equals(this.getPassword()) && id.equals(this.getUsername()))
		{
			ans = true;
		}
		return ans;
	}
	public DataBase getRoot() {
		return root;
	}
	public void setRoot(DataBase root) {
		this.root = root;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public HashMap<Category, Category> getProductpath() {
		return productpath;
	}
	public void setProductpath(HashMap<Category, Category> productpath) {
		this.productpath = productpath;
	}
	public HashMap<Long, Category> getProductid() {
		return productid;
	}
	public void setProductid(HashMap<Long, Category> productid) {
		this.productid = productid;
	}
	public String toString()
	{
		return(id+" "+username + " "+ password);
	}
	@Override
	public void ModifyById(double cost, int q, long c) {
		// TODO Auto-generated method stub
		
	}
}
public class storehouseadmin {

}
