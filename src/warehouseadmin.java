import java.io.Serializable;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
class WareHouseAdmin implements Admin, Serializable
{
	private DataBase root = new Category();
	private String username;
	private String password;
	private boolean status;
	private HashMap<Category,Category> productpath;
	private HashMap<Long, Category> productid;
	private long id;
	private ArrayList<Category> order ;
	private StoreHouseAdmin request;
	
	public void setRequest(StoreHouseAdmin obj)
	{
		this.request = obj;
	}
	public StoreHouseAdmin getRequest()
	{
		return(this.request);
	}
	public void setOrder(ArrayList<Category> obj)
	{
		this.order = obj;
	}
	public ArrayList<Category> getOrder()
	{
		return(this.order);
	}
	public WareHouseAdmin(long n, String user, String pass)
	{
		this.setId(n);
		productid = new HashMap<Long, Category>();
		productpath = new HashMap<Category, Category>();
		this.setPassword(pass);
		this.setUsername(user);
	}
	public WareHouseAdmin()
	{
		productid = new HashMap<Long, Category>();
		productpath = new HashMap<Category, Category>();
		this.setPassword(null);
		this.setUsername(null);
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public SuperUser HandleOrder(WareHouse house) throws InsufficientStockException
	{
		SuperUser akki = read.read();
		for(WareHouse a : akki.getWarehouses())
		{
			System.out.println(a.getAdmin().request+"akki"+a.getAdmin().order+"akki"+house.getAdmin().getId());
			if(a.getAdmin().getId() == house.getAdmin().getId())
			{
				this.request = a.getAdmin().getRequest();
				this.order = a.getAdmin().getOrder();
			}
		}
		if(this.request == null || this.order == null)
		{
			return akki ;
		}
		boolean ans = true;
		System.out.println(this.order+"Handle Order");
		System.out.println(this.request+"Requested Order");
		for(Category b : this.order)
		{
			try
			{
				Category t = this.SearchById(b.getId());
				if(t!=null && t.getQty()>=b.getQty())
				{
					System.out.println("Product Found");
				}
			} 
			catch (ProductNotFoundException e) 
			{
				ans = false;
				System.out.println("Product Not Found");
			}
		}
		if(ans == false)
		{
			throw(new InsufficientStockException());
		}
		for(StoreHouse a : read.read().getStores())
		{
			System.out.println(a.getAdmin().getProductid()+"Abe Jaa naa"+a);
		}
		
		System.out.println(akki+"************");
		for(StoreHouse a : akki.getStores())
		{
			StoreHouseAdmin temp = null;
			if(a.getAdmin().getId() == this.request.getId())
			{
				System.out.println(a+"Chosen One");
				for(Category b : this.order)
				{
					Category te = null;
					try 
					{
						te = SearchById(b.getId());
					}
					catch (ProductNotFoundException e) 
					{
						System.out.println("Product Not Found");
					}
					System.out.println(te+"Before Modified Product");
					ModifyById(te.getPrize(), (int) (te.getQty()-b.getQty()), te.getId(), te.getType());
					System.out.println(te+"After Modified Product");
					
					temp = a.getAdmin();
					this.request = a.getAdmin();
					System.out.println(this.request);
					System.out.println(b.getType()+"Kya hua ");
					this.request.Add(b.getId(), new Category(b.getType(), b.getQty(), b.getId()));
//					a.setAdmin(temp);
//					a.getAdmin().getProductid().put(b.getId(), new Category(b.getType()));
					System.out.println(a.getAdmin().getProductid());
				}
			}
		}
		for(WareHouseAdmin x : akki.getWarehouse_admin())
		{
			if(x.getUsername().equals(this.getUsername()))
			{
				x.setOrder(null);
				x.setRequest(null);
			}
		}
//		System.out.println("After Writing");
//		SuperUser temp = read.read();
//		System.out.println(temp+"*********");
//		for(StoreHouse x : temp.getStores())
//		{
//			System.out.println(x.getAdmin().getProductid()+"Abe Jaa naa"+x);
//		}
		return akki;
	}
	public boolean CheckForCredentials(String id, String pass) 
	{
		boolean ans = false;
		if(MD5(pass).equals(this.getPassword()) && id.equals(this.getUsername()))
		{
			ans = true;
		}
		return ans;
	}

	public void InsertByPath(String path, String product)
	{
		String arr[] = path.split(" ");
		DataBase node = new Category();
		node = this.getRoot();
		ArrayList<Category> list = node.arr;
		String t;
		HashMap<Category, Category> ppath = this.getProductpath();
		HashMap<Long, Category> pid = this.getProductid();
		
		boolean flag ;
		
		for(int i=0;i<arr.length+1;i++)
		{
			if(i<arr.length)
				t = arr[i];
			else
				t = product;
			System.out.println(t+"TobeEntered"+list.size());
			if(list.size()==0)
			{
				DataBase fresh = new Category(t);
				list.add((Category) fresh);
				node.setArr(list);
				list = fresh.arr;
				ppath.put((Category)fresh, (Category)node);
				pid.put(((Category)fresh).getId(), (Category) fresh);
				node = fresh;
			}
			else
			{
				flag = false;
				for(int j=0; j<list.size(); j++)
				{
					if(list.get(j).getType().equals(t))
					{
						flag = true;
						node = list.get(j);
						list = node.getArr();
						break;
					}
				}
				if(!flag)
				{
					DataBase fresh = new Category(t);
					list.add((Category) fresh);
					node.setArr(list);
					ppath.put((Category) fresh, (Category) node);
					pid.put(((Category)fresh).getId(), (Category) fresh);
					list = fresh.getArr();
					node = fresh;
				}
			}
		}
		for(WareHouse x : read.read().getWarehouses())
		{
			if(x.getAdmin().getId() == this.getId())
			{
				x.setProductid(pid);
				x.setProductpath(ppath);
			}
		}
	}

	public void InsertById(String p, long c) 
	{
		HashMap<Long, Category> pid = this.getProductid();
		HashMap<Category, Category> ppath = this.getProductpath();
		Category obj = pid.get(c);
		DataBase fresh = new Category(p);
		ArrayList<Category> list = obj.getArr();
		list.add((Category) fresh);
		pid.put(((Category)fresh).getId(), (Category) fresh);
		ppath.put((Category) fresh, obj);
	}

	public boolean ModifyById(double cost, int q, long c, String name) 
	{
		try 
		{
			SearchById(c);
		} 
		catch (ProductNotFoundException e) 
		{
			System.out.println("Product Not Found Exception");
			return false;
		}
		HashMap<Long, Category> pid = this.getProductid();
		HashMap<Category, Category> ppath = this.getProductpath();
		Category obj = pid.get(c);
		obj.setPrize(cost);
		obj.setQty(q);
		obj.setType(name);
		return true;
	}

	public boolean DeleteById(long c) 
	{
		try 
		{
			SearchById(c);
		} 
		catch (ProductNotFoundException e) 
		{
			System.out.println("Product Not Found Exception");
			return false;
		}
		HashMap<Long, Category> pid = this.getProductid();
		HashMap<Category, Category> ppath = this.getProductpath();
		Category obj = ppath.get(((pid.get(c))));
		ArrayList<Category> list = obj.getArr();
		list.remove(pid.get(c));
		obj.setArr(list);
		return true;
	}

	public Category SearchByName(String n) throws ProductNotFoundException 
	{
		Category ans = null;
		ArrayList<Category> queue = (ArrayList<Category>) root.getArr().clone();
		while(!queue.isEmpty())
		{
			Category c = queue.get(0);
			queue.remove(0);
			if(c.getType().equals(n))
			{
				ans = c;
				return ans;
			}
			for(Category a : c.getArr())
			{
				if(a.getArr().equals(n))
				{
					ans = a;
					return ans;
				}
				queue.addAll(a.getArr());
			}
		}
		if(ans == null)
		{
			throw(new ProductNotFoundException());
		}
		return ans;
	}

	public Category SearchById(long c) throws ProductNotFoundException 
	{
		HashMap<Long, Category> pid = this.getProductid();
		Category obj = null;
		obj = pid.get(c);
		if(obj == null)
		{
			throw(new ProductNotFoundException());
		}
		return obj;
	}
	public ArrayList<Category> PartialSearch(String n) throws ProductNotFoundException
	{
		ArrayList<Category> ans = new ArrayList<Category>();
		ArrayList<Category> queue = (ArrayList<Category>) root.getArr().clone();
		System.out.println(n.equals(""));
		if(n.equals(""))
		{
			while(!queue.isEmpty())
			{
				System.out.println(queue+"Hello");
				System.out.println(ans+"Bye");
				Category c = queue.get(0);
				queue.remove(0);
				System.out.println(queue+"PartialSearch"+n+" "+c.getType());
				ans.add(c);
				for(Category a : c.getArr())
				{
					queue.add(a);
//					queue.addAll(a.getArr());
				}
			}
			System.out.println(ans+"Before Returning");
			return ans;
		}
		
		ans = new ArrayList<Category>();
		queue = (ArrayList<Category>) root.getArr().clone();
		
		while(!queue.isEmpty())
		{
			Category c = queue.get(0);
			queue.remove(0);
			System.out.println(queue+"PartialSearchAfterSearching&&"+n+" "+c.getType());
			if(c.getType().contains(n) || n.contains(c.getType()))
			{
				ans.add(c);
			}
			for(Category a : c.getArr())
			{
				if(a.getType().contains(n) || n.contains(a.getType()))
				{
					ans.add(a);
				}
				else
				{
					queue.addAll(a.getArr());
				}
			}
		}
		if(ans.size() == 0)
		{
			ans = null;
			throw(new ProductNotFoundException());
		}
		return ans;
	}
	public static String MD5(String input) 
    { 
        try 
        { 
            MessageDigest md = MessageDigest.getInstance("MD5"); 
            byte[] messageDigest = md.digest(input.getBytes()); 
            BigInteger no = new BigInteger(1, messageDigest); 
            String hashtext = no.toString(16); 
            while (hashtext.length() < 32) 
            { 
                hashtext = "0" + hashtext; 
            } 
            return hashtext; 
        }  
        catch (NoSuchAlgorithmException e) 
        { 
            throw new RuntimeException(e); 
        } 
    } 
	public DataBase getRoot() {
		return root;
	}
	public void setRoot(DataBase root) {
		this.root = root;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public HashMap<Category, Category> getProductpath() {
		return productpath;
	}
	public void setProductpath(HashMap<Category, Category> productpath) {
		this.productpath = productpath;
	}
	public HashMap<Long, Category> getProductid() {
		return productid;
	}
	public void setProductid(HashMap<Long, Category> productid) {
		this.productid = productid;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String toString()
	{
		return(id+" "+username + " "+ password);
	}
	@Override
	public void ModifyById(double cost, int q, long c) {
		// TODO Auto-generated method stub
		
	}
}
public class warehouseadmin 
{

}
