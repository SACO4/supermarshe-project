import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.StageStyle;

class DuplicacyException extends Exception 
{
	public DuplicacyException()
	{
		super("Duplicate Entry Exception");
	}
}
class ProductNotFoundException extends Exception 
{
	public ProductNotFoundException()
	{
		super("Product Not Found");
	}
}
class InsufficientBalanceException extends Exception 
{
	public InsufficientBalanceException()
	{
		super("Insufficient Balance Exception");
	}
}
class InsufficientStockException extends Exception 
{
	public InsufficientStockException()
	{
		super("Insufficient Stock Exception");
	}
}
class InvalidIdException extends Exception 
{
	public InvalidIdException()
	{
		super("Invalid Id Exception");
	}
}
class IncorrectLoginException extends Exception 
{
	public IncorrectLoginException()
	{
		super("Invalid Login Exception");
	}
}
public class Error {

}