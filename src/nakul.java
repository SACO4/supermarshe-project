import java.util.ArrayList;
import java.util.Optional;
import java.util.Random;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;


public class nakul extends Application {

	Button leaderboardButton, backButton, playButton, resumeButton, exitButton, pauseButton, yesButton, noButton, pauseExitButton, pauseResumeButton;
	Text gameLabel, leaderboardLabel, exitLabel, pauseLabel, scoreLabel;
	Pane pauseFlow, mainFlow, leaderboardFlow, gameFlow, exitFlow;
	Stage stage1, stage2;
	Scene mainPage, gamePage, leaderboardPage, pausePage, exitPage;
	Circle circle;
	Rectangle block1, block2, block3, block4, block5;
	Rectangle block[] = new Rectangle[5];

	@Override
	public void start(Stage primaryStage) {
		stage1 = primaryStage;		

		//Main Page
		resumeButton = new Button("Resume Game");
		resumeButton.setOnAction(e -> handleButtonAction(e));
		resumeButton.setLayoutX(102);
		resumeButton.setLayoutY(150);
		playButton = new Button("Play Game");
		playButton.setOnAction(e -> handleButtonAction(e));
		playButton.setLayoutX(113);
		playButton.setLayoutY(200);
		leaderboardButton = new Button("Leaderboard");
		leaderboardButton.setOnAction(e -> handleButtonAction(e));
		leaderboardButton.setLayoutX(107);
		leaderboardButton.setLayoutY(250);
		exitButton = new Button("Exit Game");
		exitButton.setOnAction(e -> handleButtonAction(e));
		exitButton.setLayoutX(113);
		exitButton.setLayoutY(300);
		Font font1 = new Font("Georgia", 30);
		gameLabel  = new Text("Snakes vs Blocks");
		gameLabel.setX(38);
		gameLabel.setY(30);
		gameLabel.setFont(font1);
		gameLabel.setFill(Color.WHITE);

		//Main Page Pane
		mainFlow = new Pane();
		mainFlow.getChildren().addAll(gameLabel, resumeButton, playButton, leaderboardButton, exitButton);
		mainFlow.setPadding(new Insets(20));
		mainFlow.setStyle("-fx-background: black;");

		//Initialize Main Page
		mainPage = new Scene(mainFlow, 300, 500);

		//Leaderboard Page
		backButton = new Button("Go Back");
		backButton.setOnAction(e -> handleButtonAction(e));
		backButton.setLayoutX(5);
		backButton.setLayoutY(470);
		Font font2 = new Font("Georgia", 25);
		leaderboardLabel = new Text("Leaderboard");
		leaderboardLabel.setX(80);
		leaderboardLabel.setY(30);
		leaderboardLabel.setFont(font2);
		leaderboardLabel.setFill(Color.WHITE);

		//Leaderboard Page Pane
		leaderboardFlow = new Pane();
		leaderboardFlow.getChildren().addAll(backButton, leaderboardLabel);
		leaderboardFlow.setStyle("-fx-background: black;");

		//Initialize Leaderboard Page
		leaderboardPage = new Scene(leaderboardFlow, 300, 500);

		//Game Page
		pauseButton = new Button("Pause");
		pauseButton.setOnAction(e -> handleButtonAction(e));
		pauseButton.setLayoutX(249);
		pauseButton.setLayoutY(5);
		scoreLabel = new Text("Score: ");
		Font font3 = new Font("Georgia", 19);
		scoreLabel.setX(8);
		scoreLabel.setY(25);
		scoreLabel.setFont(font3);
		scoreLabel.setFill(Color.WHITE);

		//Snake in-game
		circle = new Circle(10, Color.BLUE);
		circle.setCenterX(150);
		circle.setCenterY(450);
		circle.toFront();

		//Block in-game
		Random r = new Random();
		block[0] = new Rectangle(60, 50, 60, 50);
		block[1] = new Rectangle(60, 50, 60, 50);
		block[2] = new Rectangle(60, 50, 60, 50);
		block[3] = new Rectangle(60, 50, 60, 50);
		block[4] = new Rectangle(60, 50, 60, 50);
		block[0].setFill(Color.rgb(r.nextInt(255), r.nextInt(255), r.nextInt(255)));
		String color = ColourGenerator();
		System.out.println(color);
		//		block1.setFill(Color.valueOf(ColourGenerator()));
		block[1].setFill(Color.rgb(r.nextInt(255), r.nextInt(255), r.nextInt(255)));
		block[2].setFill(Color.rgb(r.nextInt(255), r.nextInt(255), r.nextInt(255)));
		block[3].setFill(Color.rgb(r.nextInt(255), r.nextInt(255), r.nextInt(255)));
		block[4].setFill(Color.rgb(r.nextInt(255), r.nextInt(255), r.nextInt(255)));
		block[0].setLayoutX(-60);
		block[1].setLayoutX(0);
		block[2].setLayoutX(60);
		block[3].setLayoutX(120);
		block[4].setLayoutX(180);
		gameFlow = new Pane();
		gameFlow.getChildren().addAll(pauseButton, circle, scoreLabel, block[0], block[1], block[2], block[3], block[4]);
		gameFlow.setPadding(new Insets(20, 20, 20, 20));
		gameFlow.setStyle("-fx-background: black;");

		//Initialize Game Page
		gamePage = new Scene(gameFlow, 300, 500);

		//Move Snake
		moveCircleOnKeyPress(gamePage);

		//Move Blocks
		Timeline timeline = new Timeline(new KeyFrame(Duration.millis(3000),ae -> 
		{
			block[0].setFill(Color.rgb(r.nextInt(255)+1, r.nextInt(255)+1, r.nextInt(255)+1));
			block[1].setFill(Color.rgb(r.nextInt(255)+1, r.nextInt(255)+1, r.nextInt(255)+1));
			block[2].setFill(Color.rgb(r.nextInt(255)+1, r.nextInt(255)+1, r.nextInt(255)+1));
			block[3].setFill(Color.rgb(r.nextInt(255)+1, r.nextInt(255)+1, r.nextInt(255)+1));
			block[4].setFill(Color.rgb(r.nextInt(255)+1, r.nextInt(255)+1, r.nextInt(255)+1));
			while(gameFlow.getChildren().size()!=3)
			{
				gameFlow.getChildren().remove(gameFlow.getChildren().size()-1);
			}
			int n = r.nextInt(5)+1;
			if(n==1)
			{
				gameFlow.getChildren().addAll(block[r.nextInt(5)]);
			}
			if(n==2)
			{
				gameFlow.getChildren().addAll(block[r.nextInt(2)], block[r.nextInt(3)+2]);
			}
			if(n==3)
			{
				gameFlow.getChildren().addAll(block[r.nextInt(2)], block[r.nextInt(2)+2], block[4]);
			}
			if(n==4)
			{
				gameFlow.getChildren().addAll(block[r.nextInt(1)], block[r.nextInt(1)+1], block[r.nextInt(1)+2], block[r.nextInt(1)+3]);
			}
			if(n==5)
			{
				gameFlow.getChildren().addAll(block[0], block[1], block[2], block[3], block[4]);
			}
		}
		));
		timeline.setCycleCount(Timeline.INDEFINITE);
		timeline.setAutoReverse(false);

		KeyValue kv1 = new KeyValue(block[0].yProperty(), 500);
		KeyFrame kf1 = new KeyFrame(Duration.millis(3000), kv1);

		KeyValue kv2 = new KeyValue(block[1].yProperty(), 500);
		KeyFrame kf2 = new KeyFrame(Duration.millis(3000), kv2);

		KeyValue kv3 = new KeyValue(block[2].yProperty(), 500);
		KeyFrame kf3 = new KeyFrame(Duration.millis(3000), kv3);

		KeyValue kv4 = new KeyValue(block[3].yProperty(), 500);
		KeyFrame kf4 = new KeyFrame(Duration.millis(3000), kv4);

		KeyValue kv5 = new KeyValue(block[4].yProperty(), 500);
		KeyFrame kf5 = new KeyFrame(Duration.millis(3000), kv5);

		timeline.getKeyFrames().add(kf1);
		timeline.getKeyFrames().add(kf2);
		timeline.getKeyFrames().add(kf3);
		timeline.getKeyFrames().add(kf4);
		timeline.getKeyFrames().add(kf5);

		timeline.play();

		//Exit Page
		yesButton = new Button("Yes");
		yesButton.setOnAction(e -> handlePopUpButtonAction(e));
		yesButton.setLayoutX(10);
		yesButton.setLayoutY(66);
		noButton = new Button("No");
		noButton.setOnAction(e -> handlePopUpButtonAction(e));
		noButton.setLayoutX(160);
		noButton.setLayoutY(66);
		exitLabel = new Text("Are You Sure?");
		Font font4 = new Font("Georgia", 15);
		exitLabel.setX(50);
		exitLabel.setY(30);
		exitLabel.setFont(font4);
		exitLabel.setFill(Color.WHITE);

		//Exit Page Pane
		exitFlow = new Pane();
		exitFlow.getChildren().addAll(exitLabel, yesButton, noButton);
		exitFlow.setPadding(new Insets(20));
		exitFlow.setStyle("-fx-background: black;");

		//Initialize Exit Page
		exitPage = new Scene(exitFlow, 200, 100);

		//Pause Page
		pauseExitButton = new Button("Exit Game");
		pauseExitButton.setOnAction(e -> handlePopUpButtonAction(e));
		pauseExitButton.setLayoutX(42);
		pauseExitButton.setLayoutY(126);
		pauseResumeButton = new Button("Continue");
		pauseResumeButton.setOnAction(e -> handlePopUpButtonAction(e));
		pauseResumeButton.setLayoutX(43);
		pauseResumeButton.setLayoutY(66);
		pauseLabel = new Text("Game Paused");
		pauseLabel.setX(20);
		pauseLabel.setY(30);
		pauseLabel.setFont(font3);
		pauseLabel.setFill(Color.WHITE);

		//Pause Page Pane
		pauseFlow = new Pane();
		pauseFlow.getChildren().addAll(pauseLabel, pauseResumeButton, pauseExitButton);
		pauseFlow.setPadding(new Insets(20));
		pauseFlow.setStyle("-fx-background: black;");
		pauseFlow.setBorder(new Border(new BorderStroke(Color.WHITE, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
		//		pauseFlow.setStyle("-fx-border: 12px;");

		//Initialize Pause Page
		pausePage = new Scene(pauseFlow, 150, 200);

		//Initialize Pop Ups
		stage2 = new Stage();
		stage2.initStyle(StageStyle.UNDECORATED);

		//Initialize App
		primaryStage.setTitle("Snake vs Blocks");
		primaryStage.setScene(mainPage);
		primaryStage.initStyle(StageStyle.UNDECORATED);
		primaryStage.show();
		//		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
	}

	private void handleButtonAction(ActionEvent event) {
		// TODO Auto-generated method stub
		if(event.getTarget() == resumeButton) {
			stage1.setScene(gamePage);
		}
		else if(event.getTarget() == playButton) {
			stage1.setScene(gamePage);
		}
		else if(event.getTarget() == leaderboardButton) {
			stage1.setScene(leaderboardPage);
		}
		else if(event.getTarget() == backButton) {
			stage1.setScene(mainPage);
		}
		else if(event.getTarget() == pauseButton) {
			stage2.setScene(pausePage);
			stage2.showAndWait();
		}
		else if(event.getTarget() == exitButton) {
			Alert alert = new Alert(AlertType.CONFIRMATION);
			//            alert.setTitle("Inform");
			alert.setHeaderText(null);
			alert.setContentText("Are you sure?");
			alert.initStyle(StageStyle.UNDECORATED);
			Optional<ButtonType> result = alert.showAndWait();
			if (result.get() == ButtonType.OK) {
				stage1.close();
			}
		}
	}

	private void handlePopUpButtonAction(ActionEvent event) {
		// TODO Auto-generated method stub
		if(event.getTarget() == noButton) {
			stage2.close();
		}
		else if(event.getTarget() == pauseResumeButton) {
			stage2.close();;
		}
		else if(event.getTarget() == pauseExitButton) {
			stage2.close();
			stage1.setScene(mainPage);
		}
		else if(event.getTarget() == yesButton) {
			stage2.close();
			stage1.close();
		}
	}

	public static String ColourGenerator()
	{
		String ans = "";
		ArrayList<String> list = new ArrayList<String>();
		list.add("0");
		list.add("1");
		list.add("2");
		list.add("3");
		list.add("4");
		list.add("5");
		list.add("6");
		list.add("7");
		list.add("8");
		list.add("9");
		list.add("a");
		list.add("b");
		list.add("c");
		list.add("d");
		list.add("e");
		list.add("f");
		Random rand = new Random();
		int n;
		for(int i=0;i<6;i++)
		{
			n = rand.nextInt(16);
			ans = ans + list.get(n);
			if(i==5 && ans == "ffffff")
			{
				i = -1;
			}
		}
		ans = "#"+ans;
		return ans;
	}

	private void moveCircleOnKeyPress(Scene scene) {
		scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
			@Override 
			public void handle(KeyEvent event) {
				switch (event.getCode()) {
				case UP:    circle.setCenterY(circle.getCenterY() - 10); break;

				case RIGHT: circle.setCenterX(((circle.getCenterX() + 10)>290)?(circle.getCenterX()):(circle.getCenterX() + 10)); break;
				case DOWN:  circle.setCenterY(circle.getCenterY() + 10); break;
				case LEFT:  circle.setCenterX(((circle.getCenterX() - 10)<10)?(circle.getCenterX()):(circle.getCenterX() - 10)); break;
				default:	break;
				}
			}
		}
				);
	}

	public static void main(String[] args) {
		launch(args);
		//		System.out.println(ColourGenerator());
	}
}