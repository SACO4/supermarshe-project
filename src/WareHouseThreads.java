import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

class addproductbypath extends Thread
{
	SuperUser akki;
	WareHouse house ;
	String path;
	String product;
	public addproductbypath(WareHouse obj, String p, String prd)
	{
		this.house = obj;
		this.path = p;
		this.product = prd;
	}
	public void run()
	{
		//********************************************************************
		SuperUser temp = null;
		try
		{
			File data = new File("SuperUserData.txt");
			if(data.exists())
			{
				FileInputStream file = new FileInputStream(data);
				ObjectInputStream in = new ObjectInputStream(file);
				temp = (SuperUser) in.readObject();
				in.close();
				file.close();
			}
			System.out.println("Reading Successful");
		} 
		catch (IOException | ClassNotFoundException e) 
		{
			e.printStackTrace();
		}
		finally
		{
			if(temp == null)
			{
				akki = new SuperUser();
			}
			else
			{
				akki = temp;
			}
		}
		//********************************************************************
		for(WareHouse a : akki.getWarehouses())
		{
			if(a.getWarehouseid() == house.getWarehouseid())
			{
				System.out.println(house+"%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
				house = a ; 
				house.InsertByPath(path, product);
			}
		}
		//********************************************************************
		try
		{
			File data = new File("SuperUserData.txt");
			if(data.exists())
			{
				FileOutputStream file = new FileOutputStream(data);
				ObjectOutputStream out = new ObjectOutputStream(file);
				
				data.delete();
				
				data = new File("SuperUserData.txt");
				file = new FileOutputStream(data);
				out = new ObjectOutputStream(file);
				
				out.writeObject(akki);
				out.close();
				file.close();
			}
			else
			{
				FileOutputStream file = new FileOutputStream(data);
				ObjectOutputStream out = new ObjectOutputStream(file);
				out.writeObject(akki);
				out.close();
				file.close();
			}
			System.out.println("Writing Successful");
		}
		catch(IOException e)
		{
			e.printStackTrace();
//			System.out.println("Error While Writing the Data");
		}
		//********************************************************************
	}
}

class addproductbyid extends Thread
{
	SuperUser akki;
	WareHouse house ;
	int path;
	String product;
	
	public addproductbyid(WareHouse obj, int p, String prd)
	{
		this.house = obj;
		this.path = p;
		this.product = prd;
	}
	public void run()
	{
		//********************************************************************
		SuperUser temp = null;
		try
		{
			File data = new File("SuperUserData.txt");
			if(data.exists())
			{
				FileInputStream file = new FileInputStream(data);
				ObjectInputStream in = new ObjectInputStream(file);
				temp = (SuperUser) in.readObject();
				in.close();
				file.close();
			}
			System.out.println("Reading Successful");
		} 
		catch (IOException | ClassNotFoundException e) 
		{
			e.printStackTrace();
		}
		finally
		{
			if(temp == null)
			{
				akki = new SuperUser();
			}
			else
			{
				akki = temp;
			}
		}
		//********************************************************************
		for(WareHouse a : akki.getWarehouses())
		{
			if(a.getId() == house.getId())
			{
				house = a ; 
				house.InsertById(product, path);
			}
		}
		//********************************************************************
		try
		{
			File data = new File("SuperUserData.txt");
			if(data.exists())
			{
				FileOutputStream file = new FileOutputStream(data);
				ObjectOutputStream out = new ObjectOutputStream(file);
				
				data.delete();
				
				data = new File("SuperUserData.txt");
				file = new FileOutputStream(data);
				out = new ObjectOutputStream(file);
				
				out.writeObject(akki);
				out.close();
				file.close();
			}
			else
			{
				FileOutputStream file = new FileOutputStream(data);
				ObjectOutputStream out = new ObjectOutputStream(file);
				out.writeObject(akki);
				out.close();
				file.close();
			}
			System.out.println("Writing Successful");
		}
		catch(IOException e)
		{
			e.printStackTrace();
//			System.out.println("Error While Writing the Data");
		}
		//********************************************************************
	}
}

class searchinware extends Thread
{
	SuperUser akki;
	WareHouse house ;
	String product ;
	ArrayList<Category> ans = new ArrayList<Category>();
	
	public searchinware(WareHouse obj, String prd)
	{
		this.house = obj;
		this.product = prd;
	}
	public void run()
	{
		//********************************************************************
		SuperUser temp = null;
		try
		{
			File data = new File("SuperUserData.txt");
			if(data.exists())
			{
				FileInputStream file = new FileInputStream(data);
				ObjectInputStream in = new ObjectInputStream(file);
				temp = (SuperUser) in.readObject();
				in.close();
				file.close();
			}
			System.out.println("Reading Successful");
		} 
		catch (IOException | ClassNotFoundException e) 
		{
			e.printStackTrace();
		}
		finally
		{
			if(temp == null)
			{
				akki = new SuperUser();
			}
			else
			{
				akki = temp;
			}
		}
		//********************************************************************
		try 
		{
			for(WareHouse a : akki.getWarehouses())
			{
				if(a.getAdmin().getId() == house.getAdmin().getId())
				{
					house = a ; 
					this.ans = a.PartialSearch(product);
					System.out.println(this.ans+"Before1");
				}
			}
		}
		catch (ProductNotFoundException e1) 
		{
			System.out.println("Product Not Found");
		}
		System.out.println(this.ans+"Before2");
		//********************************************************************
		try
		{
			File data = new File("SuperUserData.txt");
			if(data.exists())
			{
				FileOutputStream file = new FileOutputStream(data);
				ObjectOutputStream out = new ObjectOutputStream(file);
				
				data.delete();
				
				data = new File("SuperUserData.txt");
				file = new FileOutputStream(data);
				out = new ObjectOutputStream(file);
				
				out.writeObject(akki);
				out.close();
				file.close();
			}
			else
			{
				FileOutputStream file = new FileOutputStream(data);
				ObjectOutputStream out = new ObjectOutputStream(file);
				out.writeObject(akki);
				out.close();
				file.close();
			}
			System.out.println("Writing Successful");
		}
		catch(IOException e)
		{
			e.printStackTrace();
//			System.out.println("Error While Writing the Data");
		}
		//********************************************************************
	}
}

class delete extends Thread
{
	SuperUser akki;
	WareHouse house ;
	long product;
	boolean condition = true;
	public delete(WareHouse obj, long prd)
	{
		this.house = obj;
		this.product = prd;
	}
	public void run()
	{
		//********************************************************************
		SuperUser temp = null;
		try
		{
			File data = new File("SuperUserData.txt");
			if(data.exists())
			{
				FileInputStream file = new FileInputStream(data);
				ObjectInputStream in = new ObjectInputStream(file);
				temp = (SuperUser) in.readObject();
				in.close();
				file.close();
			}
			System.out.println("Reading Successful");
		} 
		catch (IOException | ClassNotFoundException e) 
		{
			e.printStackTrace();
		}
		finally
		{
			if(temp == null)
			{
				akki = new SuperUser();
			}
			else
			{
				akki = temp;
			}
		}
		//********************************************************************
		for(WareHouse a : akki.getWarehouses())
		{
			if(a.getId() == house.getId())
			{
				house = a ; 
				condition = house.DeleteById(product);
			}
		}
		
		//********************************************************************
		try
		{
			File data = new File("SuperUserData.txt");
			if(data.exists())
			{
				FileOutputStream file = new FileOutputStream(data);
				ObjectOutputStream out = new ObjectOutputStream(file);
				
				data.delete();
				
				data = new File("SuperUserData.txt");
				file = new FileOutputStream(data);
				out = new ObjectOutputStream(file);
				
				out.writeObject(akki);
				out.close();
				file.close();
			}
			else
			{
				FileOutputStream file = new FileOutputStream(data);
				ObjectOutputStream out = new ObjectOutputStream(file);
				out.writeObject(akki);
				out.close();
				file.close();
			}
			System.out.println("Writing Successful");
		}
		catch(IOException e)
		{
			e.printStackTrace();
//			System.out.println("Error While Writing the Data");
		}
		//********************************************************************
	}
}

class modify extends Thread
{
	SuperUser akki;
	WareHouse house ;
	double cost;
	int q ; 
	long id ;
	String n;
	boolean condition = true;
	public modify(WareHouse obj, double c, long a, long i, String m)
	{
		this.house = obj;
		this.cost = c;
		this.q = (int) a;
		this.id = i;
		this.n = m;
	}
	public void run()
	{
		//********************************************************************
		SuperUser temp = null;
		try
		{
			File data = new File("SuperUserData.txt");
			if(data.exists())
			{
				FileInputStream file = new FileInputStream(data);
				ObjectInputStream in = new ObjectInputStream(file);
				temp = (SuperUser) in.readObject();
				in.close();
				file.close();
			}
			System.out.println("Reading Successful");
		} 
		catch (IOException | ClassNotFoundException e) 
		{
			e.printStackTrace();
		}
		finally
		{
			if(temp == null)
			{
				akki = new SuperUser();
			}
			else
			{
				akki = temp;
			}
		}
		//********************************************************************
		for(WareHouse a : akki.getWarehouses())
		{
			if(a.getWarehouseid() == house.getWarehouseid())
			{
				house = a ; 
				condition = house.ModifyById(cost, q, id, n);
			}
		}
		
		//********************************************************************
		try
		{
			File data = new File("SuperUserData.txt");
			if(data.exists())
			{
				FileOutputStream file = new FileOutputStream(data);
				ObjectOutputStream out = new ObjectOutputStream(file);
				
				data.delete();
				
				data = new File("SuperUserData.txt");
				file = new FileOutputStream(data);
				out = new ObjectOutputStream(file);
				
				out.writeObject(akki);
				out.close();
				file.close();
			}
			else
			{
				FileOutputStream file = new FileOutputStream(data);
				ObjectOutputStream out = new ObjectOutputStream(file);
				out.writeObject(akki);
				out.close();
				file.close();
			}
			System.out.println("Writing Successful");
		}
		catch(IOException e)
		{
			e.printStackTrace();
//			System.out.println("Error While Writing the Data");
		}
		//********************************************************************
	}
}
class handle extends Thread
{
	SuperUser akki ;
	WareHouse house;
	public handle(WareHouse a)
	{
		this.house = a;
	}
	public void run()
	{
		//********************************************************************
		SuperUser temp = null;
		try
		{
			File data = new File("SuperUserData.txt");
			if(data.exists())
			{
				FileInputStream file = new FileInputStream(data);
				ObjectInputStream in = new ObjectInputStream(file);
				temp = (SuperUser) in.readObject();
				in.close();
				file.close();
			}
			System.out.println("Reading Successful");
		} 
		catch (IOException | ClassNotFoundException e) 
		{
			e.printStackTrace();
		}
		finally
		{
			if(temp == null)
			{
				akki = new SuperUser();
			}
			else
			{
				akki = temp;
			}
		}
		//********************************************************************
		for(WareHouse a : akki.getWarehouses())
		{
			System.out.println(a.getAdmin().getId());
			if(a.getAdmin().getId() == house.getAdmin().getId())
			{
				house = a ;
				try 
				{
					akki = house.HandleOrder(house);
					a.getAdmin().setRequest(null);
					a.getAdmin().setOrder(null);
				} 
				catch (InsufficientStockException e) 
				{
					System.out.println("Insufficient Stock Error");
				}
			}
		}
		
		
		//********************************************************************
		try
		{
			File data = new File("SuperUserData.txt");
			if(data.exists())
			{
				FileOutputStream file = new FileOutputStream(data);
				ObjectOutputStream out = new ObjectOutputStream(file);
				
				data.delete();
				
				data = new File("SuperUserData.txt");
				file = new FileOutputStream(data);
				out = new ObjectOutputStream(file);
				
				out.writeObject(akki);
				out.close();
				file.close();
			}
			else
			{
				FileOutputStream file = new FileOutputStream(data);
				ObjectOutputStream out = new ObjectOutputStream(file);
				out.writeObject(akki);
				out.close();
				file.close();
			}
			System.out.println("Writing Successful");
		}
		catch(IOException e)
		{
			e.printStackTrace();
//			System.out.println("Error While Writing the Data");
		}
		//********************************************************************
	}
}



public class WareHouseThreads 
{
	
}
